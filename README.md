# CS_lab9_server

##To use server side of this application:

-   run npm install to install packages
    -copy .env.example in .env file
-   install mongoose for local server hosting
-   run "npm run start" in our project console
-   connect mongoose to our local DB
-   open server on localhost:3000

#lab 9
CSRF Security
One of the very dangerous attacks that an adversary can perform is a Cross-Site Request
Forgery (CSRF) attack. A CSRF attack allows the adversary to perform unwanted actions on
a web application in which a user is currently authenticated. The task for this week’s laboratory
work is to create a web application which would be protected from such attacks.
CSRF attacks bring the most damage when executed by a logged user (especially an admin).
To implement mechanisms of protection from CSRF, you’ll first need to create a web application
that would allow a user to authenticate themselves. After applying the defence mechanisms of
your choice, you’ll need to show their effectiveness via a video. To summarize, you’ll need to:
• Create a web application where the user can authenticate;
• Implement CSRF protection for your web application;
• Prove the effectiveness of implemented mechanisms (via video)
#lab 8
Email Confirmation
Some applications choose email confirmation as an additional step when signing up a new user.
This allows users to restore forgotten passwords and applications to protect from fake, spam
accounts. The task for this week’s laboratory work is to create an application that would
perform email confirmation.
You’ll first need to create an application that could register a new user. However, the
application must also require email confirmation (via a one time password / code or via a link).
After confirming their email, a user should be able to see that their email is confirmed. To
summarize:
• Create an application that could register a new user;
• Perform email confirmation (via a one time password / code or via a link);
• Output on the screen whether a user confirmed their email or did not confirm it yet.
#lab 7
Database Security
Nowadays, many applications require sensitive information about their users to work properly.
Users want to be sure that their data cannot be stolen or leaked and it is the job of the
programmer to ensure that their data is stored securely. The task for this week’s laboratory
work is to configure a MongoDB database so that it can store sensitive data securely.
You will need to pre-populate a MongoDB database with some data. Some of the data you
will keep in the database should be considered sensitive (e.g. a user’s email), hence needs to
be encrypted via 2-way encryption. After populating and securing the database, create an app
that would be able to access the database and output on screen the decrypted version of the
data stored. To summarize:
• Create a MongoDB database which would contain some secured sensitive data (protected
via 2-way encryption);
• Create an application which would display the data contained in the database (both
common data and the decrypted sensitive data);
• Make sure that the sensitive data can only be accessed via your application (i.e. it is
secure)
