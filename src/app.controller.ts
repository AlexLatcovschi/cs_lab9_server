import { Controller, Get, Param, Query, Req, Res } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { ImageProxyQuery } from './utils/dto/ImageProxy.query';

@ApiUseTags('Public')
@Controller()
export class AppController {
  @Get('public/:fileId')
  async serveImages(@Param('fileId') fileId, @Res() res): Promise<any> {
    res.sendFile(fileId, { root: 'public' });
  }

  @Get('public/articles/images/:fileId')
  async serveArticleImages(@Param('fileId') fileId, @Res() res): Promise<any> {
    res.sendFile(fileId, { root: 'public/articles/images' });
  }

  @Get('public/widgets/files/:fileId')
  async serveWidgetsFiles(@Param('fileId') fileId, @Res() res): Promise<any> {
    res.sendFile(fileId, { root: 'public/widgets/files' });
  }

  @Get('public/widget-templates/icons/:fileId')
  async serveWidgetTemplateImages(@Param('fileId') fileId, @Res() res): Promise<any> {
    res.sendFile(fileId, { root: 'public/widget-templates/icons' });
  }

  @Get('public/products/documentation/:fileId')
  async serveProductDocumentation(@Param('fileId') fileId, @Res() res): Promise<any> {
    res.sendFile(fileId, { root: 'public/products/documentation' });
  }

  @Get('public/products/images/:fileId')
  async serveProductsImages(
    @Param('fileId') fileId,
    @Res() res,
    @Req() req: any,
    @Query() query: ImageProxyQuery,
  ): Promise<any> {
    res.sendFile(fileId, { root: 'public/products/images' });
  }

  @Get('public/product-categories/thumbnail/:fileId')
  async serveProductCatThumbnail(@Param('fileId') fileId, @Res() res): Promise<any> {
    res.sendFile(fileId, { root: 'public/product-categories/thumbnail' });
  }

  @Get('public/product-categories/icon/:fileId')
  async serveProductCatIcon(@Param('fileId') fileId, @Res() res): Promise<any> {
    res.sendFile(fileId, { root: 'public/product-categories/icon' });
  }

  @Get('public/product/facility-icons/:fileId')
  async serveProductFacilityIcon(@Param('fileId') fileId, @Res() res): Promise<any> {
    res.sendFile(fileId, { root: 'public/product/facility-icons' });
  }

  @Get('public/menu/icons/:fileId')
  async serveMenuIcon(@Param('fileId') fileId, @Res() res): Promise<any> {
    res.sendFile(fileId, { root: 'public/menu/icons' });
  }

  @Get('public/pages/images/:fileId')
  async servePageImage(@Param('fileId') fileId, @Res() res): Promise<any> {
    res.sendFile(fileId, { root: 'public/pages/images' });
  }

  @Get('public/user/photo/:fileId')
  async serveUserPhoto(@Param('fileId') fileId, @Res() res): Promise<any> {
    res.sendFile(fileId, { root: 'public/user/photo' });
  }
}
