import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { DatabaseModule } from './database/database.module';
import { ProductsModule } from './modules/products/products.module';
import { ProductCategoriesModule } from './modules/product-categories/product-categories.module';
import { AttributesModule } from './modules/attributes/attributes.module';
import { FilterGroupModule } from './modules/filter-group/filter-group.module';
import { ArticlesModule } from './modules/articles/articles.module';
import { TagsModule } from './modules/tags/tags.module';
import { InitService } from './middlewares/init.service';
import { AuthModule } from './modules/auth/auth.module';
import { UserSchema } from './schemas/user.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from './modules/users/users.module';
import { ArticleCategoriesModule } from './modules/article-categories/article-categories.module';
import { CartModule } from './modules/cart/cart.module';
import { ConfigService } from './config/config.service';
import { FacilityModule } from './modules/facilities/facility.module';
import { LanguagesModule } from './modules/system/languages/languages.module';
import { MenuModule } from './modules/menus/menu.module';
import { OrderModule } from './modules/orders/order.module';
import { APP_GUARD } from '@nestjs/core';
import { RolesGuard } from './modules/auth/roles.guard';
import { ProfileModule } from './modules/profile/profile.module';
import { EmailModule } from './modules/email/email.module';
import { AttributeUnitModule } from './modules/attribute-units/attribute-unit.module';
import { ContactsController } from './utils/contacts.controller';
import { SeedModule } from './modules/seed/seed.module';
import { SearchModule } from './modules/search/search.module';
import { AttributeGroupModule } from './modules/attribute-group/attribute-group.module';
import { PublicInfoModule } from './modules/public-info/public-info.module';
import { PartnersModule } from './modules/partners/partners.module';
import { BlogModule } from './modules/blog/blog.module';
import { PartnersRequestModule } from './modules/partners-request/partners-request.module';

@Module({
  imports: [
    DatabaseModule,
    ProductsModule,
    ProductCategoriesModule,
    AttributesModule,
    FilterGroupModule,
    ArticlesModule,
    TagsModule,
    UsersModule,
    PublicInfoModule,
    FacilityModule,
    CartModule,
    LanguagesModule,
    AttributeUnitModule,
    MenuModule,
    AuthModule,
    EmailModule,
    ProfileModule,
    AttributeGroupModule,
    SearchModule,
    OrderModule,
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    ArticleCategoriesModule,
    SeedModule,
    PartnersModule,
    BlogModule,
    PartnersRequestModule,
  ],
  controllers: [AppController, ContactsController],
  providers: [
    ConfigService,
    InitService,
    {
      provide: APP_GUARD,
      useClass: RolesGuard,
    },
  ],
})
export class AppModule {}
