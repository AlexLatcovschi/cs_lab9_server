import { Module } from '@nestjs/common';
import { PartnersService } from './partners.service';
import { PartnersController } from './partners.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UploadController } from '../upload/upload.controller';
import { TagSchema } from '../../schemas/tag.schema';
import { LanguageSchema } from '../../schemas/language.schema';
import { ErrorsService } from '../../config/errors.service';
import { UploadService } from '../upload/upload.service';
import { PublicInfoSchema } from '../../schemas/PublicInfo.schema';
import { PartnersSchema } from '../../schemas/partners.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Partners', schema: PartnersSchema },
      { name: 'Language', schema: LanguageSchema },
    ]),
  ],
  providers: [PartnersService, ErrorsService, UploadService],
  controllers: [PartnersController, UploadController],
  exports: [PartnersService],
})
export class PartnersModule {}
