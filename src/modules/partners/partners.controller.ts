import { Body, Controller, Delete, Get, Param, Post, Query, UseGuards } from '@nestjs/common';
import { PartnersService } from './partners.service';
import { PartnersDto } from './dto/partners.dto';
import { Product } from '../../schemas/product.schema';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { AbstractLangQuery } from '../../utils/abstract/AbstractLang.query';
import { Roles } from '../../decorators/roles.decorator';
import { PublicInfo } from '../../schemas/PublicInfo.schema';
import { ProductFindQuery } from './dto/ProductFind.query';
import { Partners } from '../../schemas/partners.schema';

@ApiUseTags('Partners')
@Controller('partners')
export class PartnersController {
  constructor(private readonly service: PartnersService) {}

  @Get()
  async findAll(@Query() query: ProductFindQuery): Promise<Partners[]> {
    return this.service.findAll(query);
  }

  @Get('/:id')
  async findById(@Param('id') id: string, @Query() query: AbstractLangQuery): Promise<Partners> {
    return this.service.findById(id, query);
  }

  @Get('/slug/:slug')
  async findBySlug(@Param('slug') slug: string, @Query() query: any): Promise<Partners> {
    return this.service.findBySlug(slug, query);
  }

  @ApiBearerAuth()
  @Post()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async create(@Body() createProductDto: PartnersDto) {
    console.log('AAAAAAAAAAA: ', createProductDto);
    return this.service.create(createProductDto);
  }

  @ApiBearerAuth()
  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async remove(@Param('id') id: string) {
    return this.service.remove(id);
  }
}
