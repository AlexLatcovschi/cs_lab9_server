import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Response,
  UseGuards,
} from '@nestjs/common';
import { FacilityService } from './facility.service';
import { CreateFacilityDto } from './dto/create-facility.dto';
import { Facility } from '../../schemas/facility.schema';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { Roles } from '../../decorators/roles.decorator';

@ApiUseTags('Facilities')
@Controller('facilities')
export class FacilityController {
  constructor(private readonly facilityService: FacilityService) {}

  @Get()
  async findAll(@Query() query): Promise<Facility[]> {
    return this.facilityService.findAll(query);
  }

  @Get('/:id')
  async findById(@Param('id') id: string): Promise<Facility> {
    return this.facilityService.findById(id);
  }

  @ApiBearerAuth()
  @Post()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async create(@Response() res: any, @Body() createFacilityDto: CreateFacilityDto) {
    try {
      const facility = await this.facilityService.create(createFacilityDto);
      return res.status(HttpStatus.ACCEPTED).json(facility);
    } catch (err) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
    }
  }

  @ApiBearerAuth()
  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async findByIdAndUpdate(@Param('id') id: string, @Body() facility): Promise<Facility> {
    return this.facilityService.findByIdAndUpdate(id, facility);
  }

  @ApiBearerAuth()
  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async remove(@Param('id') id: string) {
    return this.facilityService.remove(id);
  }
}
