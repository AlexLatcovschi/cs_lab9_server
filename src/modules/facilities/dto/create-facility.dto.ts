export class CreateFacilityDto {
  readonly name: object;
  readonly description: object;
  readonly icon: string;
}
