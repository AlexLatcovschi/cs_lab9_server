import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UploadController } from '../upload/upload.controller';
import { FacilityController } from './facility.controller';
import { FacilityService } from './facility.service';
import { FacilitySchema } from '../../schemas/facility.schema';
import { ProductSchema } from '../../schemas/product.schema';
import { ErrorsService } from '../../config/errors.service';
import { UploadService } from '../upload/upload.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Facility', schema: FacilitySchema },
      { name: 'Product', schema: ProductSchema },
    ]),
  ],
  providers: [FacilityService, ErrorsService, UploadService],
  controllers: [FacilityController, UploadController],
})
export class FacilityModule {}
