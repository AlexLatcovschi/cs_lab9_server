import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateFacilityDto } from './dto/create-facility.dto';
import { Facility } from '../../schemas/facility.schema';
import { PaginateModel } from 'mongoose-paginate-v2';
import { getDataByLang } from '../../utils/lang.utils';
import { Product } from '../../schemas/product.schema';

@Injectable()
export class FacilityService {
  constructor(
    @InjectModel('Facility')
    private facilityModel: PaginateModel<Facility>,
    @InjectModel('Product')
    private productModel: PaginateModel<Product>,
  ) {}

  async findAll(queryParams: any): Promise<Facility[]> {
    const query: any = {};
    const options: any = {
      lean: true,
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
      sort: { updatedAt: -1 },
    };

    if (queryParams._sort) {
      options.sort = {
        [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
      };
    }

    if (Object.keys(queryParams).length === 0) {
      return this.facilityModel.find(query).sort({ createdAt: -1 });
    }

    if (queryParams.lang && !queryParams.page) {
      let array = await this.facilityModel
        .find(query)
        .sort({ createdAt: -1 })
        .lean();
      array = await getDataByLang(array, queryParams);
      return array;
    }

    if (queryParams.name) {
      query['name.' + queryParams.lang] = { $regex: queryParams.name, $options: 'i' };
    }

    const paginateData = await this.facilityModel.paginate(query, options);
    return getDataByLang(paginateData, queryParams);
  }

  async findById(id: string): Promise<Facility> {
    return this.facilityModel.findById(id);
  }

  async create(createFacilityDto: CreateFacilityDto): Promise<Facility> {
    const createdFacility = new this.facilityModel(createFacilityDto);
    return this.facilityModel.create(createdFacility);
  }

  async findByIdAndUpdate(id: string, data: object): Promise<Facility> {
    return this.facilityModel.findByIdAndUpdate(id, data);
  }

  async remove(id: string): Promise<Facility> {
    const products = await this.productModel.find({ facilities: { $in: id } });
    if (products.length > 0) {
      throw new BadRequestException('This facility is used in one product or more');
    } else {
      return this.facilityModel.findByIdAndDelete(id);
    }
  }
}
