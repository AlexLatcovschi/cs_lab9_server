import { Injectable } from '@nestjs/common';
import * as fs from 'fs';

@Injectable()
export class UploadService {
  removeFileByPath(path) {
    fs.unlink(path, err => {
      if (err) {
        return new Error('Error occurred while trying to remove file');
      }
    });
  }
}
