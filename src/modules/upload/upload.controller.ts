import { Body, Controller, Delete, Param, Post, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import * as multer from '../../config/multer.config';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { Roles } from '../../decorators/roles.decorator';
import { UploadService } from './upload.service';

@ApiUseTags('Upload')
@Controller('upload')
@UseGuards(AuthGuard('jwt'))
export class UploadController {
  constructor(private uploadService: UploadService) {}

  @Post('/products/image')
  @UseInterceptors(FileInterceptor('image', multer.ProductImageOptions))
  async uploadProductImage(@UploadedFile() file) {
    return file;
  }

  @Post('/pages/image')
  @UseInterceptors(FileInterceptor('image', multer.PageImageOptions))
  async uploadPageImage(@UploadedFile() file) {
    return file;
  }

  @Post('/products/document')
  @UseInterceptors(FileInterceptor('file0', multer.ProductDocumentationOptions))
  async uploadProductDocument(@UploadedFile() file) {
    return file;
  }

  @Post('/articles/image')
  @UseInterceptors(FileInterceptor('image', multer.ArticleImageOptions))
  async uploadArticleImage(@UploadedFile() file) {
    return file;
  }

  @Post('/widget-templates/icon')
  @UseInterceptors(FileInterceptor('image', multer.WidgetTemplateIconOptions))
  async uploadWidgetTempIcon(@UploadedFile() file) {
    return file;
  }

  @Post('/category/cover')
  @UseInterceptors(FileInterceptor('image', multer.ProductCategoryThumbnailOptions))
  async uploadCategoryCover(@UploadedFile() file) {
    return file;
  }

  @Post('/user/photo')
  @UseInterceptors(FileInterceptor('image', multer.UserPhotoOptions))
  async uploadUserPhoto(@UploadedFile() file) {
    return file;
  }

  @Post('/widget/file')
  @UseInterceptors(FileInterceptor('file', multer.WidgetFileOptions))
  async uploadWidgetFile(@UploadedFile() file) {
    return file;
  }

  @Post('/menu/icon')
  @UseInterceptors(FileInterceptor('image', multer.MenuIconOptions))
  async uploadMenuIcon(@UploadedFile() file) {
    return file;
  }

  @Post('/category/thumbnail')
  @UseInterceptors(FileInterceptor('image', multer.ProductCategoryThumbnailOptions))
  async uploadCategoryThumbnail(@UploadedFile() file) {
    return file;
  }

  @Post('/category/icon')
  @UseInterceptors(FileInterceptor('image', multer.ProductCategoryIconOptions))
  async uploadCategoryIcon(@UploadedFile() file) {
    return file;
  }

  @Post('/products/facility/icon')
  @UseInterceptors(FileInterceptor('image', multer.FacilityIconOptions))
  async uploadFacilityIcon(@UploadedFile() file) {
    return file;
  }

  @ApiBearerAuth()
  @Delete('/pages/image/:id')
  @Roles('admin')
  async removePageImage(@Param('id') id: string) {
    await this.uploadService.removeFileByPath(`public/widgets/files/${id}`);
  }
}
