import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class UpdateLanguageDto {
  @ApiModelPropertyOptional()
  readonly name: string;
  @ApiModelPropertyOptional()
  readonly shortName: string;
}
