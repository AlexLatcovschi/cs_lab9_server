import { ApiModelProperty } from '@nestjs/swagger';

export class CreateLanguageDto {
  @ApiModelProperty()
  readonly name: string;
  @ApiModelProperty()
  readonly shortName: string;
}
