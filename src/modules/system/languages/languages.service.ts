import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Language } from '../../../schemas/language.schema';
import { CreateLanguageDto } from './dto/create-language.dto';

@Injectable()
export class LanguagesService {
  constructor(@InjectModel('Language') private languageModel: Model<Language>) {}

  async findAll(query: any): Promise<Language[]> {
    return this.languageModel.find(query);
  }

  async findById(id: string): Promise<Language> {
    return this.languageModel.findById(id);
  }

  async create(createLanguageDto: CreateLanguageDto): Promise<Language> {
    const createdLanguage = new this.languageModel(createLanguageDto);
    return this.languageModel.create(createdLanguage);
  }

  async findByIdAndUpdate(id: string, data: object): Promise<Language> {
    return this.languageModel.findByIdAndUpdate(id, data, { new: true });
  }

  async remove(id: string): Promise<Language> {
    return this.languageModel.findByIdAndDelete(id);
  }
}
