import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { LanguagesService } from './languages.service';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { CreateLanguageDto } from './dto/create-language.dto';
import { Language } from '../../../schemas/language.schema';
import { UpdateLanguageDto } from './dto/update-language.dto';

@ApiUseTags('Languages')
@Controller('languages')
export class LanguagesController {
  constructor(private readonly languagesService: LanguagesService) {}

  @ApiBearerAuth()
  @Get()
  async findAll(@Query() query): Promise<Language[]> {
    return this.languagesService.findAll(query);
  }

  @ApiBearerAuth()
  @Get('/:id')
  async findById(@Param('id') id: string): Promise<Language> {
    return this.languagesService.findById(id);
  }

  @ApiBearerAuth()
  @Post()
  @UseGuards(AuthGuard('jwt'))
  async create(@Body() createLanguageDto: CreateLanguageDto) {
    return this.languagesService.create(createLanguageDto);
  }

  @ApiBearerAuth()
  @Put('/:id')
  @UseGuards(AuthGuard('jwt'))
  async findByIdAndUpdate(@Param('id') id: string, @Body() updateLanguageDto: UpdateLanguageDto): Promise<Language> {
    return this.languagesService.findByIdAndUpdate(id, updateLanguageDto);
  }

  @ApiBearerAuth()
  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  async remove(@Param('id') id: string) {
    return this.languagesService.remove(id);
  }
}
