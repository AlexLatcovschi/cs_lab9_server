import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { AttributesService } from './attributes.service';
import { Attribute } from '../../schemas/attribute.schema';
import { CreateAttributeDto } from './dto/create-attribute.dto';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { Roles } from '../../decorators/roles.decorator';

@ApiUseTags('Attributes')
@Controller('attributes')
export class AttributesController {
  constructor(private readonly attributesService: AttributesService) {}

  @Get()
  async findAll(@Query() query): Promise<Attribute[]> {
    return this.attributesService.findAll(query);
  }

  @Get(':id')
  async findById(@Param('id') id: string): Promise<Attribute> {
    return this.attributesService.findById(id);
  }

  @ApiBearerAuth()
  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async findByIdAndUpdate(@Param('id') id: string, @Body() attribute): Promise<Attribute> {
    return this.attributesService.findByIdAndUpdate(id, attribute);
  }

  @ApiBearerAuth()
  @Post()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async create(@Body() createAttributeDto: CreateAttributeDto) {
    return this.attributesService.create(createAttributeDto);
  }

  @ApiBearerAuth()
  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async remove(@Param('id') id: string) {
    return this.attributesService.remove(id);
  }
}
