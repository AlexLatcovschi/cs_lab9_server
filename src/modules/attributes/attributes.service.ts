import { BadRequestException, HttpException, Injectable } from '@nestjs/common';
import { PaginateModel } from 'mongoose-paginate-v2';
import { InjectModel } from '@nestjs/mongoose';
import { CreateAttributeDto } from './dto/create-attribute.dto';
import { Attribute } from '../../schemas/attribute.schema';
import { getDataByLang } from '../../utils/lang.utils';
import { FilterGroup } from '../../schemas/filter-group.schema';
import { Product } from '../../schemas/product.schema';

@Injectable()
export class AttributesService {
  constructor(
    @InjectModel('Attribute')
    private readonly attributeModel: PaginateModel<Attribute>,
    @InjectModel('Product')
    private readonly productModel: PaginateModel<Product>,
    @InjectModel('Attribute')
    private readonly filterModel: PaginateModel<FilterGroup>,
  ) {}

  async findAll(queryParams: any): Promise<Attribute[]> {
    const query: any = {};
    const options: any = {
      lean: true,
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
      populate: 'attributeGroup',
    };

    if (queryParams._sort) {
      options.sort = {
        [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
      };
    }

    if (Object.keys(queryParams).length === 0) {
      return this.attributeModel.find({}).populate('unit attributeGroup');
    }

    if (queryParams.lang && !queryParams.page) {
      let array = await this.attributeModel.find(query).lean();
      array = await getDataByLang(array, queryParams);
      return array;
    }

    if (queryParams.name) {
      query['name.' + queryParams.lang] = { $regex: queryParams.name, $options: 'i' };
    }

    const paginateData = await this.attributeModel.paginate(query, options);
    return getDataByLang(paginateData, queryParams);
  }

  async findById(id: string): Promise<Attribute> {
    return this.attributeModel.findById(id).populate('unit attributeGroup');
  }

  async findByIdAndUpdate(id: string, attribute: object): Promise<Attribute> {
    return this.attributeModel.findByIdAndUpdate(id, attribute);
  }

  async create(createAttributeDto: CreateAttributeDto): Promise<Attribute> {
    const newAttribute = new this.attributeModel(createAttributeDto);
    return this.attributeModel.create(newAttribute);
  }

  // TODO set null to all attributes when deleted
  async remove(id: string): Promise<Attribute> {
    const productsVariants = await this.productModel.find({
      variants: { $elemMatch: { attributes: { $elemMatch: { attributeId: id } } } },
    });
    const products = await this.productModel.find({ attributes: { $elemMatch: { attributeId: id } } });
    const filters = await this.filterModel.find({ attributes: { $elemMatch: { attributeId: id } } });
    if (filters.length > 0 || products.length > 0 || productsVariants.length > 0) {
      throw new BadRequestException('This attribute is used in one or more components');
    }
    return this.attributeModel.findByIdAndDelete(id);
  }
}
