import { IsEnum, IsNotEmpty, IsNumber, IsOptional, Min } from 'class-validator';
import { ATTRIBUTE_TYPE } from '../../../schemas/attribute.schema';
import { AttributeUnit } from '../../../schemas/attribute-unit.schema';
import { ApiModelProperty } from '@nestjs/swagger';

export class CreateAttributeDto {
  @ApiModelProperty()
  @IsNotEmpty()
  readonly name: object;
  @ApiModelProperty()
  @IsOptional()
  @IsNumber()
  @Min(1)
  readonly order: number;
  @ApiModelProperty({ enum: ATTRIBUTE_TYPE })
  @IsOptional()
  @IsEnum(ATTRIBUTE_TYPE)
  readonly type: string;
  @ApiModelProperty()
  readonly unit: AttributeUnit;
}
