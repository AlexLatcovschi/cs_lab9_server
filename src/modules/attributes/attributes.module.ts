import { Module } from '@nestjs/common';
import { AttributesController } from './attributes.controller';
import { AttributesService } from './attributes.service';
import { AttributeSchema } from '../../schemas/attribute.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { FilterGroupSchema } from '../../schemas/filter-group.schema';
import { ProductSchema } from '../../schemas/product.schema';
import { ErrorsService } from '../../config/errors.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'Attribute',
        schema: AttributeSchema,
      },
      {
        name: 'Product',
        schema: ProductSchema,
      },
      {
        name: 'FilterGroup',
        schema: FilterGroupSchema,
      },
    ]),
  ],

  controllers: [AttributesController],
  providers: [AttributesService, ErrorsService],
  exports: [AttributesService],
})
export class AttributesModule {}
