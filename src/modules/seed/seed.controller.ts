import { Controller, Get, UseGuards } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Product } from '../../schemas/product.schema';
import { ProductCategory } from '../../schemas/product-category.schema';
import { ProductsService } from '../products/products.service';
import { ProductCategoriesService } from '../product-categories/product-categories.service';
import { CreateProductDto } from '../products/dto/create-product.dto';
import { Attribute, ATTRIBUTE_TYPE } from '../../schemas/attribute.schema';
import * as faker from 'faker/locale/en';
import * as fs from 'fs';
import * as request from 'request';

import { AttributesService } from '../attributes/attributes.service';
import { AttributeUnitService } from '../attribute-units/attribute-unit.service';
import { AttributeUnit } from '../../schemas/attribute-unit.schema';
import { FilterGroup } from '../../schemas/filter-group.schema';
import { FilterGroupService } from '../filter-group/filter-group.service';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../../decorators/roles.decorator';
import { ApiBearerAuth } from '@nestjs/swagger';

@Controller('/seed')
export class SeedController {
  attributeVars: any[] = [
    {
      name: { ro: 'culoare', ru: 'цвет' },
      type: ATTRIBUTE_TYPE.color,
    },
    {
      name: { ro: 'carcasă', ru: 'кейс' },
      type: ATTRIBUTE_TYPE.color,
    },
    {
      name: { ro: 'memorie', ru: 'память' },
      type: ATTRIBUTE_TYPE.checkbox,
    },
    {
      name: { ro: 'Diagonala', ru: 'диагональ' },
      type: ATTRIBUTE_TYPE.checkbox,
    },
  ];

  constructor(
    @InjectModel('Product') private productModel: Product,
    @InjectModel('Attribute') private attributeModel: Attribute,
    @InjectModel('ProductCategory') private readonly productCategoryModel: ProductCategory,
    @InjectModel('AttributeUnit') private readonly attributeUnitModel: AttributeUnit,
    @InjectModel('FilterGroup') private readonly filterModel: FilterGroup,
    private productService: ProductsService,
    private categoryService: ProductCategoriesService,
    private attributesService: AttributesService,
    private attributeUnitService: AttributeUnitService,
    private filterGroupService: FilterGroupService,
  ) {}

  @ApiBearerAuth()
  @Get()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  public async seed(): Promise<string> {
    await this.productCategoryModel.remove();
    await this.productModel.remove();
    await this.attributeModel.remove();
    await this.attributeUnitModel.remove();
    await this.filterModel.remove();

    const download = (uri, filename, callback) => {
      request.head(uri, (err, res, body) => {
        console.log('content-type:', res.headers['content-type']);
        console.log('content-length:', res.headers['content-length']);
        if (!fs.existsSync('./public/products/images')) {
          fs.mkdirSync('./public/products/images', { recursive: true });
        }
        request(uri)
          .pipe(fs.createWriteStream(`./public/products/images/${filename}`, {}))
          .on('close', callback);
      });
    };

    download(
      'https://images.unsplash.com/photo-1484417894907-623942c8ee29?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1189&q=80',
      'test.jpg',
      call => {
        console.log('done');
      },
    );
    const units = [];

    for (const name of ['CM', 'BUC', 'GB', 'Mhz']) {
      units.push(await this.attributeUnitService.create({ name }));
    }

    const filterAttributes = [];

    for (const attr of this.attributeVars) {
      // attr.unit = units[units.length - 1];
      filterAttributes.push(await this.attributesService.create(attr));
    }
    const attributesMap = filterAttributes.map(value => {
      return {
        attributeId: value._id,
      };
    });
    const filterGroup: FilterGroup = await this.filterGroupService.create({
      name: this.formatLang('main filter'),
      description: this.formatLang('main filter description'),
      attributes: attributesMap,
    });

    const secondFilterGroup: FilterGroup = await this.filterGroupService.create({
      name: this.formatLang('second filter'),
      description: this.formatLang('second filter description'),
      attributes: attributesMap.slice(2),
    });

    const categories = [
      this.formatLang('Lapropuri', 'Ноутбуки'),
      this.formatLang('Smartfoane', 'Ноутбуки2'),
      this.formatLang('Tablete', 'Ноутбуки3'),
      this.formatLang('Mobila', 'Ноутбуки4'),
      this.formatLang('Auto', 'Ноутбуки5'),
    ];
    for (const name of categories) {
      const filters = [filterGroup._id, secondFilterGroup._id];
      const category: ProductCategory = await this.categoryService.create({ filters, name });

      for (let j = 0; j < 100; j++) {
        const prodDto = new CreateProductDto();
        prodDto.category = category._id;
        prodDto.title = this.formatLang(faker.commerce.productName());
        prodDto.price = faker.random.number({ min: 10, max: 5000 });
        prodDto.images = [];
        prodDto.variants = this.buildPriceVariant();
        const length = faker.random.number({ min: 1, max: 10 });
        for (let u = 0; u < length; u++) {
          prodDto.images.push({ path: 'public/products/images/test.jpg' });
        }
        prodDto.description = this.formatLang(faker.hacker.phrase());
        await this.productService.create(prodDto);
      }
    }
    return 'ok';
  }

  private formatLang(ro: string, ru?: string): object {
    return { ro, ru: ru || ro };
  }

  buildPriceVariant() {
    return [
      {
        sku: faker.random.alphaNumeric(8),
        price: faker.random.number({ min: 25, max: 8000 }),
        quantity: faker.random.number({ min: 0, max: 250 }),
        attributes: [],
      },
    ];
  }
}
