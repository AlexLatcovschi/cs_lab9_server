import { Module } from '@nestjs/common';
import { SeedController } from './seed.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductCategorySchema } from '../../schemas/product-category.schema';
import { ProductSchema } from '../../schemas/product.schema';
import { ProductsModule } from '../products/products.module';
import { ProductCategoriesModule } from '../product-categories/product-categories.module';
import { AttributeSchema } from '../../schemas/attribute.schema';
import { AttributesModule } from '../attributes/attributes.module';
import { AttributeUnitSchema } from '../../schemas/attribute-unit.schema';
import { AttributeUnitModule } from '../attribute-units/attribute-unit.module';
import { FilterGroupSchema } from '../../schemas/filter-group.schema';
import { FilterGroupModule } from '../filter-group/filter-group.module';
import { PartnersRequestSchema } from '../../schemas/partners-request.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'ProductCategory', schema: ProductCategorySchema },
      { name: 'Product', schema: ProductSchema },
      { name: 'Attribute', schema: AttributeSchema },
      { name: 'AttributeUnit', schema: AttributeUnitSchema },
      { name: 'FilterGroup', schema: FilterGroupSchema },
      { name: 'PartnersRequest', schema: PartnersRequestSchema },
    ]),
    ProductsModule,
    ProductCategoriesModule,
    AttributesModule,
    AttributeUnitModule,
    FilterGroupModule,
  ],
  controllers: [SeedController],
})
export class SeedModule {}
