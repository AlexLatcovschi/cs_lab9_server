import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateMenuDto } from './dto/create-menu.dto';
import { Menu } from '../../schemas/menu.schema';
import { PaginateModel } from 'mongoose-paginate-v2';
import { getDataByLang } from '../../utils/lang.utils';

@Injectable()
export class MenuService {
  constructor(@InjectModel('Menu') private menuModel: PaginateModel<Menu>) {}

  async findById(id: string): Promise<Menu> {
    return this.menuModel.findById(id);
  }

  async getActiveMenu(queryParams: any) {
    let menu = await this.menuModel.findOne({ active: true }).lean();
    if (queryParams.lang) {
      const menus = await getDataByLang([menu], queryParams);
      menu = menus[0];
      return menu;
    }
    return menu;
  }

  async create(createMenuDto: CreateMenuDto) {
    const createdMenu = new this.menuModel(createMenuDto);
    if (createdMenu.active === true) {
      await this.menuModel.updateMany({}, { $set: { active: false } });
    }
    return this.menuModel.create(createdMenu);
  }

  async findByIdAndUpdate(id: string, data) {
    if (data.active === true) {
      await this.menuModel.updateMany({}, { $set: { active: false } });
    } else if (data.active === false) {
      const activeMenu = await this.menuModel.findOne({ active: true });
      if (activeMenu && activeMenu._id.toString() === id) {
        throw new BadRequestException('This menu is active');
      }
    }
    return this.menuModel.findByIdAndUpdate(id, data);
  }

  async findAll(queryParams: any): Promise<Menu[]> {
    const query = {};
    const options: any = {
      lean: true,
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 2,
      sort: { updatedAt: -1 },
    };

    if (Object.keys(queryParams).length === 0) {
      return this.menuModel.find().sort({ createdAt: -1 });
    }

    if (queryParams.lang && !queryParams.page) {
      let array = await this.menuModel
        .find(query)
        .sort({ createdAt: -1 })
        .lean();
      array = await getDataByLang(array, queryParams);
      return array;
    }

    if (queryParams.name) {
      query['name.' + queryParams.lang] = { $regex: queryParams.name, $options: 'i' };
    }

    const paginateData = await this.menuModel.paginate(query, options);
    return getDataByLang(paginateData, queryParams);
  }

  async remove(id: string): Promise<Menu> {
    const activeMenu = await this.menuModel.findById(id);
    if (activeMenu.active) {
      throw new BadRequestException('This menu is active');
    }
    return this.menuModel.findByIdAndDelete(id);
  }
}
