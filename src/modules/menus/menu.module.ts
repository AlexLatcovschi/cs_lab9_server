import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UploadController } from '../upload/upload.controller';
import { MenuController } from './menu.controller';
import { MenuService } from './menu.service';
import { MenuSchema } from '../../schemas/menu.schema';
import { ErrorsService } from '../../config/errors.service';
import { UploadService } from '../upload/upload.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Menu', schema: MenuSchema }])],
  providers: [MenuService, ErrorsService, UploadService],
  controllers: [MenuController, UploadController],
})
export class MenuModule {}
