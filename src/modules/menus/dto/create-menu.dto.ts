export class CreateMenuDto {
  title: object;
  elements: object[];
  active: boolean;
}
