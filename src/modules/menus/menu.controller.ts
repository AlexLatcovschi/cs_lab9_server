import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Response,
  UseGuards,
} from '@nestjs/common';
import { MenuService } from './menu.service';
import { CreateMenuDto } from './dto/create-menu.dto';
import { Menu } from '../../schemas/menu.schema';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { Roles } from '../../decorators/roles.decorator';

@ApiUseTags('Menus')
@Controller('menus')
export class MenuController {
  constructor(private readonly menuService: MenuService) {}

  @Get('active')
  async getActiveMenu(@Query() query: any): Promise<Menu> {
    return this.menuService.getActiveMenu(query);
  }

  @ApiBearerAuth()
  @Get('/:id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async findById(@Param('id') id: string): Promise<Menu> {
    return this.menuService.findById(id);
  }

  @ApiBearerAuth()
  @Post()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async create(@Response() res: any, @Body() createMenuDto: CreateMenuDto) {
    try {
      const menu = await this.menuService.create(createMenuDto);
      return res.status(HttpStatus.ACCEPTED).json(menu);
    } catch (err) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
    }
  }

  @ApiBearerAuth()
  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async findByIdAndUpdate(@Param('id') id: string, @Body() menu): Promise<Menu> {
    return this.menuService.findByIdAndUpdate(id, menu);
  }

  @Get()
  async findAll(@Query() query): Promise<Menu[]> {
    return this.menuService.findAll(query);
  }

  @ApiBearerAuth()
  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async remove(@Param('id') id: string) {
    return this.menuService.remove(id);
  }
}
