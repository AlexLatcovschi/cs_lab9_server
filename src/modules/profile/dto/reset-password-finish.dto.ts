import { IsDefined, IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class ResetPasswordFinishDto {
  @IsNotEmpty()
  @ApiModelProperty()
  public key: string;
  @IsDefined()
  @ApiModelProperty()
  public newPassword: string;
}
