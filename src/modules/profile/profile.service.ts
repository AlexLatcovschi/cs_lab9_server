import { BadRequestException, HttpException, Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import * as crypto from 'crypto';
import { User } from '../../schemas/user.schema';
import { ResetPasswordFinishDto } from './dto/reset-password-finish.dto';
import { InjectEventEmitter } from 'nest-emitter';
import { EmailEvents } from '../email/email.events';
import { response } from 'express';
import { ConfigService } from '../../config/config.service';
import { ErrorsService } from '../../config/errors.service';

@Injectable()
export class ProfileService {
  constructor(
    @InjectEventEmitter() private readonly emitter: EmailEvents,
    private readonly usersService: UsersService,
    private config: ConfigService,
    private error: ErrorsService,
  ) {
    this.config = config;
  }

  public async activateEmailByToken(email: string, activationToken: string): Promise<void> {
    const user = await this.usersService.findOneByEmailForActivation(email, activationToken);
    console.log('YYY: ', user);

    if (!user) {
      throw new HttpException(this.error.get('USER_NOT_FOUND'), 400);
    }
    if (user.activated) {
      throw new HttpException(this.error.get('USER_HAS_ALREADY_ACTIVATED'), 400);
    }
    if (user.activationToken !== activationToken) {
      throw new HttpException(this.error.get('ACTIVATION_TOKEN_INVALID'), 400);
    }
    console.log('XXX: ', user);
    await this.usersService.activateUser(user.id);
    return response.redirect(this.config.get('CLIENT_URL'));
  }

  public async resetPasswordInit(email: string): Promise<any> {
    const user: User = await this.usersService.findOneByEmail(email);
    if (user) {
      user.passwordResetToken = generateToken();
      user.passwordResetExpire = new Date();
      user.passwordResetExpire.setHours(user.passwordResetExpire.getHours() + 24);
      this.emitter.emit('resetPasswordEmail', user);
      await this.usersService.findByIdAndUpdate(user._id, user);
      return { success: true };
    } else {
      throw new HttpException(this.error.get('USER_NOT_FOUND'), 400);
    }
  }

  public async resetPasswordFinish(dto: ResetPasswordFinishDto): Promise<any> {
    const date = new Date();
    const user: User = await this.usersService.findOneByKeyForPasswordReset(dto.key);
    if (user) {
      if (date < user.passwordResetExpire) {
        user.passwordResetToken = null;
        user.passwordResetExpire = null;
        user.password = dto.newPassword;
        user.save();
        this.emitter.emit('resetPasswordConfirmEmail', user);
        return { success: true };
      } else {
        throw new HttpException(this.error.get('TOKEN_TIME_EXPIRED'), 400);
      }
    } else {
      throw new HttpException(this.error.get('ACTIVATION_TOKEN_INVALID'), 400);
    }
  }
}

export const generateToken = (): string => {
  return crypto.randomBytes(16).toString('hex');
};
