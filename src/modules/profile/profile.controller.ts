import { Body, Controller, Get, HttpCode, Post, Query } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { User } from '../../schemas/user.schema';
import { ProfileService } from './profile.service';
import { ResetPasswordInitDto } from './dto/reset-password-init.dto';
import { ResetPasswordFinishDto } from './dto/reset-password-finish.dto';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('Profile')
@Controller('profile')
export class ProfileController {
  constructor(private userService: UsersService, private profileService: ProfileService) {}

  @Get('/activate-email')
  public async activateEmail(@Query('email') email: string, @Query('key') token: string): Promise<void> {
    return this.profileService.activateEmailByToken(email, token);
  }

  @Post('/reset-password/init')
  public async resetPasswordInit(@Body() resetPasswordDto: ResetPasswordInitDto): Promise<any> {
    return this.profileService.resetPasswordInit(resetPasswordDto.email);
  }

  @Post('/reset-password/finish')
  public async resetPasswordFinish(@Body() resetPasswordFinishDto: ResetPasswordFinishDto): Promise<any> {
    console.log('resetPasswordFinishDto: ', resetPasswordFinishDto);
    return this.profileService.resetPasswordFinish(resetPasswordFinishDto);
  }
}
