import { IsNotEmpty, IsNumber, IsOptional, Min } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class AttributeGroupDto {
  readonly _id: string;
  @ApiModelProperty()
  @IsNotEmpty()
  readonly name: object;
  @ApiModelProperty()
  @IsOptional()
  @IsNumber()
  @Min(1)
  readonly order: number;
}
