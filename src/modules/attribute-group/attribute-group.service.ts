import { Injectable } from '@nestjs/common';
import { PaginateModel } from 'mongoose-paginate-v2';
import { InjectModel } from '@nestjs/mongoose';
import { IAttributeGroup } from '../../schemas/attribute-group.schema';
import { AttributeGroupDto } from './dto/attribute-group.dto';
import { Attribute } from '../../schemas/attribute.schema';
import { getDataByLang } from '../../utils/lang.utils';

@Injectable()
export class AttributeGroupService {
  constructor(
    @InjectModel('AttributeGroup')
    private readonly attributeGroupSchema: PaginateModel<IAttributeGroup>,
  ) {}

  async findAll(queryParams: any): Promise<AttributeGroupDto[]> {
    const query: any = {};
    const options: any = {
      lean: true,
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
    };

    if (queryParams._sort) {
      options.sort = {
        [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
      };
    }

    if (Object.keys(queryParams).length === 0) {
      return this.attributeGroupSchema.find({});
    }

    if (queryParams.lang && !queryParams.page) {
      let array = await this.attributeGroupSchema.find(query).lean();
      array = await getDataByLang(array, queryParams);
      return array;
    }

    if (queryParams.name) {
      query['name.' + queryParams.lang] = { $regex: queryParams.name, $options: 'i' };
    }

    const paginateData = await this.attributeGroupSchema.paginate(query, options);
    return getDataByLang(paginateData, queryParams);
  }

  async findById(id: string): Promise<AttributeGroupDto> {
    return this.attributeGroupSchema.findById(id);
  }

  async findByIdAndUpdate(id: string, attribute: AttributeGroupDto): Promise<AttributeGroupDto> {
    return this.attributeGroupSchema.findByIdAndUpdate(id, attribute);
  }

  async create(createAttributeGroupDto: AttributeGroupDto): Promise<AttributeGroupDto> {
    const newAttributeGroup = new this.attributeGroupSchema(createAttributeGroupDto);
    return this.attributeGroupSchema.create(newAttributeGroup);
  }

  async remove(id: string): Promise<AttributeGroupDto> {
    return this.attributeGroupSchema.findByIdAndDelete(id);
  }
}
