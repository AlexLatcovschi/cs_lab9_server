import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { AttributeGroupDto } from './dto/attribute-group.dto';
import { AttributeGroupService } from './attribute-group.service';
import { Roles } from '../../decorators/roles.decorator';

@ApiBearerAuth()
@ApiUseTags('Attribute Groups')
@UseGuards(AuthGuard('jwt'))
@Roles('admin')
@Controller('attribute-groups')
export class AttributeGroupController {
  constructor(private readonly attributeGroupService: AttributeGroupService) {}

  @Get()
  async findAll(@Query() query): Promise<AttributeGroupDto[]> {
    return this.attributeGroupService.findAll(query);
  }

  @Get(':id')
  async findById(@Param('id') id: string): Promise<AttributeGroupDto> {
    return this.attributeGroupService.findById(id);
  }

  @Put(':id')
  async findByIdAndUpdate(@Param('id') id: string, @Body() attribute: AttributeGroupDto): Promise<AttributeGroupDto> {
    return this.attributeGroupService.findByIdAndUpdate(id, attribute);
  }

  @Post()
  async create(@Body() createAttributeGroupDto: AttributeGroupDto) {
    return this.attributeGroupService.create(createAttributeGroupDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    return this.attributeGroupService.remove(id);
  }
}
