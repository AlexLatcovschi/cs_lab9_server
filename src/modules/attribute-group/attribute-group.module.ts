import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ErrorsService } from '../../config/errors.service';
import { AttributeGroupSchema } from '../../schemas/attribute-group.schema';
import { AttributeGroupService } from './attribute-group.service';
import { AttributeGroupController } from './attribute-group.controller';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'AttributeGroup', schema: AttributeGroupSchema }])],
  controllers: [AttributeGroupController],
  providers: [AttributeGroupService, ErrorsService],
  exports: [AttributeGroupService],
})
export class AttributeGroupModule {}
