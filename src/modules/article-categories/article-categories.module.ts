import { Module } from '@nestjs/common';
import { ArticleCategoriesService } from './article-categories.service';
import { ArticleCategoriesController } from './article-categories.controller';
import { UploadController } from '../upload/upload.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticleCategorySchema } from '../../schemas/article-category.schema';
import { ArticleSchema } from '../../schemas/article.schema';
import { ErrorsService } from '../../config/errors.service';
import { UploadService } from '../upload/upload.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'ArticleCategory', schema: ArticleCategorySchema },
      { name: 'Article', schema: ArticleSchema },
    ]),
  ],
  providers: [ArticleCategoriesService, ErrorsService, UploadService],
  controllers: [ArticleCategoriesController, UploadController],
})
export class ArticleCategoriesModule {}
