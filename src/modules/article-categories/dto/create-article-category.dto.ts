export class CreateArticleCategoryDto {
  readonly name: object;
  readonly description: object;
  readonly slug: string;
}
