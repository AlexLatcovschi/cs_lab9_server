import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateArticleCategoryDto } from './dto/create-article-category.dto';
import { PaginateModel } from 'mongoose-paginate-v2';
import { ArticleCategory } from '../../schemas/article-category.schema';
import { generateSlugByName } from '../../utils/generate-slug.utils';
import { getDataByLang } from '../../utils/lang.utils';
import { Article } from '../../schemas/article.schema';

@Injectable()
export class ArticleCategoriesService {
  constructor(
    @InjectModel('ArticleCategory')
    private articleCategoryModel: PaginateModel<ArticleCategory>,
    @InjectModel('Article')
    private articleModel: PaginateModel<Article>,
  ) {}

  async findAll(queryParams: any): Promise<ArticleCategory[]> {
    const query: any = {};
    const options: any = {
      lean: true,
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
    };

    if (queryParams._sort) {
      options.sort = {
        [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
      };
    }

    if (Object.keys(queryParams).length === 0) {
      let articlesCategories = await this.articleCategoryModel.find(query).lean();
      articlesCategories = await this.countNumberOfArticlesAndSortByCount(articlesCategories);
      return articlesCategories;
    }

    if (queryParams.name) {
      query['name.' + queryParams.lang] = { $regex: queryParams.name, $options: 'i' };
    }

    if (queryParams.top) {
      let translatedArticlesCategories = await this.articleCategoryModel.find(query).lean();
      translatedArticlesCategories = await this.countNumberOfArticlesAndSortByCount(translatedArticlesCategories);
      return getDataByLang(translatedArticlesCategories, queryParams).slice(0, queryParams.top);
    }

    if (queryParams.lang && !queryParams.page) {
      let translatedArticlesCategories = await this.articleCategoryModel.find(query).lean();
      translatedArticlesCategories = await this.countNumberOfArticlesAndSortByCount(translatedArticlesCategories);
      return getDataByLang(translatedArticlesCategories, queryParams);
    }

    const paginatedArticleCategories = await this.articleCategoryModel.paginate(query, options);
    paginatedArticleCategories.docs = await this.countNumberOfArticlesAndSortByCount(paginatedArticleCategories.docs);
    return getDataByLang(paginatedArticleCategories, queryParams);
  }

  async findById(id: string): Promise<ArticleCategory> {
    return this.articleCategoryModel.findById(id);
  }

  async findBySlug(slug: string, queryParams: any): Promise<ArticleCategory> {
    if (queryParams.lang) {
      let array = await this.articleCategoryModel.findOne({ slug }).lean();
      array = await getDataByLang([array], queryParams);
      return array[0];
    }
    return this.articleCategoryModel.findOne({ slug });
  }

  async create(createArticleDto: CreateArticleCategoryDto) {
    let createdArticle = new this.articleCategoryModel(createArticleDto);
    createdArticle = await generateSlugByName(createdArticle, 'name');
    return this.articleCategoryModel.create(createdArticle);
  }

  async findByIdAndUpdate(id: string, data): Promise<ArticleCategory> {
    data = await generateSlugByName(data, 'name');
    return this.articleCategoryModel.findByIdAndUpdate(id, data);
  }

  async remove(id: string): Promise<ArticleCategory> {
    const articles = await this.articleModel.find({ categories: { $in: id } });
    if (articles.length > 0) {
      throw new BadRequestException('This category is used in one article or more');
    } else {
      return this.articleCategoryModel.findByIdAndDelete(id);
    }
  }

  async countNumberOfArticlesAndSortByCount(categories) {
    for (const category of categories) {
      category.count = await this.articleModel.countDocuments({
        category: category._id,
      });
    }
    return categories.sort((a, b) => (a.count > b.count ? -1 : 1));
  }
}
