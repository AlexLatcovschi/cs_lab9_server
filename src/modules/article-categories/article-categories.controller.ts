import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ArticleCategoriesService } from './article-categories.service';
import { CreateArticleCategoryDto } from './dto/create-article-category.dto';
import { ArticleCategory } from '../../schemas/article-category.schema';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { Roles } from '../../decorators/roles.decorator';

@ApiUseTags('Article-categories')
@Controller('article-categories')
export class ArticleCategoriesController {
  constructor(private readonly articleCategoriesService: ArticleCategoriesService) {}

  @Get()
  async findAll(@Query() query: any): Promise<ArticleCategory[]> {
    return this.articleCategoriesService.findAll(query);
  }

  @Get('/:id')
  async findById(@Param('id') id: string): Promise<ArticleCategory> {
    return this.articleCategoriesService.findById(id);
  }

  @Get('/slug/:slug')
  async findBySlug(@Param('slug') slug: string, @Query() query: any): Promise<ArticleCategory> {
    return this.articleCategoriesService.findBySlug(slug, query);
  }

  @ApiBearerAuth()
  @Post()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async create(@Body() createArticleCategoryDto: CreateArticleCategoryDto) {
    return this.articleCategoriesService.create(createArticleCategoryDto);
  }

  @ApiBearerAuth()
  @Put('/:id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async findByIdAndUpdate(@Param('id') id: string, @Body() data): Promise<ArticleCategory> {
    return this.articleCategoriesService.findByIdAndUpdate(id, data);
  }

  @ApiBearerAuth()
  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async remove(@Param('id') id: string) {
    return this.articleCategoriesService.remove(id);
  }
}
