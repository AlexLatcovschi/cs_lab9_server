import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ProductCategoriesService } from './product-categories.service';
import { ProductCategory } from '../../schemas/product-category.schema';
import { CreateProductCategoryDto } from './dto/create-product-category.dto';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { Product } from '../../schemas/product.schema';
import { ProductsService } from '../products/products.service';
import { ProductFindQuery } from '../products/dto/ProductFind.query';
import { AbstractLangQuery } from '../../utils/abstract/AbstractLang.query';
import { Roles } from '../../decorators/roles.decorator';

@ApiUseTags('Category')
@Controller('categories')
export class ProductCategoriesController {
  constructor(
    private readonly productCategoriesService: ProductCategoriesService,
    private readonly productService: ProductsService,
  ) {}

  @Get()
  findAll(@Query() query): Promise<ProductCategory[]> {
    return this.productCategoriesService.findAll(query);
  }

  @Get('/:id/products')
  findProductsByCategory(@Param('id') id: string, @Query() query: ProductFindQuery): Promise<Product[]> {
    return this.productService.findManyByCategory(id, query);
  }

  @Get('/:id/subcategories')
  findCategoryByIdWithSubcategories(@Param('id') id: string, @Query() query: any): Promise<ProductCategory[]> {
    return this.productCategoriesService.findCategoryByIdWithSubcategories(id, query);
  }

  @Get(':id')
  findById(@Param('id') id: string): Promise<ProductCategory> {
    return this.productCategoriesService.findById(id);
  }

  @Get('/slug/:slug')
  findBySlug(@Param('slug') slug: string, @Query() query: AbstractLangQuery): Promise<ProductCategory> {
    return this.productCategoriesService.findBySlug(slug, query);
  }

  @ApiBearerAuth()
  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async findByIdAndUpdate(@Param('id') id: string, @Body() category): Promise<ProductCategory> {
    return this.productCategoriesService.findByIdAndUpdate(id, category);
  }

  @ApiBearerAuth()
  @Post()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async create(@Body() createProductCategoryDto: CreateProductCategoryDto) {
    return this.productCategoriesService.create(createProductCategoryDto);
  }

  @ApiBearerAuth()
  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async remove(@Param('id') id: string) {
    return this.productCategoriesService.remove(id);
  }
}
