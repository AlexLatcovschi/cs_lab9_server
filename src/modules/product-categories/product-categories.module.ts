import { Module } from '@nestjs/common';
import { ProductCategoriesController } from './product-categories.controller';
import { ProductCategoriesService } from './product-categories.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductCategorySchema } from '../../schemas/product-category.schema';
import { ProductSchema } from '../../schemas/product.schema';
import { ArticleSchema } from '../../schemas/article.schema';
import { ProductsModule } from '../products/products.module';
import { ErrorsService } from '../../config/errors.service';

@Module({
  imports: [
    ProductsModule,
    MongooseModule.forFeature([
      { name: 'ProductCategory', schema: ProductCategorySchema },
      { name: 'Product', schema: ProductSchema },
      { name: 'Article', schema: ArticleSchema },
    ]),
  ],
  controllers: [ProductCategoriesController],
  providers: [ProductCategoriesService, ErrorsService],
  exports: [ProductCategoriesService],
})
export class ProductCategoriesModule {}
