import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { PaginateModel } from 'mongoose-paginate-v2';
import { InjectModel } from '@nestjs/mongoose';
import { CreateProductCategoryDto } from './dto/create-product-category.dto';
import { ProductCategory } from '../../schemas/product-category.schema';
import * as mongoose from 'mongoose';
import { generateSlugByName } from '../../utils/generate-slug.utils';
import { getCategoryWithSubcategoriesByLang, getDataByLang } from '../../utils/lang.utils';
import { Product } from '../../schemas/product.schema';
import { Article } from '../../schemas/article.schema';
import { AbstractLangQuery } from '../../utils/abstract/AbstractLang.query';

@Injectable()
export class ProductCategoriesService {
  constructor(
    @InjectModel('ProductCategory') private readonly productCategoryModel: PaginateModel<ProductCategory>,
    @InjectModel('Product') private readonly productModel: PaginateModel<Product>,
    @InjectModel('Article') private readonly articleModel: PaginateModel<Article>,
  ) {}

  async findAll(queryParams: any): Promise<ProductCategory[]> {
    const query: any = {};
    const options: any = {
      lean: true,
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
      populate: 'filters',
      sort: queryParams._sort ? { [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1 } : { order: 1 },
    };

    if (Object.keys(queryParams).length === 0) {
      return this.productCategoryModel
        .find()
        .populate({ path: 'filters', populate: { path: 'attributes.attributeId' } })
        .sort(queryParams._sort ? { [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1 } : { order: 1 });
    }

    if (queryParams.lang && !queryParams.page) {
      let array = await this.productCategoryModel
        .find(query)
        .lean()
        .populate({ path: 'filters', populate: { path: 'attributes.attributeId' } })
        .sort(queryParams._sort ? { [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1 } : { order: 1 });
      array = await getDataByLang(array, queryParams);
      return array;
    }

    if (queryParams.name) {
      query['name.' + queryParams.lang] = { $regex: queryParams.name, $options: 'i' };
    }

    const paginateData = await this.productCategoryModel.paginate(query, options);
    return getDataByLang(paginateData, queryParams);
  }

  async findCategoryByIdWithSubcategories(id, query?) {
    const agregatedCategories = await this.productCategoryModel.aggregate([
      {
        // get stuff where you have the parent or subcats.
        $match: {
          $or: [{ _id: mongoose.Types.ObjectId(id) }, { parent: mongoose.Types.ObjectId(id) }],
        },
      },
      // reshape the data you'll need further on from each mached doc
      {
        $project: {
          _id: false,
          data: {
            id: '$_id',
            name: '$name',
            slug: '$slug',
            filters: '$filters',
            thumbnail: '$thumbnail',
            coverPhoto: '$coverPhoto',
            description: '$description',
            order: '$order',
            // I guess you'll also want the `slug` and `image` here.
            // but that's homework :)
          },
          parent: '$parent',
        },
      },
      // now put a common _id so you can group them, and also put stuff into arrays
      {
        $project: {
          id: { $literal: 'id' },
          parent: {
            // if our parent is null, put our data.
            // otherwise put null here.
            $cond: [
              { $eq: [null, '$parent'] },
              {
                _id: '$data.id',
                name: '$data.name',
                slug: '$data.slug',
                filters: '$data.filters',
                thumbnail: '$data.thumbnail',
                coverPhoto: '$data.coverPhoto',
                description: '$data.description',
              },
              null,
            ],
          },
          subcat: {
            // here is the other way around.
            $cond: [
              { $ne: [null, '$parent'] },
              {
                _id: '$data.id',
                name: '$data.name',
                slug: '$data.slug',
                filters: '$data.filters',
                thumbnail: '$data.thumbnail',
                coverPhoto: '$data.coverPhoto',
                description: '$data.description',
                order: '$data.order',
              },
              null,
            ],
          },
        },
        // that stage produces for each doc either a mainCat or subcat
        // (and the other prop equals to null)
      },
      // finally, group the things so you can have them together
      {
        $group: {
          _id: '$id',
          // a bit hacky, but mongo will yield to it
          parent: { $max: '$parent' },
          childs: {
            // this will, unfortunately, also add the `null` we have
            // assigned to main category up there
            $addToSet: '$subcat',
          },
        },
      },
      // so we get rid of the unwanted _id = 'id' and the null from subcats.
      {
        $project: {
          _id: false,
          parent: 1,
          childs: {
            $setDifference: ['$childs', [null]],
          },
        },
      },
    ]);
    agregatedCategories[0].childs = await agregatedCategories[0].childs.sort((a, b) => {
      if (a.order === null || a.order === undefined) {
        return 1;
      } else if (b.order === null || b.order === undefined) {
        return -1;
      } else if (a.order === b.order) {
        return a.slug.localeCompare(b.slug);
      } else {
        return a.order - b.order;
      }
    });
    return getCategoryWithSubcategoriesByLang(agregatedCategories[0], query.lang);
  }

  async findById(id: string): Promise<ProductCategory> {
    return this.productCategoryModel.findById(id);
  }

  async findBySlug(slug: string, queryParams: AbstractLangQuery): Promise<ProductCategory> {
    if (queryParams.lang) {
      const productCategory = await this.productCategoryModel
        .findOne({ slug })
        .lean()
        .populate({
          path: 'filters',
          populate: { path: 'attributes.attributeId', select: 'name type _id', populate: { path: 'unit' } },
        });

      if (!productCategory) {
        throw new NotFoundException();
      }

      if (productCategory.filters && productCategory.filters.length) {
        productCategory.filters = productCategory.filters
          .flatMap(val => val.attributes)
          .map(({ attributeId }) => attributeId)
          .reduce((prev, curr) => (prev.find(v => v._id === curr._id) ? prev : [...prev, curr]), [])
          .map(({ name, type, _id }) => {
            return { _id, name, type, options: {} };
          });

        for (const filter of productCategory.filters) {
          filter.options = await this.productModel
            .find({
              category: productCategory._id,
              'variants.quantity': { $gt: 0 },
              'variants.attributes.attributeId': filter._id,
            })
            .select('variants')
            .populate({ path: 'variants.attributes.attributeId', select: ['name', 'type', '_id'] })
            .lean();
        }
      }
      await this.getPriceRange(productCategory._id).then(res => {
        if (res !== null && (productCategory && productCategory.filters)) {
          productCategory.filters.splice(0, 0, res);
        }
      });
      return getDataByLang(productCategory, queryParams);
    }
  }

  async findByIdAndUpdate(id: string, category): Promise<ProductCategory> {
    category = await generateSlugByName(category, 'name');
    return this.productCategoryModel.findByIdAndUpdate(id, category);
  }

  async create(createProductDto: CreateProductCategoryDto): Promise<ProductCategory> {
    let createdProduct = new this.productCategoryModel(createProductDto);
    createdProduct = await generateSlugByName(createdProduct, 'name');
    return this.productCategoryModel.create(createdProduct);
  }

  // TODO set null to all products when deleted
  async remove(id: string): Promise<ProductCategory> {
    const products = await this.productModel.find({ category: id });
    const articles = await this.articleModel.find({ productCategories: { $in: id } });
    if (products.length > 0) {
      throw new BadRequestException('This category is used in one product or more');
    } else {
      if (articles.length > 0) {
        throw new BadRequestException('This category is used in one articles or more');
      }
      return this.productCategoryModel.findByIdAndDelete(id);
    }
  }

  async getPriceRange(category) {
    const products = await this.productModel
      .find({ category, 'variants.price': { $ne: null } })
      .sort({ 'variants.price': 1 });

    return products.length > 0
      ? {
          name: 'Price',
          type: 'price',
          minPrice: Math.min(...products[0].variants.map(variant => variant.price)),
          maxPrice: Math.max(...products[products.length - 1].variants.map(variant => variant.price)),
        }
      : null;
  }
}
