import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class CoverPhotoDto {
  @ApiModelPropertyOptional()
  description: string;
  @ApiModelPropertyOptional()
  path: string;
  @ApiModelPropertyOptional()
  mimetype: string;
}
