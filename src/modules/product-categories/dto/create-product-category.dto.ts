import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { CoverPhotoDto } from './cover-photo.dto';

export class CreateProductCategoryDto {
  @ApiModelProperty()
  @IsNotEmpty()
  readonly name: object;
  @ApiModelPropertyOptional({ description: 'Description of category' })
  readonly description?: object;
  readonly slug?: string;
  @ApiModelPropertyOptional({ description: 'Parent category if existing' })
  readonly parent?: string;
  @ApiModelPropertyOptional({ description: 'Filters group of category' })
  readonly filters?: string[];
  @ApiModelPropertyOptional({ description: 'Category cover photo info' })
  readonly coverPhoto?: CoverPhotoDto;
  @ApiModelPropertyOptional({ description: 'Category thumbnail info' })
  readonly thumbnail?: {
    description: string;
    path: string;
    mimetype: string;
  };
  readonly order?: number;
  readonly icon?: {
    path: string;
    mimetype: string;
  };
  readonly activeIcon?: {
    path: string;
    mimetype: string;
  };
}
