## Cart logic

-   If user is logged out and there is no cartId in localstorage

    1. User selects product and add to cart
    2. Client will send a request to product/add with product only
    3. Server will create a cart and will add the product to it and return cart to client
    4. Then Client will save cartId to localstorage
    5. If user add another product to cart then he wil call
       product/add/:cartId(that will be taken from localstorage)

-   If user is logged out and there is cartId in localstorage

    1. If user add another product to cart then he wil call
       product/add/:cartId(that will be taken from localstorage)

-   If user is logged in and there no cartId in localstorage

    1. Client will send a request to product/add with product and userId
    2. Server will create a cart and will add the product to it and return cart to client

-   If user is logged in and there is cartId in localstorage

    4. If user add another product to cart then he wil call
       product/add/:cartId(that will be taken from localstorage)
