import { Body, Controller, Delete, Get, Param, Patch, Post, Put, Query, Req, UseGuards } from '@nestjs/common';
import { CartService } from './cart.service';
import { Cart } from '../../schemas/cart.schema';
import { AddProductToCartDto } from './dto/add-product-to-cart.dto';
import { JwtService } from '@nestjs/jwt';
import { AuthGuard } from '@nestjs/passport';
import { AuthUser } from '../../decorators/user.decorator';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { Roles } from '../../decorators/roles.decorator';

@ApiUseTags('Carts')
@Controller('carts')
export class CartController {
  constructor(private readonly cartService: CartService, private readonly jwtService: JwtService) {}

  @ApiBearerAuth()
  @ApiOperation({ title: 'Get my active cart' })
  @ApiResponse({ status: 200, description: 'Return my active cart' })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Get('/me')
  @UseGuards(AuthGuard('jwt'))
  async getMyCart(@AuthUser() user: any, @Query() query): Promise<Cart> {
    return this.cartService.findCartByUserId(user._id, query);
  }

  @ApiBearerAuth()
  @ApiOperation({ title: 'Get active cart by user id' })
  @ApiResponse({ status: 200, description: 'Return cart' })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Get('/user/:userId')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async getCartByUserId(@Param('userId') userId: string, @Query() query): Promise<Cart> {
    return this.cartService.findCartByUserId(userId, query);
  }

  @ApiBearerAuth()
  @ApiOperation({ title: 'Merge two cart' })
  @ApiResponse({ status: 200, description: 'Return final cart' })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Patch('/:firstCart/merge/:secondCart')
  @UseGuards(AuthGuard('jwt'))
  async combineCarts(
    @AuthUser() user: any,
    @Param('firstCart') firstCart: string,
    @Param('secondCart') secondCart: string,
  ): Promise<Cart> {
    return this.cartService.combineCarts(user._id, firstCart, secondCart);
  }

  @ApiOperation({ title: 'Get cart by cartId' })
  @ApiResponse({ status: 200, description: 'Return cart by cartId' })
  @Get('/:cartId')
  async findCartByUserId(@Param('cartId') cartId: string, @Query() query): Promise<Cart> {
    return this.cartService.findCartById(cartId, query);
  }

  @ApiOperation({ title: 'Create cart and add product to it' })
  @ApiResponse({ status: 200, description: 'Return new cart with product in it' })
  @Put('product/add')
  async createCartAndProductToIt(@Req() request, @Body() data): Promise<Cart> {
    let payload: any = {
      _id: '',
    };

    // Decode token to extract user id
    if (request.headers.authorization) {
      const token = request.headers.authorization.substring(7, request.headers.authorization.length);
      payload = this.jwtService.decode(token);
    }

    // Check if payload has user id in it
    if (payload._id) {
      return this.cartService.createCartAndAddProductToIt(data, payload._id);
    } else {
      return this.cartService.createCartAndAddProductToIt(data);
    }
  }

  @ApiOperation({ title: 'Add product to cart' })
  @ApiResponse({ status: 200, description: 'Return cart' })
  @Put('product/add/:cartId')
  async addProductToCart(@Param('cartId') cartId: string, @Body() cartProduct: AddProductToCartDto): Promise<Cart> {
    return this.cartService.addProductToCart(cartId, cartProduct);
  }

  @ApiBearerAuth()
  @ApiOperation({ title: 'Add user to cart' })
  @ApiResponse({ status: 200, description: 'Return cart' })
  @ApiResponse({ status: 400, description: 'Shopping Cart has already been assigned to the specified user' })
  @Patch('/:cartId/user')
  @UseGuards(AuthGuard('jwt'))
  async addUserIdToCart(@AuthUser() user: any, @Param('cartId') cartId: string): Promise<Cart> {
    return this.cartService.addUserIdToCart(user._id, cartId);
  }

  @ApiOperation({ title: 'Empty cart' })
  @ApiResponse({ status: 200, description: 'return cart' })
  @ApiResponse({ status: 400, description: '' })
  @Patch('/:cartId/empty')
  async emptyCart(@Param('cartId') cartId: string): Promise<Cart> {
    return this.cartService.emptyCart(cartId);
  }

  @ApiOperation({ title: 'Remove product variant from cart' })
  @ApiResponse({ status: 200, description: 'Return cart' })
  @Delete(':cartId/product/:variantId')
  async removeProductVariantFromCart(
    @Param('cartId') cartId: string,
    @Param('variantId') variantId: string,
  ): Promise<Cart> {
    return this.cartService.removeProductFromCart(cartId, variantId);
  }

  @ApiOperation({ title: 'Update cart products quantities' })
  @ApiResponse({ status: 200, description: 'Return cart' })
  @Patch(':cartId/product/:variantId')
  async modifyProductFromCart(
    @Param('cartId') cartId: string,
    @Param('variantId') variantId: string,
    @Body() product: any,
  ): Promise<Cart> {
    return this.cartService.modifyProductFromCart(cartId, variantId, product);
  }
}
