import { BadRequestException, HttpException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Cart } from '../../schemas/cart.schema';
import { getDataByLang } from '../../utils/lang.utils';
import { Product } from '../../schemas/product.schema';
import { AddProductToCartDto } from './dto/add-product-to-cart.dto';
import { ErrorsService } from '../../config/errors.service';

@Injectable()
export class CartService {
  constructor(
    @InjectModel('Cart') private cartModel: Model<Cart>,
    @InjectModel('Product') private productModel: Model<Product>,
    private error: ErrorsService,
  ) {}

  async findCartByUserId(userId: string, queryParams: any): Promise<Cart> {
    if (queryParams.lang) {
      let array = [
        await this.cartModel
          .findOne({ userId, status: 'active' })
          .populate('products.product')
          .lean({ virtuals: true }),
      ];
      if (array.length === 0) {
        throw new HttpException(this.error.get('CART_NOT_FOUND'), 400);
      }
      array = await getDataByLang(array, queryParams);
      return array[0];
    } else {
      const cart = await this.cartModel
        .findOne({ userId, status: 'active' })
        .populate('products.product')
        .lean({ virtuals: true });
      if (!cart) {
        throw new HttpException(this.error.get('CART_NOT_FOUND'), 400);
      }
      return cart;
    }
  }

  async addUserIdToCart(userId: string, cartId: string): Promise<Cart> {
    const cart = await this.cartModel.findById(cartId).populate('products.product');

    if (cart == null) {
      throw new HttpException(this.error.get('CART_NOT_FOUND'), 400);
    }
    // If cart has already assigned userId throw error
    if (cart.userId) {
      throw new HttpException(this.error.get('CART_HAS_ALREADY_ASSIGNED'), 400);
    }
    return this.cartModel.findByIdAndUpdate(cartId, { $set: { userId } }, { new: true }).populate('products.product');
  }

  async combineCarts(userId, firstCartId, targetCartId): Promise<Cart> {
    const initialCart = await this.cartModel
      .findById(firstCartId)
      .populate('products.product')
      .lean();
    const targetCart = await this.cartModel
      .findById(targetCartId)
      .populate('products.product')
      .lean();
    if (userId.toString() === targetCart.userId.toString()) {
      for (const product of initialCart.products) {
        const pos = targetCart.products.map(e => e.variant).indexOf(product.variant);
        if (pos >= 0) {
          targetCart.products[pos].quantity += product.quantity;
        } else {
          targetCart.products.push(product);
        }
      }
      for (const product of targetCart.products) {
        if (product.quantity > (await this.maxValue(product.variant))) {
          throw new HttpException(this.error.get('QUANTITY_PRODUCTS_EXCEEDED'), 400);
        }
      }
      await this.cartModel.findByIdAndDelete(firstCartId);
      return this.cartModel.findByIdAndUpdate(targetCartId, targetCart, { new: true }).populate('products.product');
    } else {
      throw new HttpException(this.error.get('NOT_HAVE_ACCES_TO_CART'), 400);
    }
  }

  async emptyCart(cartId: string): Promise<Cart> {
    try {
      return await this.cartModel
        .findByIdAndUpdate(cartId, { $set: { products: [] } }, { new: true })
        .populate('products.product');
    } catch (err) {
      throw new BadRequestException('Error while fetching the data on the database');
    }
  }

  async findCartById(cartId: string, queryParams: any): Promise<Cart> {
    if (queryParams.lang) {
      let array = [
        await this.cartModel
          .findById(cartId)
          .populate('products.product')
          .lean({ virtuals: true }),
      ];
      array = await getDataByLang(array, queryParams);
      return array[0];
    } else {
      return this.cartModel.findById(cartId).populate('products.product');
    }
  }

  async addProductToCart(cartId: string, product: AddProductToCartDto): Promise<Cart> {
    const maxValue = await this.maxValue(product.variant);
    const existProduct = await this.cartModel
      .findOne({
        _id: cartId,
        products: { $elemMatch: { variant: product.variant } },
      })
      .populate('products.product');
    if (existProduct) {
      const quantity =
        existProduct.products.filter(iterProduct => iterProduct.variant.toString() === product.variant.toString())[0]
          .quantity + product.quantity;
      if (quantity > maxValue) {
        throw new HttpException(this.error.get('QUANTITY_PRODUCTS_EXCEEDED'), 400);
      } else {
        return this.cartModel
          .findOneAndUpdate(
            {
              _id: cartId,
              products: { $elemMatch: { variant: product.variant } },
            },
            { $inc: { 'products.$.quantity': +product.quantity } },
            {
              new: true,
            },
          )
          .populate('products.product');
      }
    } else {
      if (product.quantity > maxValue) {
        throw new HttpException(this.error.get('QUANTITY_PRODUCTS_EXCEEDED'), 400);
      } else {
        return this.cartModel
          .findByIdAndUpdate(cartId, { $push: { products: product } }, { new: true })
          .populate('products.product');
      }
    }
  }

  async createCartAndAddProductToIt(data: any, userId?: string): Promise<Cart> {
    let newCart;
    if (data.quantity > (await this.maxValue(data.variant))) {
      throw new HttpException(this.error.get('QUANTITY_PRODUCTS_EXCEEDED'), 400);
    } else if (userId) {
      newCart = await this.cartModel.create({ userId, status: 'active' });
    } else {
      newCart = await this.cartModel.create({ status: 'active' });
    }
    return this.cartModel
      .findByIdAndUpdate(
        newCart._id,
        { $push: { products: data } },
        {
          new: true,
        },
      )
      .populate('products.product');
  }

  async removeProductFromCart(cartId: string, variantId: string): Promise<Cart> {
    return this.cartModel
      .findByIdAndUpdate(cartId, { $pull: { products: { variant: variantId } } }, { new: true })
      .populate('products.product');
  }

  async modifyProductFromCart(cartId: string, variantId: string, product: any): Promise<Cart> {
    if (product.quantity > (await this.maxValue(variantId))) {
      throw new HttpException(this.error.get('QUANTITY_PRODUCTS_EXCEEDED'), 400);
    } else {
      return this.cartModel
        .findOneAndUpdate(
          {
            _id: cartId,
            products: { $elemMatch: { variant: variantId } },
          },
          { $set: { 'products.$.quantity': product.quantity } },
          {
            new: true,
          },
        )
        .populate('products.product');
    }
  }

  async maxValue(variantId) {
    const value: any = await this.productModel.findOne({
      variants: { $elemMatch: { _id: variantId } },
    });
    return value.variants.filter(variant => variant._id.toString() === variantId.toString())[0].quantity;
  }
}
