export class CreateCartDto {
  readonly userId: string;
  readonly products: any[];
}
