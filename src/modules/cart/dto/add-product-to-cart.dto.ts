import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsMongoId, IsNotEmpty } from 'class-validator';

export class AddProductToCartDto {
  @IsMongoId()
  @ApiModelPropertyOptional()
  readonly variant: string;
  @ApiModelProperty()
  @IsNotEmpty()
  readonly product: string;
  @ApiModelProperty()
  @IsNotEmpty()
  readonly quantity: string;
}
