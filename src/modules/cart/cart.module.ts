import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UploadController } from '../upload/upload.controller';
import { CartController } from './cart.controller';
import { CartService } from './cart.service';
import { CartSchema } from '../../schemas/cart.schema';
import { ProductSchema } from '../../schemas/product.schema';
import { AuthModule } from '../auth/auth.module';
import { ErrorsService } from '../../config/errors.service';
import { UploadService } from '../upload/upload.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Cart', schema: CartSchema }]),
    MongooseModule.forFeature([{ name: 'Product', schema: ProductSchema }]),
    AuthModule,
  ],
  providers: [CartService, ErrorsService, UploadService],
  controllers: [CartController, UploadController],
})
export class CartModule {}
