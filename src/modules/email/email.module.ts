import { Module } from '@nestjs/common';
import { EmailService } from './email.service';
import { setApiKey } from '@sendgrid/mail';
import { ConfigService } from '../../config/config.service';
import { NestEmitterModule } from 'nest-emitter';
import { EventEmitter } from 'events';
import { ErrorsService } from '../../config/errors.service';

@Module({
  imports: [NestEmitterModule.forRoot(new EventEmitter())],
  controllers: [],
  providers: [ConfigService, EmailService, ErrorsService],
})
export class EmailModule {
  constructor(private config: ConfigService) {
    setApiKey(this.config.get('SENDGRID_API_KEY'));
  }
}
