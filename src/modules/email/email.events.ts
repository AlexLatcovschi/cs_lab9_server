import { StrictEventEmitter } from 'nest-emitter';
import { EventEmitter } from 'events';

interface AppEvents {
  orderEmail: (cart) => void;
  confirmationEmail: (user) => void;
  resetPasswordEmail: (user) => void;
  quoteNotification: (user) => void;
  resetPasswordConfirmEmail: (user) => void;
  contactEmail: (contactData) => void;
  newsletterSubscription: (contactData: any) => void;
}

export type EmailEvents = StrictEventEmitter<EventEmitter, AppEvents>;
