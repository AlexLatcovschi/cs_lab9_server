import { Injectable, Logger } from '@nestjs/common';
import { send } from '@sendgrid/mail';
import { User } from '../../schemas/user.schema';
import { InjectEventEmitter } from 'nest-emitter';
import { EmailEvents } from './email.events';
import { ContactEmailDto } from '../../utils/dto/contact-email.dto';
import { ConfigService } from '../../config/config.service';

@Injectable()
export class EmailService {
  private config: ConfigService;
  private readonly logger = new Logger(EmailService.name);

  constructor(@InjectEventEmitter() private readonly emitter: EmailEvents, config: ConfigService) {
    this.config = config;
    this.emitter.once('confirmationEmail', async (user: User) => this.sendConfirmationEmail(user));
    this.emitter.once('orderEmail', async cart => this.sendNewOrderEmail(cart));
    this.emitter.once('quoteNotification', async data => this.sendQuoteNotificationEmail(data));
    this.emitter.once('resetPasswordEmail', async (user: User) => this.sendResetPasswordEmail(user));
    this.emitter.once('resetPasswordConfirmEmail', async (user: User) => this.sendResetPasswordConfirmation(user));
    this.emitter.once('contactEmail', async (contactData: any) => this.sendContactEmail(contactData));
    this.emitter.once('newsletterSubscription', async (contactData: any) =>
      this.sendNewsletterSubscriptionConfirm(contactData),
    );
  }

  public async sendNewOrderEmail(order): Promise<any> {
    try {
      const templateVariables = {
        order: {
          buyerName: order.shipping.firstName + ' ' + order.shipping.lastName,
          id: order._id,
          date: order.createdAt.toLocaleString('en-GB', { timeZone: 'Europe/Kiev', hour12: false }),
          shipping: order.shipping,
        },
        products: [],
      };
      order.products.map((product, i) => {
        const newProduct = {
          index: (i + 1).toString(),
          description: order.products[i].title.ro,
          price: order.products[i].price,
          quantity: order.products[i].quantity,
          total: (order.products[i].price * order.products[i].quantity).toString(),
        };
        templateVariables.products.push(newProduct);
      });
      if (order.shipping && order.shipping.email) {
        const msg = {
          to: order.shipping.email,
          from: { name: 'RTI Moldova', email: this.config.get('FROM_EMAIL') },
          templateId: 'd-e3e88c2a8ebb41c39e2b3320c0a180e2',
          dynamic_template_data: templateVariables,
        };
        this.sendOrderNotificationEmail(order);
        return await send(msg);
      }
    } catch (err) {
      this.logger.error(err);
    }
  }

  public async sendConfirmationEmail(user: User): Promise<any> {
    try {
      const templateVariables = {
        activationUrl: `${this.config.get('APP_FULL_HOST')}/api/profile/activate-email?email=${user.email}&key=${
          user.activationToken
        }`,
        firstName: user.firstName,
        lastName: user.lastName,
      };
      const msg = {
        to: `${user.email}`,
        from: { name: 'Email Activation', email: this.config.get('FROM_EMAIL') },
        templateId: 'd-0f39a31b45884133a35482a38ccf0da7',
        dynamic_template_data: templateVariables,
      };
      console.log('msg: ', msg);
      return await send(msg);
    } catch (err) {
      this.logger.error(err);
    }
  }

  public async sendResetPasswordEmail(user: User): Promise<any> {
    try {
      const templateVariables = {
        resetUrl: `${this.config.get('CLIENT_URL')}/confirm-reset/${user.email}/${user.passwordResetToken}`,
        firstName: user.firstName,
        lastName: user.lastName,
      };
      const msg = {
        to: `${user.email}`,
        from: { name: 'RTI Moldova', email: this.config.get('FROM_EMAIL') },
        templateId: 'd-143e0f7e4285414f8548a245cc13a429',
        dynamic_template_data: templateVariables,
      };

      return await send(msg);
    } catch (err) {
      this.logger.error(err);
    }
  }

  public async sendResetPasswordConfirmation(user: User): Promise<any> {
    try {
      const templateVariables = {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
      };
      const msg = {
        to: `${user.email}`,
        from: { name: 'RTI Moldova', email: this.config.get('FROM_EMAIL') },
        templateId: 'd-7bf55785c6bf486484f8f48e5d7e9d7d',
        dynamic_template_data: templateVariables,
      };
      return await send(msg);
    } catch (err) {
      this.logger.error(err);
    }
  }

  public async sendOrderNotificationEmail(order) {
    try {
      const templateVariables = {
        order: {
          buyerName: order.shipping.firstName + ' ' + order.shipping.lastName,
          id: order._id,
          date: order.createdAt.toLocaleString('en-GB', { timeZone: 'Europe/Kiev', hour12: false }),
          shipping: order.shipping,
        },
        products: [],
      };
      order.products.map((product, i) => {
        const newProduct = {
          index: (i + 1).toString(),
          description: order.products[i].title.ro,
          price: order.products[i].price,
          quantity: order.products[i].quantity,
          total: (order.products[i].price * order.products[i].quantity).toString(),
        };
        templateVariables.products.push(newProduct);
      });
      const msg = {
        to: this.config.get('ORDERS_EMAIL'),
        from: { name: 'RTI Moldova', email: this.config.get('FROM_EMAIL') },
        templateId: 'd-e3e88c2a8ebb41c39e2b3320c0a180e2',
        dynamic_template_data: templateVariables,
      };
      return await send(msg);
    } catch (err) {
      this.logger.error(err);
    }
  }

  public async sendQuoteNotificationEmail(quote) {
    try {
      const templateVariables = {
        quote,
        dashboardUrl: this.config.get('DASHBOARD_URL') + '/pages/quotes/' + quote._id,
      };
      const msg = {
        to: this.config.get('QUOTES_EMAIL'),
        from: { name: 'RTI Moldova', email: this.config.get('FROM_EMAIL') },
        templateId: 'd-7d45bf2e551e4c6087cacf9f9b8966cc',
        dynamic_template_data: templateVariables,
      };
      return await send(msg);
    } catch (err) {
      this.logger.error(err);
    }
  }

  public async sendContactEmail(contactData: ContactEmailDto) {
    try {
      const noInfo = '[No Info]';
      const msg = {
        to: this.config.get('CONTACT_EMAIL'),
        from: { name: 'RTI Moldova', email: this.config.get('FROM_EMAIL') },
        subject: `New contact email - ${contactData.type}`,
        text: `Name: ${contactData.name};
               Email: ${contactData.email};
               Phone: ${contactData.phone ? contactData.phone : noInfo};
               Company: ${contactData.company ? contactData.company : noInfo};
               Text: ${contactData.message}`,
      };
      return await send(msg);
    } catch (err) {
      this.logger.error(err);
    }
  }

  public async sendNewsletterSubscriptionConfirm(user: User): Promise<any> {
    try {
      const msg = {
        to: `${user.email}`,
        from: { name: 'RTI Moldova', email: this.config.get('FROM_EMAIL') },
        templateId: 'd-0b8bfba65e884c2e86444ead610fb5ca',
      };
      return await send(msg);
    } catch (err) {
      this.logger.error(err);
    }
  }
}
