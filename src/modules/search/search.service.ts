import { BadRequestException, Injectable } from '@nestjs/common';
import * as crypto from 'crypto';
import { InjectEventEmitter } from 'nest-emitter';
import { EmailEvents } from '../email/email.events';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Product } from '../../schemas/product.schema';
import { Tag } from '../../schemas/tag.schema';
import { ProductCategory } from '../../schemas/product-category.schema';
import { PaginateModel } from 'mongoose-paginate-v2';

import { SearchResultDto } from './dto/search-result.dto';
import { getDataByLang } from '../../utils/lang.utils';
import { Article } from '../../schemas/article.schema';
import { ArticleCategory } from '../../schemas/article-category.schema';
import { ArticleSearchQueryParamsDto } from './dto/article-search-queryParams.dto';

@Injectable()
export class SearchService {
  constructor(
    @InjectModel('Product') private productModel: Model<Product>,
    @InjectModel('Article') private articleModel: PaginateModel<Article>,
    @InjectModel('ArticleCategory') private articleCategoryModel: Model<ArticleCategory>,
  ) {}
  public async productsSearch(lang: string, queryParams: any): Promise<Product[]> {
    let result: any[];
    const titleQuery = {};
    const descriptionQuery = {};
    if (lang && queryParams.search) {
      titleQuery['title.' + lang] = { $regex: queryParams.search, $options: 'i' };
      descriptionQuery['description.' + lang] = { $regex: queryParams.search, $options: 'i' };
    }
    result = await this.productModel
      .find({ $or: [titleQuery, descriptionQuery] })
      .populate('tags', 'name slug')
      .populate('category')
      .populate({ path: 'attributes.attributeId', select: 'name _id unit', populate: { path: 'unit' } })
      .populate('facilities')
      .populate('variants.attributes.attributeId', 'name _id unit')
      .lean();
    result = await getDataByLang(result, { lang });
    return result;
  }

  public async articlesSearch(lang: string, queryParams: ArticleSearchQueryParamsDto): Promise<Article[]> {
    const options: any = {
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
      populate: 'tags author productCategories category',
      lean: true,
    };
    let result: Article[];
    let categories = [];
    const categoryQueries = [];
    const titleQuery = {};
    const descriptionQuery = {};
    const preambleQuery = {};
    let generalQuery = {};

    if (lang && queryParams.search) {
      titleQuery['title.' + lang] = { $regex: queryParams.search, $options: 'i' };
      descriptionQuery['content.' + lang] = { $regex: queryParams.search, $options: 'i' };
      preambleQuery['preamble.' + lang] = { $regex: queryParams.search, $options: 'i' };
      categories = await this.articleCategoryModel
        .find({ ['name.' + lang]: { $regex: queryParams.search, $options: 'i' } })
        .lean();
    }
    if (categories.length > 0) {
      for (const category of categories) {
        categoryQueries.push({ category });
      }
      generalQuery = { $or: [titleQuery, descriptionQuery, preambleQuery, ...categoryQueries] };
    } else {
      generalQuery = { $or: [titleQuery, descriptionQuery, preambleQuery] };
    }
    result = await this.articleModel.paginate(generalQuery, options);
    result = await getDataByLang(result, { lang });
    return result;
  }
}
export const generateToken = (): string => {
  return crypto.randomBytes(16).toString('hex');
};
