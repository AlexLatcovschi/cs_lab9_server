import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class ArticleSearchQueryParamsDto {
  @ApiModelProperty()
  search: string;

  @ApiModelProperty()
  page: string;

  @ApiModelPropertyOptional()
  limit: string;
}
