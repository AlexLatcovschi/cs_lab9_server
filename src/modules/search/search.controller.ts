import { Body, Controller, Get, HttpCode, Param, Post, Query } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { SearchService } from './search.service';
import { Product } from '../../schemas/product.schema';
import { Article } from '../../schemas/article.schema';
import { ArticleSearchQueryParamsDto } from './dto/article-search-queryParams.dto';

@ApiUseTags('Search')
@Controller('search')
export class SearchController {
  constructor(private searchService: SearchService) {}

  @Get('/products/global/:lang')
  public async searchFromProducts(@Param('lang') lang: string, @Query() query: any): Promise<Product[]> {
    return this.searchService.productsSearch(lang, query);
  }

  @Get('/articles/global/:lang')
  public async searchFromArticles(
    @Param('lang') lang: string,
    @Query() query: ArticleSearchQueryParamsDto,
  ): Promise<Article[]> {
    return this.searchService.articlesSearch(lang, query);
  }
}
