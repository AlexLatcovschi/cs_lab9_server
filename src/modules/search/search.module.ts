import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SearchController } from './search.controller';
import { UsersService } from '../users/users.service';
import { ProductSchema } from '../../schemas/product.schema';
import { SearchService } from './search.service';
import { TagSchema } from '../../schemas/tag.schema';
import { ProductCategorySchema } from '../../schemas/product-category.schema';
import { ArticleSchema } from '../../schemas/article.schema';
import { ArticleCategorySchema } from '../../schemas/article-category.schema';
import { ErrorsService } from '../../config/errors.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Product', schema: ProductSchema }]),
    MongooseModule.forFeature([{ name: 'Article', schema: ArticleSchema }]),
    MongooseModule.forFeature([{ name: 'ArticleCategory', schema: ArticleCategorySchema }]),
  ],
  controllers: [SearchController],
  providers: [SearchService, ErrorsService],
})
export class SearchModule {}
