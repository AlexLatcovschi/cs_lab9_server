import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class UserPhotoDto {
  @ApiModelPropertyOptional()
  description: string;
  @ApiModelPropertyOptional()
  path: string;
  @ApiModelPropertyOptional()
  mimetype: string;
}
