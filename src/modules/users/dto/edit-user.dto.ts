import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { UserPhotoDto } from './user-photo.dto';

export class EditUserDto {
  @ApiModelPropertyOptional()
  readonly firstName: string;
  @ApiModelPropertyOptional()
  readonly lastName: string;
  @ApiModelProperty()
  readonly description: string;
}
