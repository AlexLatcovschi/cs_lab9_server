import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsEmail } from 'class-validator';
import { UserPhotoDto } from './user-photo.dto';

export class CreateUserDto {
  @ApiModelProperty()
  readonly firstName: string;
  @ApiModelProperty()
  readonly lastName: string;
  @ApiModelProperty()
  readonly description: string;
  @ApiModelPropertyOptional({ description: 'User photo info' })
  readonly userPhoto: UserPhotoDto;
  @ApiModelProperty()
  @IsEmail()
  readonly email: string;
  @ApiModelProperty()
  readonly password: string;
  activationToken: string;
}
