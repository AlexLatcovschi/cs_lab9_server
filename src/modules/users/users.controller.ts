import { Body, Controller, Get, Param, Post, Put, Delete, Query, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from '../../schemas/user.schema';
import { AuthGuard } from '@nestjs/passport';
import { AuthUser } from '../../decorators/user.decorator';
import { Roles } from '../../decorators/roles.decorator';
import { ApiBearerAuth, ApiOperation, ApiUseTags } from '@nestjs/swagger';
import { EditUserDto } from './dto/edit-user.dto';

@ApiUseTags('User')
@ApiBearerAuth()
@Controller('users')
@UseGuards(AuthGuard('jwt'))
@Roles('admin')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @ApiOperation({ title: 'Get my user details' })
  @Get('/me')
  async geyMyUserProfile(@AuthUser() user: any): Promise<User> {
    return this.usersService.getMyProfile(user._id);
  }

  @ApiOperation({ title: 'Update my user data' })
  @Put('/me/edit')
  async UpdateMyProfile(@AuthUser() user: any, @Body() data: EditUserDto): Promise<User> {
    return this.usersService.findByIdAndUpdate(user._id, data);
  }

  @ApiOperation({ title: 'Find all users' })
  @Get('/')
  @Roles('admin')
  async findAll(@Query() query): Promise<User[]> {
    return this.usersService.findAll(query);
  }

  @ApiOperation({ title: 'Find user by Id' })
  @Get('/:id')
  @Roles('admin')
  async findById(@Param('id') id: string): Promise<User> {
    return this.usersService.findById(id);
  }

  @ApiOperation({ title: 'Create new user' })
  @Post()
  @Roles('admin')
  async create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @ApiOperation({ title: 'Admin - update user - add new shipping data' })
  @Put('/:id')
  @Roles('admin')
  async findByIdAndUpdate(@Param('id') id: string, @Body() data): Promise<User> {
    return this.usersService.findByIdAndUpdate(id, data);
  }

  @ApiOperation({ title: 'Admin - delete user' })
  @Delete('/:id')
  @Roles('admin')
  async findByIdAndDelete(@Param('id') id: string): Promise<User> {
    return this.usersService.findByIdAndDelete(id);
  }

  @ApiOperation({ title: 'Add new shipping data' })
  @Put('/shipping/add')
  async addShippingDetail(@AuthUser() user: any, @Body() data): Promise<User> {
    return this.usersService.addShipping(user._id, data);
  }

  @ApiOperation({ title: 'Update shipping data by shipping id' })
  @Put('/shipping/modify/:shippingId')
  async updateShippingDetail(
    @AuthUser() user: any,
    @Param('shippingId') shippingId: string,
    @Body() data,
  ): Promise<User> {
    return this.usersService.updateShipping(user._id, data, shippingId);
  }

  @ApiOperation({ title: 'Remove shipping data by shipping id' })
  @Delete('/shipping/remove/:shippingId')
  async removeShippingDetail(
    @AuthUser() user: any,
    @Param('shippingId') shippingId: string,
    @Body() data,
  ): Promise<User> {
    return this.usersService.removeShipping(user._id, data, shippingId);
  }
}
