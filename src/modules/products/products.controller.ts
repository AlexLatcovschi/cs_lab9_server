import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Query,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/create-product.dto';
import { Product } from '../../schemas/product.schema';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { ProductFindQuery } from './dto/ProductFind.query';
import { AbstractLangQuery } from '../../utils/abstract/AbstractLang.query';
import { FileInterceptor } from '@nestjs/platform-express';
import { Roles } from '../../decorators/roles.decorator';

@ApiUseTags('Product')
@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Get()
  async findAll(@Query() query: ProductFindQuery): Promise<Product[]> {
    return this.productsService.findAll(query);
  }

  @ApiBearerAuth()
  @Get('/export-as-csv')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async exportAsCSV(@Res() res) {
    return this.productsService.exportAsCSV(res);
  }

  @ApiBearerAuth()
  @Post('/import-from-csv')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  @UseInterceptors(FileInterceptor('csv'))
  async importFromCSV(@UploadedFile() file) {
    return this.productsService.importFromCSV(file);
  }

  @Get('/:id')
  async findById(@Param('id') id: string, @Query() query: AbstractLangQuery): Promise<Product> {
    return this.productsService.findById(id, query);
  }

  @Get('/tag/:tag')
  async findByTag(@Param('tag') tag: string, @Query() queryParams: any): Promise<any> {
    return this.productsService.findByTag(tag, queryParams);
  }

  @Get('/slug/:slug')
  async findBySlug(@Param('slug') slug: string, @Query() query: any): Promise<Product> {
    return this.productsService.findBySlug(slug, query);
  }

  @ApiBearerAuth()
  @Post()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async create(@Body() createProductDto: CreateProductDto) {
    console.log('AAAAAAAAAAA: ', createProductDto);
    return this.productsService.create(createProductDto);
  }

  @ApiBearerAuth()
  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async findByIdAndUpdate(@Param('id') id: string, @Body() product): Promise<Product> {
    return this.productsService.findByIdAndUpdate(id, product);
  }

  @ApiBearerAuth()
  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async remove(@Param('id') id: string) {
    return this.productsService.remove(id);
  }

  @ApiBearerAuth()
  @Patch('/regenerate-id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async regenerateIds() {
    return this.productsService.regenerateIds();
  }
}
