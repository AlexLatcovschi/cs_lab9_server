import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateProductDto } from './dto/create-product.dto';
import { PaginateModel } from 'mongoose-paginate-v2';
import { Product } from '../../schemas/product.schema';
import { Tag } from '../../schemas/tag.schema';
import { Model } from 'mongoose';
import { generateSlugByName, generateVariantSlug } from '../../utils/generate-slug.utils';
import { getDataByLang } from '../../utils/lang.utils';
import { Language } from '../../schemas/language.schema';
import { AbstractLangQuery } from '../../utils/abstract/AbstractLang.query';
import { ProductFindQuery } from './dto/ProductFind.query';
import { Widget } from '../../schemas/widget.schema';
import { Attribute } from '../../schemas/attribute.schema';

import * as json2csv from 'json2csv';
import * as csv2json from 'csvtojson';
import { ProductCategory } from '../../schemas/product-category.schema';
import { Cart } from '../../schemas/cart.schema';
import { Order } from '../../schemas/order.schema';

@Injectable()
export class ProductsService {
  constructor(
    @InjectModel('Product') private productModel: PaginateModel<Product>,
    @InjectModel('ProductCategory') private categoryModel: Model<ProductCategory>,
    @InjectModel('Tag') private tagModel: Model<Tag>,
    @InjectModel('Cart') private cartModel: Model<Cart>,
    @InjectModel('Order') private orderModel: Model<Order>,
    @InjectModel('Language') private langModel: Model<Language>,
    @InjectModel('Widget') private widgetModel: Model<Widget>,
    @InjectModel('Attribute') private attributeModel: Model<Attribute>,
  ) {}

  async findById(id: string, query?: AbstractLangQuery): Promise<Product> {
    let res = await this.productModel
      .findById(id)
      .lean()
      .populate('tags', 'name slug')
      .populate('facilities')
      .populate({
        path: 'category',
        populate: { path: 'parent' },
      })
      .populate('relatedProducts')
      .populate({
        path: 'attributes.attributeId',
        select: 'name _id unit type attributeGroup order',
        populate: { path: 'unit attributeGroup' },
      })
      .populate('attributes.attributeId.unit')
      .populate('variants.attributes.attributeId', 'name _id unit type attributeGroup order');
    if (query && query.lang && res) {
      res.description = res.description[query.lang];
      res.title = res.title[query.lang];
    }
    if (res && res.widgets && Object.keys(res.widgets).length > 0) {
      res = await this.populateWithWidgets(res);
    }
    if (query && query.lang) {
      getDataByLang(res, query);
    }
    return res;
  }

  async findBySlug(slug: string, query): Promise<Product> {
    let res = await this.productModel
      .findOne({ slug })
      .populate('tags', 'name slug')
      .populate('relatedProducts')
      .populate({
        path: 'category',
        populate: { path: 'parent' },
      })
      .populate({
        path: 'attributes.attributeId',
        select: 'name _id unit type attributeGroup order',
        populate: { path: 'unit attributeGroup' },
      })
      .populate('facilities')
      .populate('variants.attributes.attributeId', 'name _id unit type attributeGroup order')
      .lean();

    if (res && res && res.widgets && Object.keys(res.widgets).length > 0) {
      res = await this.populateWithWidgets(res);
    }
    if (query.lang) {
      return getDataByLang(res, query);
    }
    return res;
  }

  async findByTag(slug: string, queryParams: any): Promise<Product[]> {
    const tags = await this.tagModel.findOne({ slug });
    if (tags) {
      const query = { tags };
      const options: any = {
        lean: true,
        page: parseInt(queryParams.page, 10) || 1,
        limit: parseInt(queryParams.limit, 10) || 10,
        sort: { createdAt: -1 },
        populate: [
          'category',
          'tags',
          {
            path: 'attributes.attributeId',
            select: 'name _id unit type attributeGroup order',
            populate: { path: 'unit attributeGroup' },
          },
          'facilities',
          { path: 'variants.attributes.attributeId', select: 'name _id unit type attributeGroup order' },
        ],
      };
      if (Object.keys(queryParams).length === 0) {
        return this.productModel
          .find({ tags })
          .populate('tags', 'name slug')
          .populate('category')
          .populate({
            path: 'attributes.attributeId',
            select: 'name _id unit type attributeGroup order',
            populate: { path: 'unit attributeGroup' },
          })
          .populate('facilities')
          .populate('variants.attributes.attributeId', 'name _id unit type attributeGroup order');
      }
      if (queryParams.lang && !queryParams.page) {
        let products = await this.productModel
          .find({ tags })
          .populate('tags', 'name slug')
          .populate('category')
          .populate({
            path: 'attributes.attributeId',
            select: 'name _id unit type attributeGroup order',
            populate: { path: 'unit attributeGroup' },
          })
          .populate('facilities')
          .populate('variants.attributes.attributeId', 'name _id unit type attributeGroup order')
          .lean();
        products = await getDataByLang(products, queryParams);
        return products;
      }
      const paginateData = await this.productModel.paginate(query, options);
      return getDataByLang(paginateData, queryParams);
    } else {
      return [];
    }
  }

  async findManyByCategory(categoryId: string, queryParams: ProductFindQuery): Promise<Product[]> {
    const subcategories = await this.categoryModel.find({ parent: categoryId });
    const query = { $or: [{ category: categoryId }] };
    if (subcategories.length > 0) {
      for (const subcategory of subcategories) {
        await query.$or.push({ category: subcategory._id.toString() });
      }
    }
    const options: any = {
      lean: true,
      page: queryParams.page || 1,
      limit: queryParams.limit || 10,
      sort: { createdAt: -1 },
      populate: [
        'tags',
        {
          path: 'attributes.attributeId',
          select: 'name _id unit type attributeGroup order',
          populate: { path: 'unit attributeGroup' },
        },
        'facilities',
        { path: 'variants.attributes.attributeId', select: 'name _id unit type attributeGroup order' },
      ],
    };

    await this.addFilters(queryParams, query);
    const paginateData = await this.productModel.paginate(query, options);
    if (queryParams.price) {
      for (const product of paginateData.docs) {
        const priceRange = queryParams.price.split('-');
        product.variants = product.variants.filter(
          variant => parseInt(priceRange[0], 10) <= variant.price && variant.price <= parseInt(priceRange[1], 10),
        );
      }
    }
    return getDataByLang(paginateData, queryParams);
  }

  async create(createProductDto: CreateProductDto): Promise<Product> {
    let createdProduct = new this.productModel(createProductDto);
    createdProduct = await generateSlugByName(createdProduct, 'title');
    await this.validateSlug(null, createdProduct);
    createdProduct.variants = await generateVariantSlug(createdProduct.variants);
    return this.productModel.create(createdProduct);
  }

  async findByIdAndUpdate(id: string, product): Promise<Product> {
    product = await generateSlugByName(product, 'title');
    product.variants = await generateVariantSlug(product.variants);
    await this.validateSlug(id, product);
    return this.productModel.findByIdAndUpdate(id, product).populate('facilities');
  }

  public async validateSlug(currentId, product) {
    const dbProduct = await this.productModel.findOne({ slug: product.slug });
    if (dbProduct && dbProduct._id.toString() !== currentId.toString()) {
      throw new BadRequestException(`Product with this name already exists`);
    }
  }

  async findAll(queryParams: any): Promise<Product[]> {
    const query: any = {};
    const options: any = {
      lean: true,
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
      populate: 'category',
      sort: { createdAt: -1 },
    };

    if (queryParams._sort) {
      options.sort = {
        [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
      };
    }

    if (queryParams._sort) {
      options.sort = {
        [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
      };
    }

    if (Object.keys(queryParams).length === 0) {
      return this.productModel
        .find(query)
        .populate('tags', 'name slug')
        .populate('category')
        .populate({
          path: 'attributes.attributeId',
          select: 'name _id unit type attributeGroup order',
          populate: { path: 'unit attributeGroup' },
        })
        .populate('facilities')
        .populate('variants.attributes.attributeId', 'name _id unit type attributeGroup order')
        .sort({ createdAt: -1 });
    }

    if (queryParams.lang && !queryParams.page) {
      let array = await this.productModel
        .find(query)
        .lean()
        .populate('tags', 'name slug')
        .populate('category')
        .populate({
          path: 'attributes.attributeId',
          select: 'name _id unit type attributeGroup order',
          populate: { path: 'unit attributeGroup' },
        })
        .populate('facilities')
        .populate('variants.attributes.attributeId', 'name _id unit type attributeGroup order');
      array = await getDataByLang(array, queryParams);
      return array;
    }

    if (queryParams.title) {
      query['title.' + queryParams.lang] = { $regex: queryParams.title, $options: 'i' };
    }

    const paginateData = await this.productModel.paginate(query, options);
    return getDataByLang(paginateData, queryParams);
  }

  async remove(id: string): Promise<Product> {
    return this.productModel.findByIdAndDelete(id);
  }

  async populateWithWidgets(response) {
    for (const lang in response.widgets) {
      if (response.widgets.hasOwnProperty(lang)) {
        if (response.widgets[lang] && Array.isArray(response.widgets[lang])) {
          await Promise.all(
            response.widgets[lang].map(async (el, index) => {
              try {
                let widget = await this.widgetModel.findById(el).populate('template');
                widget = await this.populateDualWidget(widget);
                widget = await this.populateBackgroundWidget(widget);
                return (response.widgets[lang][index] = widget);
              } catch (err) {
                throw err;
              }
            }),
          );
        }
      }
    }
    return response;
  }

  async populateBackgroundWidget(widget) {
    if (widget && widget.template && widget.template.type === 'widget-background') {
      for (const data of widget.payload.widgets) {
        let newWidget = await this.widgetModel
          .findById(data.widget_id)
          .populate('template')
          .lean(true);
        newWidget = await this.populateDualWidget(newWidget);
        data.content = newWidget;
      }
    }
    return widget;
  }

  async populateDualWidget(widget) {
    if (widget && widget.template && widget.template.type === 'dual-widget') {
      widget.payload.left.content = await this.widgetModel.findById(widget.payload.left.widget_id).populate('template');
      widget.payload.right.content = await this.widgetModel
        .findById(widget.payload.right.widget_id)
        .populate('template');
      widget = await this.populateBackgroundWidget(widget);
    }
    return widget;
  }

  async addFilters(queryParams, query) {
    for (const key in queryParams) {
      if (queryParams.hasOwnProperty(key)) {
        if (key !== 'lang' && key !== 'limit' && key !== 'page') {
          const valueRange =
            queryParams[key] instanceof Array ? queryParams[key].join(',') : queryParams[key].split('-');
          const multipleValues =
            queryParams[key] instanceof Array ? queryParams[key].join(',') : queryParams[key].split(',');
          if (key === 'price') {
            query.variants = {
              $elemMatch: {
                price: {
                  $gt: parseInt(valueRange[0], 10) - 1,
                  $lt: parseInt(valueRange[1], 10) + 1,
                },
              },
            };
          } else {
            const multipleQuery = { $or: [] };
            for (const value of multipleValues) {
              multipleQuery.$or.push({ ['value.' + queryParams.lang]: { $regex: value, $options: 'i' } });
            }
            const attribute = await this.attributeModel.findOne({
              ['name.' + queryParams.lang]: key,
            });
            const finalQuery =
              valueRange.length === 1
                ? multipleValues.length === 1
                  ? {
                      ['value.' + queryParams.lang]: {
                        $regex: valueRange[0],
                        $options: 'i',
                      },
                    }
                  : multipleQuery
                : {
                    ['value.' + queryParams.lang]: {
                      $gt: parseInt(valueRange[0], 10) - 1,
                      $lt: parseInt(valueRange[1], 10) + 1,
                    },
                  };
            query.$or = [
              {
                variants: {
                  $elemMatch: { attributes: { $elemMatch: { $and: [{ attributeId: attribute._id }, finalQuery] } } },
                },
              },
              { attributes: { $elemMatch: { $and: [{ attributeId: attribute._id }, finalQuery] } } },
            ];
          }
        }
      }
    }
    return query;
  }

  async exportAsCSV(res) {
    let products = await this.productModel.find().lean();
    products = products.map(product => {
      product._id = product._id.toString();
      return product;
    });

    const filename = 'products.csv';
    res.setHeader('Content-Type', 'text/csv');
    res.setHeader('Content-Disposition', 'attachment; filename=' + filename);
    const fields = ['_id', 'title.ro', 'description.ro', 'slug', 'createdAt'];
    const json2csvParser = new json2csv.Parser({ fields });
    const csv = json2csvParser.parse(products);
    res.status(200).send(csv);
  }

  async importFromCSV(file) {
    // JSON.parse();
    csv2json()
      .fromString(file.buffer.toString())
      .then((res: any) => {
        res = JSON.stringify(res);
        res = res.replace(/""/g, null);
        res = JSON.parse(res);
        for (const product of res) {
          this.productModel.create(product);
        }
        // for (const product of res) {
        //   for (const field in product) {
        //     if (product.hasOwnProperty(field)) {
        //       if (product[field] && (product[field][0] === '{' || product[field][0] === '[')) {
        //         product[field] = JSON.parse(product[field]);
        //       }
        //       if (product[field] === '') {
        //         product[field] = null;
        //       }
        //     }
        //   }
        // }
        // const stringified = JSON.stringify(res).toString();
        // const parsed = JSON.parse(stringified);
        // for (const product of parsed) {
        //   console.log(product);
        // }
      });
    // return csv2json(file.buffer.toString(), (err, data) => {
    //   if (data) {
    //     console.log('true');
    //     for (const product of data) {
    //       console.log('true');
    //
    //       product.category = product.category.replace(/"/g,'');
    //       console.log(product);
    //
    //     }
    //   }
    // });
  }

  public async regenerateIds(): Promise<Product[]> {
    const result = await this.productModel.find().lean(true);
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < result.length; i++) {
      if (result[i].attributes) {
        // tslint:disable-next-line:prefer-for-of
        for (let j = 0; j < result[i].attributes.length; j++) {
          delete result[i].attributes[j]._id;
        }
      }
      if (result[i].variants) {
        // tslint:disable-next-line:prefer-for-of
        for (let j = 0; j < result[i].variants.length; j++) {
          delete result[i].variants[j]._id;
        }
      }
      await this.productModel.findByIdAndUpdate(result[i]._id, result[i]);
    }
    await this.orderModel.deleteMany({});
    await this.cartModel.deleteMany({});
    return result;
  }
}
