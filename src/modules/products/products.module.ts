import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductSchema } from '../../schemas/product.schema';
import { UploadController } from '../upload/upload.controller';
import { TagSchema } from '../../schemas/tag.schema';
import { LanguageSchema } from '../../schemas/language.schema';
import { WidgetSchema } from '../../schemas/widget.schema';
import { AttributeSchema } from '../../schemas/attribute.schema';
import { ErrorsService } from '../../config/errors.service';
import { ProductCategorySchema } from '../../schemas/product-category.schema';
import { CartSchema } from '../../schemas/cart.schema';
import { OrderSchema } from '../../schemas/order.schema';
import { UploadService } from '../upload/upload.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Product', schema: ProductSchema },
      { name: 'ProductCategory', schema: ProductCategorySchema },
      { name: 'Tag', schema: TagSchema },
      { name: 'Cart', schema: CartSchema },
      { name: 'Order', schema: OrderSchema },

      { name: 'Language', schema: LanguageSchema },
      { name: 'Widget', schema: WidgetSchema },
      { name: 'Attribute', schema: AttributeSchema },
    ]),
  ],
  providers: [ProductsService, ErrorsService, UploadService],
  controllers: [ProductsController, UploadController],
  exports: [ProductsService],
})
export class ProductsModule {}
