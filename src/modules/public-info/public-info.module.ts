import { Module } from '@nestjs/common';
import { PublicInfoService } from './public-info.service';
import { PublicInfoController } from './public-info.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UploadController } from '../upload/upload.controller';
import { TagSchema } from '../../schemas/tag.schema';
import { LanguageSchema } from '../../schemas/language.schema';
import { ErrorsService } from '../../config/errors.service';
import { UploadService } from '../upload/upload.service';
import { PublicInfoSchema } from '../../schemas/PublicInfo.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'PublicInfo', schema: PublicInfoSchema },
      { name: 'Tag', schema: TagSchema },
      { name: 'Language', schema: LanguageSchema },
    ]),
  ],
  providers: [PublicInfoService, ErrorsService, UploadService],
  controllers: [PublicInfoController, UploadController],
  exports: [PublicInfoService],
})
export class PublicInfoModule {}
