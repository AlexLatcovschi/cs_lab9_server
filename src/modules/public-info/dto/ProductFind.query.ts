import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { AbstractPaginationQuery } from '../../../utils/abstract/AbstractPagination.query';

export class ProductFindQuery extends AbstractPaginationQuery {
  @ApiModelPropertyOptional()
  public readonly categoryId?: string;

  @ApiModelPropertyOptional()
  public readonly price?: string;
}
