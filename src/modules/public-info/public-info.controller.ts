import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Query,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { PublicInfoService } from './public-info.service';
import { CreatePublicInfoDto } from './dto/create-public-info.dto';
import { Product } from '../../schemas/product.schema';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { ProductFindQuery } from './dto/ProductFind.query';
import { AbstractLangQuery } from '../../utils/abstract/AbstractLang.query';
import { FileInterceptor } from '@nestjs/platform-express';
import { Roles } from '../../decorators/roles.decorator';
import { PublicInfo } from '../../schemas/PublicInfo.schema';

@ApiUseTags('PublicInfo')
@Controller('public-info')
export class PublicInfoController {
  constructor(private readonly service: PublicInfoService) {}

  @Get()
  async findAll(@Query() query: ProductFindQuery): Promise<PublicInfo[]> {
    return this.service.findAll(query);
  }

  @Get('/:id')
  async findById(@Param('id') id: string, @Query() query: AbstractLangQuery): Promise<PublicInfo> {
    return this.service.findById(id, query);
  }

  @Get('/slug/:slug')
  async findBySlug(@Param('slug') slug: string, @Query() query: any): Promise<PublicInfo> {
    return this.service.findBySlug(slug, query);
  }

  @ApiBearerAuth()
  @Post()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async create(@Body() createProductDto: CreatePublicInfoDto) {
    console.log('AAAAAAAAAAA: ', createProductDto);
    return this.service.create(createProductDto);
  }

  @ApiBearerAuth()
  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async remove(@Param('id') id: string) {
    return this.service.remove(id);
  }
}
