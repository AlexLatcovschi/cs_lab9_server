import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreatePublicInfoDto } from './dto/create-public-info.dto';
import { PaginateModel } from 'mongoose-paginate-v2';
import { Product } from '../../schemas/product.schema';
import { Tag } from '../../schemas/tag.schema';
import { Model } from 'mongoose';
import { generateSlugByName, generateVariantSlug } from '../../utils/generate-slug.utils';
import { getDataByLang } from '../../utils/lang.utils';
import { Language } from '../../schemas/language.schema';
import { AbstractLangQuery } from '../../utils/abstract/AbstractLang.query';
import { ProductFindQuery } from './dto/ProductFind.query';
import { Widget } from '../../schemas/widget.schema';
import { Attribute } from '../../schemas/attribute.schema';

import * as json2csv from 'json2csv';
import * as csv2json from 'csvtojson';
import { ProductCategory } from '../../schemas/product-category.schema';
import { Cart } from '../../schemas/cart.schema';
import { Order } from '../../schemas/order.schema';
import { PublicInfo } from '../../schemas/PublicInfo.schema';

@Injectable()
export class PublicInfoService {
  constructor(
    @InjectModel('PublicInfo') private publicInfoModel: PaginateModel<PublicInfo>,
    @InjectModel('Language') private langModel: Model<Language>,
  ) {}

  async findById(id: string, query?: AbstractLangQuery): Promise<PublicInfo> {
    const res = await this.publicInfoModel
      .findById(id)
      .lean()
      .populate({
        path: 'category',
        populate: { path: 'parent' },
      })
      .populate('relatedProducts')
      .populate({
        path: 'attributes.attributeId',
        select: 'name _id unit type attributeGroup order',
        populate: { path: 'unit attributeGroup' },
      })
      .populate('attributes.attributeId.unit')
      .populate('variants.attributes.attributeId', 'name _id unit type attributeGroup order');
    if (query && query.lang && res) {
      res.description = res.description[query.lang];
      res.title = res.title[query.lang];
    }
    if (query && query.lang) {
      getDataByLang(res, query);
    }
    return res;
  }

  async findBySlug(slug: string, query): Promise<PublicInfo> {
    const res = await this.publicInfoModel
      .findOne({ slug })
      .populate('tags', 'name slug')
      .populate('relatedProducts')
      .populate({
        path: 'category',
        populate: { path: 'parent' },
      })
      .populate({
        path: 'attributes.attributeId',
        select: 'name _id unit type attributeGroup order',
        populate: { path: 'unit attributeGroup' },
      })
      .populate('facilities')
      .populate('variants.attributes.attributeId', 'name _id unit type attributeGroup order')
      .lean();
    if (query.lang) {
      return getDataByLang(res, query);
    }
    return res;
  }

  // async findManyByCategory(categoryId: string, queryParams: ProductFindQuery): Promise<Product[]> {
  //   const subcategories = await this.categoryModel.find({ parent: categoryId });
  //   const query = { $or: [{ category: categoryId }] };
  //   if (subcategories.length > 0) {
  //     for (const subcategory of subcategories) {
  //       await query.$or.push({ category: subcategory._id.toString() });
  //     }
  //   }
  //   const options: any = {
  //     lean: true,
  //     page: queryParams.page || 1,
  //     limit: queryParams.limit || 10,
  //     sort: { createdAt: -1 },
  //     populate: [
  //       'tags',
  //       {
  //         path: 'attributes.attributeId',
  //         select: 'name _id unit type attributeGroup order',
  //         populate: { path: 'unit attributeGroup' },
  //       },
  //       'facilities',
  //       { path: 'variants.attributes.attributeId', select: 'name _id unit type attributeGroup order' },
  //     ],
  //   };
  //
  //   await this.addFilters(queryParams, query);
  //   const paginateData = await this.publicInfoModel.paginate(query, options);
  //   if (queryParams.price) {
  //     for (const product of paginateData.docs) {
  //       const priceRange = queryParams.price.split('-');
  //       product.variants = product.variants.filter(
  //         variant => parseInt(priceRange[0], 10) <= variant.price && variant.price <= parseInt(priceRange[1], 10),
  //       );
  //     }
  //   }
  //   return getDataByLang(paginateData, queryParams);
  // }

  async create(createProductDto: CreatePublicInfoDto): Promise<PublicInfo> {
    let createdProduct = new this.publicInfoModel(createProductDto);
    createdProduct = await generateSlugByName(createdProduct, 'title');
    // await this.validateSlug(null, createdProduct);
    createdProduct.variants = await generateVariantSlug(createdProduct.variants);
    return this.publicInfoModel.create(createdProduct);
  }

  async findByIdAndUpdate(id: string, product): Promise<PublicInfo> {
    product = await generateSlugByName(product, 'title');
    product.variants = await generateVariantSlug(product.variants);
    // await this.validateSlug(id, product);
    return this.publicInfoModel.findByIdAndUpdate(id, product).populate('facilities');
  }

  async findAll(queryParams: any): Promise<PublicInfo[]> {
    const query: any = {};
    const options: any = {
      lean: true,
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
      populate: 'category',
      sort: { createdAt: -1 },
    };

    if (queryParams._sort) {
      options.sort = {
        [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
      };
    }

    if (queryParams._sort) {
      options.sort = {
        [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
      };
    }

    if (Object.keys(queryParams).length === 0) {
      return this.publicInfoModel
        .find(query)
        .populate('category')
        .sort({ createdAt: -1 });
    }

    if (queryParams.lang && !queryParams.page) {
      let array = await this.publicInfoModel
        .find(query)
        .lean()
        .populate('tags', 'name slug')
        .populate('category')
        .populate({
          path: 'attributes.attributeId',
          select: 'name _id unit type attributeGroup order',
          populate: { path: 'unit attributeGroup' },
        })
        .populate('facilities')
        .populate('variants.attributes.attributeId', 'name _id unit type attributeGroup order');
      array = await getDataByLang(array, queryParams);
      return array;
    }

    if (queryParams.title) {
      query['title.' + queryParams.lang] = { $regex: queryParams.title, $options: 'i' };
    }

    const paginateData = await this.publicInfoModel.paginate(query, options);
    return getDataByLang(paginateData, queryParams);
  }

  async remove(id: string): Promise<Product> {
    return this.publicInfoModel.findByIdAndDelete(id);
  }

  async exportAsCSV(res) {
    let products = await this.publicInfoModel.find().lean();
    products = products.map(product => {
      product._id = product._id.toString();
      return product;
    });

    const filename = 'products.csv';
    res.setHeader('Content-Type', 'text/csv');
    res.setHeader('Content-Disposition', 'attachment; filename=' + filename);
    const fields = ['_id', 'title.ro', 'description.ro', 'slug', 'createdAt'];
    const json2csvParser = new json2csv.Parser({ fields });
    const csv = json2csvParser.parse(products);
    res.status(200).send(csv);
  }
}
