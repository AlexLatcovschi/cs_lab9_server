import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UploadController } from '../upload/upload.controller';
import { OrderController } from './order.controller';
import { OrderService } from './order.service';
import { OrderSchema } from '../../schemas/order.schema';
import { CartSchema } from '../../schemas/cart.schema';
import { ProductSchema } from '../../schemas/product.schema';
import { ErrorsService } from '../../config/errors.service';
import { UploadService } from '../upload/upload.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Order', schema: OrderSchema }]),
    MongooseModule.forFeature([{ name: 'Cart', schema: CartSchema }]),
    MongooseModule.forFeature([{ name: 'Product', schema: ProductSchema }]),
  ],
  providers: [OrderService, ErrorsService, UploadService],
  controllers: [OrderController, UploadController],
})
export class OrderModule {}
