import { ApiModelProperty } from '@nestjs/swagger';

export class CreateShippingDto {
  @ApiModelProperty()
  firstName: string;
  @ApiModelProperty()
  lastName: string;
  @ApiModelProperty()
  email: string;
  @ApiModelProperty()
  address: string;
  @ApiModelProperty()
  floor: number;
  @ApiModelProperty()
  block: string;
  @ApiModelProperty()
  phone: string;
  @ApiModelProperty()
  apartment: number;
  @ApiModelProperty()
  interphone: string;
  @ApiModelProperty()
  zipCode: string;
  @ApiModelProperty()
  city: string;
  @ApiModelProperty()
  country: string;
}
