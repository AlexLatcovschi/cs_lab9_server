import { ApiModelProperty } from '@nestjs/swagger';

export class CreateOrderDto {
  @ApiModelProperty()
  products: [
    {
      product: string;
      variant: string;
      title: object;
      thumbnail: string;
      price: number;
      attributes: [
        {
          attributeName: object;
          value: object;
        },
      ];
      quantity: number;
    },
  ];
  @ApiModelProperty()
  shipping: {
    firstName: string;
    lastName: string;
    email: string;
    address: string;
    floor: number;
    block: string;
    phone: string;
    apartment: number;
    interphone: string;
    zipCode: string;
    city: string;
    country: string;
  };
  @ApiModelProperty()
  userId: string;
  @ApiModelProperty()
  status: string;
}
