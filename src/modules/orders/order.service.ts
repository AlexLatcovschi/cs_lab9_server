import { HttpException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateOrderDto } from './dto/create-order.dto';
import { Order } from '../../schemas/order.schema';
import { Model } from 'mongoose';
import { Cart } from '../../schemas/cart.schema';
import { PaginateModel } from 'mongoose-paginate-v2';
import { Product } from '../../schemas/product.schema';
import { CreateShippingDto } from './dto/create-shipping.dto';
import { InjectEventEmitter } from 'nest-emitter';
import { EmailEvents } from '../email/email.events';
import { getDataByLang } from '../../utils/lang.utils';
import { ErrorsService } from '../../config/errors.service';

@Injectable()
export class OrderService {
  constructor(
    @InjectModel('Order') private orderModel: PaginateModel<Order>,
    @InjectModel('Cart') private cartModel: Model<Cart>,
    @InjectModel('Product') private productModel: Model<Product>,
    @InjectEventEmitter() private emitter: EmailEvents,
    private error: ErrorsService,
  ) {}

  async findAll(queryParams: any): Promise<Order[]> {
    const query: any = {};
    const options: any = {
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
      populate: { path: 'userId', select: '-password' },
      sort: { updatedAt: -1 },
    };

    if (queryParams._sort) {
      options.sort = {
        [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
      };
    }

    if (Object.keys(queryParams).length === 0) {
      return this.orderModel.find().sort({ createdAt: -1 });
    }
    return this.orderModel.paginate(query, options);
  }

  async findbyUserId(userId, queryParams: any): Promise<Order[]> {
    const query: any = {};
    const options: any = {
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
      populate: [{ path: 'userId', select: '-password' }, 'products.product'],
      sort: { updatedAt: -1 },
    };

    if (Object.keys(queryParams).length === 0) {
      return this.orderModel.find({ userId }).populate('products.product');
    }

    if (queryParams.lang && !queryParams.page) {
      let ordersArray = await this.orderModel
        .find({ userId })
        .populate('products.product')
        .lean();
      ordersArray = await getDataByLang(ordersArray, queryParams);
      return ordersArray;
    }

    return this.orderModel.paginate({ userId }, options);
  }

  async findById(id: string): Promise<Order> {
    return this.orderModel
      .findById(id)
      .populate('products.product')
      .populate('userId', '-password');
  }

  async create(createOrderDto: CreateOrderDto): Promise<Order> {
    const createdOrder = new this.orderModel(createOrderDto);
    return this.orderModel.create(createdOrder);
  }

  async findByIdAndUpdate(id: string, data: object): Promise<Order> {
    return this.orderModel.findByIdAndUpdate(id, data);
  }

  async remove(id: string): Promise<Order> {
    return this.orderModel.findByIdAndDelete();
  }

  async updateStatus(id, status): Promise<Order> {
    return this.orderModel.updateOne({ _id: id }, { $set: { status: status.status } });
  }

  async checkoutOrder(cartId, shipping: CreateShippingDto): Promise<Order> {
    const cart = await this.cartModel
      .findById(cartId)
      .populate({ path: 'products.product', populate: { path: 'variants.attributes.attributeId' } })
      .lean(true);
    const newOrder = {
      userId: cart.userId,
      products: cart.products,
      shipping,
    };

    await this.addFieldsToProductOrder(newOrder, cart);

    // Check if cart is in complete state
    if (cart.status === 'complete') {
      throw new HttpException(this.error.get('CART_IS_ORDERED'), 400);
    }

    // Create order
    const createdOrder = await this.orderModel.create(newOrder);
    if (createdOrder) {
      cart.products.forEach(async x => {
        await this.productModel.updateOne(
          {
            _id: x.product._id.toString(),
            variants: { $elemMatch: { _id: x.variant } },
          },
          { $inc: { 'variants.$.quantity': -x.quantity } },
        );
      });
    }

    // Update status in cart
    await this.cartModel.findByIdAndUpdate(
      cartId,
      { status: 'complete', userId: null },
      {
        new: true,
        runValidators: true,
      },
    );

    this.emitter.emit('orderEmail', createdOrder);
    return createdOrder;
  }

  async addFieldsToProductOrder(newOrder, cart) {
    cart.products.map((cartProduct, i) => {
      const variant = cartProduct.product.variants.filter(currentVariant => {
        return currentVariant._id.toString() === cartProduct.variant.toString();
      });
      newOrder.products[i].title = cartProduct.product.title;
      newOrder.products[i].thumbnail = cartProduct.product.images[0];
      newOrder.products[i].price = variant[0].price;
      newOrder.products[i].attributes = variant[0].attributes.map(x => {
        return {
          attributeName: x.attributeId.name,
          value: x.value,
        };
      });
    });
  }
}
