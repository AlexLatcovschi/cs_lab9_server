import { Body, Controller, Delete, Get, Param, Post, Put, Query, Response, UseGuards } from '@nestjs/common';
import { OrderService } from './order.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { Order } from '../../schemas/order.schema';
import { AuthGuard } from '@nestjs/passport';
import { CreateShippingDto } from './dto/create-shipping.dto';
import { ApiBearerAuth, ApiOperation, ApiUseTags } from '@nestjs/swagger';
import { AuthUser } from '../../decorators/user.decorator';
import { Roles } from '../../decorators/roles.decorator';

@ApiUseTags('Orders')
@Controller('orders')
export class OrderController {
  constructor(private readonly orderService: OrderService) {}

  @ApiBearerAuth()
  @Get()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async findAll(@Query() query): Promise<Order[]> {
    return this.orderService.findAll(query);
  }

  @ApiBearerAuth()
  @ApiOperation({ title: 'Get my orders' })
  @Get('/me')
  @UseGuards(AuthGuard('jwt'))
  async findMyOrders(@AuthUser() user: any, @Query() query): Promise<Order[]> {
    return this.orderService.findbyUserId(user._id, query);
  }

  @ApiBearerAuth()
  @ApiOperation({ title: 'Get orders by userId' })
  @Get('/user/:userId')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async findByUserId(@Param('userId') userId: string, @Query() query): Promise<Order[]> {
    return this.orderService.findbyUserId(userId, query);
  }

  @ApiBearerAuth()
  @Get('/:id')
  @UseGuards(AuthGuard('jwt'))
  async findById(@Param('id') id: string): Promise<Order> {
    return this.orderService.findById(id);
  }

  @Put('/:cartId/checkout')
  async checkoutOrder(@Param('cartId') cartId: string, @Body() shipping: CreateShippingDto): Promise<Order> {
    return this.orderService.checkoutOrder(cartId, shipping);
  }

  @ApiBearerAuth()
  @Post()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async create(@AuthUser() authUser, @Response() res: any, @Body() createOrderDto: CreateOrderDto) {
    return this.orderService.create(createOrderDto);
  }

  @ApiBearerAuth()
  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  async findByIdAndUpdate(@Param('id') id: string, @Body() order): Promise<Order> {
    return this.orderService.findByIdAndUpdate(id, order);
  }

  @ApiBearerAuth()
  @Put('/status/:id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async updateStatus(@Param('id') id: string, @Body() status): Promise<Order> {
    return this.orderService.updateStatus(id, status);
  }

  @ApiBearerAuth()
  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async remove(@Param('id') id: string) {
    return this.orderService.remove(id);
  }
}
