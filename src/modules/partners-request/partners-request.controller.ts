import { Body, Controller, Delete, Get, Param, Post, Query, UseGuards } from '@nestjs/common';
import { PartnersRequestService } from './partners-request.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { AbstractLangQuery } from '../../utils/abstract/AbstractLang.query';
import { PartnersRequest } from '../../schemas/partners-request.schema';
import { ProductFindQuery } from '../partners/dto/ProductFind.query';

@ApiUseTags('PartnersRequest')
@Controller('partners-request')
export class PartnersRequestController {
  constructor(private readonly service: PartnersRequestService) {}

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get('/encrypted')
  async findAllEncrypted(@Query() query: ProductFindQuery): Promise<PartnersRequest[]> {
    return this.service.findAllEncrypted(query);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get('/decrypted')
  async findAllDecrypted(@Query() query: ProductFindQuery): Promise<PartnersRequest[]> {
    return this.service.findAllDecrypted(query);
  }

  @Get('/:id')
  async findById(@Param('id') id: string, @Query() query: AbstractLangQuery): Promise<PartnersRequest> {
    return this.service.findById(id, query);
  }

  @Get('/slug/:slug')
  async findBySlug(@Param('slug') slug: string, @Query() query: any): Promise<PartnersRequest> {
    return this.service.findBySlug(slug, query);
  }

  @Post()
  async create(@Body() createProductDto: PartnersRequest) {
    console.log('AAAAAAAAAAA: ', createProductDto);
    return this.service.create(createProductDto);
  }

  @ApiBearerAuth()
  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  async remove(@Param('id') id: string) {
    return this.service.remove(id);
  }
}
