import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaginateModel } from 'mongoose-paginate-v2';
import { generateSlugByName, generateVariantSlug } from '../../utils/generate-slug.utils';
import { getDataByLang } from '../../utils/lang.utils';
import { AbstractLangQuery } from '../../utils/abstract/AbstractLang.query';
import { PartnersRequest } from '../../schemas/partners-request.schema';
import * as crypto from 'crypto';
import { ConfigService } from '../../config/config.service';
import { Model } from 'mongoose';
import { User } from '../../schemas/user.schema';

@Injectable()
export class PartnersRequestService {
  private config: ConfigService;

  constructor(
    @InjectModel('PartnersRequest') private partnersRequestSchema: PaginateModel<PartnersRequest>,
    config: ConfigService,
  ) {
    this.config = config;
  }

  async findById(id: string, query?: AbstractLangQuery): Promise<PartnersRequest> {
    const res = await this.partnersRequestSchema.findById(id).lean();
    if (query && query.lang && res) {
      res.description = res.description[query.lang];
      res.title = res.title[query.lang];
    }
    if (query && query.lang) {
      getDataByLang(res, query);
    }
    return res;
  }

  async findBySlug(slug: string, query): Promise<PartnersRequest> {
    const res = await this.partnersRequestSchema.findOne({ slug }).lean();
    if (query.lang) {
      return getDataByLang(res, query);
    }
    return res;
  }

  async create(createProductDto: PartnersRequest): Promise<PartnersRequest> {
    Object.keys(createProductDto).map(el => {
      createProductDto[el] = this.encryptColumn(createProductDto[el]);
    });
    return this.partnersRequestSchema.create(createProductDto);
  }

  async findAllEncrypted(queryParams: any): Promise<PartnersRequest[]> {
    const query: any = {};
    const options: any = {
      lean: true,
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
      populate: 'images',
      sort: { createdAt: -1 },
    };
    return this.partnersRequestSchema.paginate(query, options);
  }

  async findAllDecrypted(queryParams: any): Promise<PartnersRequest[]> {
    const query: any = {};
    const options: any = {
      lean: true,
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
      populate: 'images',
      sort: { createdAt: -1 },
    };
    const res = await this.partnersRequestSchema.paginate(query, options);

    res.docs.map(el => {
      el.phone = this.decryptColumn(new Buffer(el.phone, 'binary'));
      el.name = this.decryptColumn(new Buffer(el.name, 'binary'));
      el.company = this.decryptColumn(new Buffer(el.company, 'binary'));
      el.email = this.decryptColumn(new Buffer(el.email, 'binary'));
    });
    return res;
  }

  async findByIdAndUpdate(id: string, product): Promise<PartnersRequest> {
    product = await generateSlugByName(product, 'title');
    product.variants = await generateVariantSlug(product.variants);
    // await this.validateSlug(id, product);
    return this.partnersRequestSchema.findByIdAndUpdate(id, product);
  }

  async findAll(queryParams: any): Promise<PartnersRequest[]> {
    const query: any = {};
    const options: any = {
      lean: true,
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
      populate: 'images',
      sort: { createdAt: -1 },
    };

    if (queryParams._sort) {
      options.sort = {
        [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
      };
    }

    if (queryParams._sort) {
      options.sort = {
        [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
      };
    }

    if (queryParams.lang && !queryParams.page) {
      let array = await this.partnersRequestSchema.find(query).lean();
      array = await getDataByLang(array, queryParams);
      return array;
    }

    if (queryParams.title) {
      query['title.' + queryParams.lang] = { $regex: queryParams.title, $options: 'i' };
    }

    const paginateData = await this.partnersRequestSchema.paginate(query, options);
    return getDataByLang(paginateData, queryParams);
  }

  async remove(id: string): Promise<PartnersRequest> {
    return this.partnersRequestSchema.findByIdAndDelete(id);
  }

  encryptColumn(textToEncrypt) {
    const cipher = crypto.createCipheriv(
      'aes-256-ctr',
      this.config.get('COLUMN_ENCRYPTION_SECRET_KEY'),
      this.config.get('IV_ENCRYPTION_VALUE'),
    );
    return Buffer.concat([cipher.update(textToEncrypt), cipher.final()]);
  }

  decryptColumn(textToDecrypt) {
    const decipher = crypto.createDecipheriv(
      'aes-256-ctr',
      this.config.get('COLUMN_ENCRYPTION_SECRET_KEY'),
      this.config.get('IV_ENCRYPTION_VALUE'),
    );
    return Buffer.concat([decipher.update(textToDecrypt), decipher.final()]).toString('utf8');
  }
}
