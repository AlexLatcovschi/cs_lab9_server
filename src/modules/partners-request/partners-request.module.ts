import { Module } from '@nestjs/common';
import { PartnersRequestService } from './partners-request.service';
import { PartnersRequestController } from './partners-request.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UploadController } from '../upload/upload.controller';
import { ErrorsService } from '../../config/errors.service';
import { UploadService } from '../upload/upload.service';
import { PartnersRequestSchema } from '../../schemas/partners-request.schema';
import { ConfigService } from '../../config/config.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'PartnersRequest', schema: PartnersRequestSchema }])],
  providers: [PartnersRequestService, ErrorsService, UploadService, ConfigService],
  controllers: [PartnersRequestController, UploadController],
  exports: [PartnersRequestService],
})
export class PartnersRequestModule {}
