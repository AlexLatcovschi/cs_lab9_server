import { Module } from '@nestjs/common';
import { TagsService } from './tags.service';
import { TagsController } from './tags.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UploadController } from '../upload/upload.controller';
import { TagSchema } from '../../schemas/tag.schema';
import { ProductSchema } from '../../schemas/product.schema';
import { ArticleSchema } from '../../schemas/article.schema';
import { ErrorsService } from '../../config/errors.service';
import { UploadService } from '../upload/upload.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Tag', schema: TagSchema },
      { name: 'Product', schema: ProductSchema },
      { name: 'Article', schema: ArticleSchema },
    ]),
  ],
  providers: [TagsService, ErrorsService, UploadService],
  controllers: [TagsController, UploadController],
})
export class TagsModule {}
