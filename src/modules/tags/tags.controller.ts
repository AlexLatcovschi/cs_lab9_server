import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { TagsService } from './tags.service';
import { CreateTagDto } from './dto/create-tag.dto';
import { Tag } from '../../schemas/tag.schema';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { Roles } from '../../decorators/roles.decorator';

@ApiUseTags('Tag')
@Controller('tags')
export class TagsController {
  constructor(private readonly tagsService: TagsService) {}

  @Get('/:id')
  async findById(@Param('id') id: string): Promise<Tag> {
    return this.tagsService.findById(id);
  }

  @Get('items/:id')
  async findItemsByTag(@Param('id') id: string) {
    return this.tagsService.findAllItemsByTag(id);
  }

  @Get('/slug/:slug')
  async findBySlug(@Param('slug') slug: string): Promise<Tag> {
    return this.tagsService.findBySlug(slug);
  }

  @Post()
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async create(@Body() createTagDto: CreateTagDto) {
    return this.tagsService.create(createTagDto);
  }

  @Put('/:id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async findByIdAndUpdate(@Param('id') id: string, @Body() data): Promise<Tag> {
    return this.tagsService.findByIdAndUpdate(id, data);
  }

  @Get()
  async findAll(@Query() query: any): Promise<Tag[]> {
    return this.tagsService.findAll(query);
  }

  @Delete(':id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async remove(@Param('id') id: string) {
    return this.tagsService.remove(id);
  }
}
