export class CreateTagDto {
  readonly name: string;
  readonly description: string;
  readonly slug: string;
}
