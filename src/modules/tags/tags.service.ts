import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateTagDto } from './dto/create-tag.dto';
import { PaginateModel } from 'mongoose-paginate-v2';
import { Tag } from '../../schemas/tag.schema';
import { getDataByLang } from '../../utils/lang.utils';
import { Product } from '../../schemas/product.schema';
import { Article } from '../../schemas/article.schema';

@Injectable()
export class TagsService {
  constructor(
    @InjectModel('Tag')
    private tagModel: PaginateModel<Tag>,
    @InjectModel('Product')
    private productModel: PaginateModel<Product>,
    @InjectModel('Article')
    private articleModel: PaginateModel<Article>,
  ) {}

  async findById(id: string): Promise<Tag> {
    return this.tagModel.findById(id);
  }

  async findBySlug(slug: string): Promise<Tag> {
    return this.tagModel.findOne({ slug });
  }

  async findAllItemsByTag(id: string) {
    return this.tagModel.find();
  }

  async create(createTagDto: CreateTagDto): Promise<Tag> {
    const createdTag = new this.tagModel(createTagDto);
    return this.tagModel.create(createdTag);
  }

  async findByIdAndUpdate(id: string, data: object): Promise<Tag> {
    return this.tagModel.findByIdAndUpdate(id, data);
  }

  async findAll(queryParams: any): Promise<Tag[]> {
    const query: any = {};
    const options: any = {
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
    };

    if (queryParams._sort) {
      options.sort = {
        [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
      };
    }

    if (Object.keys(queryParams).length === 0) {
      return this.tagModel.find();
    }

    if (queryParams.sort) {
      let tags = await this.tagModel.find({}).lean();
      tags = await this.countNumberOfArticlesAndSortByCount(tags);
      await tags.splice(15);
      return tags;
    }

    if (queryParams.name) {
      query.name = { $regex: queryParams.name, $options: 'i' };
    }

    if (queryParams.slug) {
      query.slug = { $regex: queryParams.slug, $options: 'i' };
    }

    return this.tagModel.paginate(query, options);
  }

  async remove(id: string): Promise<Tag> {
    const products = await this.productModel.find({ tags: { $in: id } });
    const articles = await this.articleModel.find({ tags: { $in: id } });
    if (products.length > 0 || articles.length > 0) {
      throw new BadRequestException('This tag is used in one or more components');
    }
    return this.tagModel.findByIdAndDelete(id);
  }

  async countNumberOfArticlesAndSortByCount(tags) {
    for (const tag of tags) {
      tag.count = await this.articleModel.countDocuments({ tags: { $in: tag._id } });
    }
    return tags.sort((a, b) => (a.count > b.count ? -1 : 1));
  }
}
