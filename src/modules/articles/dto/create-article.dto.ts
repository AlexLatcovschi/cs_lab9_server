export class CreateArticleDto {
  readonly title: object;
  readonly content: object;
  readonly slug: string;
  readonly category: string;
  readonly image: any;
  readonly preamble: object;
  readonly productCategories: string[];
  readonly tags: string[];
  readonly author: string;
  readonly type: string;
  readonly date: Date;
  readonly status: string;
  readonly language: string;
  readonly translation: [{ lang: string; article: string }];
}
