import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ArticlesService } from './articles.service';
import { CreateArticleDto } from './dto/create-article.dto';
import { Article } from '../../schemas/article.schema';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { Roles } from '../../decorators/roles.decorator';

@ApiUseTags('Articles')
@Controller('articles')
export class ArticlesController {
  constructor(private readonly articlesService: ArticlesService) {}

  @Get()
  async findAll(@Query() query): Promise<Article[]> {
    return this.articlesService.findAll(query);
  }

  @Get('/:id')
  async findById(@Param('id') id: string): Promise<Article> {
    return this.articlesService.findById(id);
  }

  @Get('/slug/:slug')
  async findBySlug(@Param('slug') slug: string, @Query() query): Promise<Article> {
    return this.articlesService.findBySlug(slug, query);
  }

  @Get('/related/slug/:slug')
  async findRelatedBySlug(@Param('slug') slug: string, @Query() query): Promise<Article[]> {
    return this.articlesService.findRelatedBySlug(slug, query);
  }

  @Get('/category/:categorySlug')
  async findByCategorySlug(@Param('categorySlug') categorySlug: string, @Query() query): Promise<Article[]> {
    return this.articlesService.findByCategorySlug(categorySlug, query);
  }

  @Get('/tag/:tag')
  async findByTag(@Param('tag') tag: string, @Query() query): Promise<Article[]> {
    return this.articlesService.findByTag(tag, query);
  }

  @ApiBearerAuth()
  @Post()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async create(@Body() createArticleDto: CreateArticleDto) {
    return this.articlesService.create(createArticleDto);
  }

  @ApiBearerAuth()
  @Put('/:id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async findByIdAndUpdate(@Param('id') id: string, @Body() data): Promise<Article> {
    return this.articlesService.findByIdAndUpdate(id, data);
  }

  @ApiBearerAuth()
  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async remove(@Param('id') id: string) {
    return this.articlesService.remove(id);
  }
}
