import { Module } from '@nestjs/common';
import { ArticlesService } from './articles.service';
import { ArticlesController } from './articles.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UploadController } from '../upload/upload.controller';
import { ArticleSchema } from '../../schemas/article.schema';
import { TagSchema } from '../../schemas/tag.schema';
import { ArticleCategorySchema } from '../../schemas/article-category.schema';
import { ErrorsService } from '../../config/errors.service';
import { UploadService } from '../upload/upload.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Article', schema: ArticleSchema }]),
    MongooseModule.forFeature([{ name: 'Tag', schema: TagSchema }]),
    MongooseModule.forFeature([{ name: 'ArticleCategory', schema: ArticleCategorySchema }]),
  ],
  providers: [ArticlesService, ErrorsService, UploadService],
  controllers: [ArticlesController, UploadController],
})
export class ArticlesModule {}
