import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateArticleDto } from './dto/create-article.dto';
import { Article } from '../../schemas/article.schema';
import { Tag } from '../../schemas/tag.schema';
import { Model } from 'mongoose';
import { PaginateModel } from 'mongoose-paginate-v2';
import { generateSlugByName } from '../../utils/generate-slug.utils';
import { getDataByLang } from '../../utils/lang.utils';
import { ArticleCategory } from '../../schemas/article-category.schema';

@Injectable()
export class ArticlesService {
  constructor(
    @InjectModel('Article') private articleModel: PaginateModel<Article>,
    @InjectModel('Tag') private tagModel: Model<Tag>,
    @InjectModel('ArticleCategory') private articleCategoryModel: Model<ArticleCategory>,
  ) {}

  async findAll(queryParams: any): Promise<Article[]> {
    const query: any = {};
    const options: any = {
      lean: true,
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
      sort: { updatedAt: -1 },
      populate: 'tags productCategories category author',
    };

    if (queryParams._sort) {
      options.sort = {
        [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
      };
    }

    if (Object.keys(queryParams).length === 0) {
      return this.articleModel
        .find({})
        .populate('tags author')
        .populate('productCategories')
        .populate('category')
        .sort({ createdAt: -1 });
    }

    if (queryParams.lang && !queryParams.page) {
      let array = await this.articleModel
        .find(query)
        .populate('tags author')
        .populate('productCategories')
        .populate('category')
        .sort({ createdAt: -1 })
        .lean();
      array = await getDataByLang(array, queryParams);
      return array;
    }

    if (queryParams.title) {
      query['title.' + queryParams.lang] = { $regex: queryParams.title, $options: 'i' };
    }
    const paginateData = await this.articleModel.paginate(query, options);
    return getDataByLang(paginateData, queryParams);
  }

  async findById(id: string): Promise<Article> {
    return this.articleModel
      .findById(id)
      .populate('tags author')
      .populate('productCategories')
      .populate('category');
  }

  async findBySlug(slug: string, queryParams: any): Promise<Article> {
    if (queryParams.lang) {
      let array = await this.articleModel
        .findOne({ slug })
        .populate('tags author')
        .populate('productCategories')
        .populate('category')
        .sort({ createdAt: -1 })
        .lean();
      array = await getDataByLang([array], queryParams);
      return array[0];
    }

    return this.articleModel
      .findOne({ slug })
      .populate('tags author')
      .populate('productCategories')
      .populate('category');
  }

  async findRelatedBySlug(slug: string, queryParams: any): Promise<Article[]> {
    const limit = queryParams.limit ? queryParams.limit : '-1';
    const doc = await this.articleModel
      .findOne({ slug })
      .populate('tags author')
      .populate('productCategories')
      .populate('category')
      .sort({ createdAt: -1 })
      .lean();
    if (!doc.tags || doc.tags.length === 0) {
      return [];
    }
    let array = await this.articleModel
      .find({ tags: { $elemMatch: { $in: doc.tags } }, _id: { $ne: doc._id } })
      .populate('tags author')
      .populate('productCategories')
      .populate('category')
      .sort({ createdAt: -1 })
      .limit(Number(limit))
      .lean();
    if (queryParams.lang) {
      array = await getDataByLang(array, queryParams);
      return array;
    }

    return array;
  }

  async findByCategorySlug(categorySlug: string, queryParams: any): Promise<Article[]> {
    const category = await this.articleCategoryModel.findOne({ slug: categorySlug }).lean();
    if (category) {
      const query = { category };
      const options: any = {
        lean: true,
        page: parseInt(queryParams.page, 10) || 1,
        limit: parseInt(queryParams.limit, 10) || 10,
        sort: { createdAt: -1 },
        populate: 'tags productCategories category author',
      };

      if (Object.keys(queryParams).length === 0) {
        return this.articleModel
          .find({ category })
          .populate('tags author')
          .populate('productCategories')
          .populate('category');
      }
      if (queryParams.lang && !queryParams.page) {
        let array = await this.articleModel
          .find({ category })
          .populate('tags author')
          .populate('productCategories')
          .populate('category')
          .sort({ createdAt: -1 })
          .lean();
        array = await getDataByLang(array, queryParams);
        return array;
      }

      const paginateData = await this.articleModel.paginate(query, options);
      return getDataByLang(paginateData, queryParams);
    } else {
      return [];
    }
  }

  async findByTag(slug: string, queryParams: any): Promise<Article[]> {
    const tags = await this.tagModel.findOne({ slug }).lean();
    if (tags) {
      const query = { tags };
      const options: any = {
        lean: true,
        page: parseInt(queryParams.page, 10) || 1,
        limit: parseInt(queryParams.limit, 10) || 10,
        sort: { createdAt: -1 },
        populate: 'tags productCategories category author',
      };

      if (Object.keys(queryParams).length === 0) {
        return this.articleModel
          .find({ tags })
          .populate('tags author')
          .populate('productCategories')
          .populate('category');
      }

      if (queryParams.lang && !queryParams.page) {
        let array = await this.articleModel
          .find({ tags })
          .populate('tags author')
          .populate('productCategories')
          .populate('category')
          .sort({ createdAt: -1 })
          .lean();
        array = await getDataByLang(array, queryParams);
        return array;
      }

      const paginateData = await this.articleModel.paginate(query, options);
      return getDataByLang(paginateData, queryParams);
    } else {
      return [];
    }
  }

  async create(createArticleDto: CreateArticleDto): Promise<Article> {
    let createdArticle = new this.articleModel(createArticleDto);
    createdArticle = await generateSlugByName(createdArticle, 'title');
    return this.articleModel.create(createdArticle);
  }

  async findByIdAndUpdate(id: string, data): Promise<Article> {
    data = await generateSlugByName(data, 'title');
    return this.articleModel.findByIdAndUpdate(id, data);
  }

  async remove(id: string): Promise<Article> {
    return this.articleModel.findByIdAndDelete(id);
  }
}
