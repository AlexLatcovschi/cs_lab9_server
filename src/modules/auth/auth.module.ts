import { Module } from '@nestjs/common';
import { UsersModule } from '../users/users.module';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';
import { AuthService } from './auth.service';
import { ConfigService } from '../../config/config.service';
import { BcryptService } from './bcrypt.service';
import { AuthController } from './auth.controller';
import { ConfigModule } from '../../config/config.module';
import { ErrorsService } from '../../config/errors.service';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get('JWT_SECRET'),
        signOptions: {
          expiresIn: '30 days', // 30 days
        },
      }),
      inject: [ConfigService],
    }),
    UsersModule,
  ],
  controllers: [AuthController],

  providers: [AuthService, BcryptService, JwtStrategy, ConfigService, ErrorsService],
  exports: [PassportModule, AuthService, JwtModule],
})
export class AuthModule {}
