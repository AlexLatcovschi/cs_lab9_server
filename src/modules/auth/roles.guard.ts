import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthService } from './auth.service';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector, private authService: AuthService) {}

  public async canActivate(context: ExecutionContext) {
    let user;
    const role = this.reflector.get<string>('role', context.getHandler());
    if (!role) {
      return true;
    }
    const token = this.getToken(context);
    const payload = await this.authService.decode(token);

    if (payload) {
      user = await this.authService.validateUserById(payload._id);
    }

    return user && user.role && isAllowed(role, user.role);
  }

  private getToken(executionContext: ExecutionContext): string {
    const request = executionContext.switchToHttp().getRequest();
    if (request.headers.authorization) {
      return request.headers.authorization.split(' ')[1];
    } else {
      return '';
    }
  }
}

function isAllowed(routeRole, userRole) {
  return routeRole.indexOf(userRole) > -1;
}
