import { Body, Controller, Get, HttpException, HttpStatus, Post, Response } from '@nestjs/common';
import { AuthService, generateToken } from './auth.service';
import { UsersService } from '../users/users.service';
import { BcryptService } from './bcrypt.service';
import { InjectEventEmitter } from 'nest-emitter';
import { EmailEvents } from '../email/email.events';
import { ApiUseTags } from '@nestjs/swagger';
import { AuthDto } from './dto/auth.dto';
import { RegisterDto } from './dto/register.dto';
import { ErrorsService } from '../../config/errors.service';

@ApiUseTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(
    @InjectEventEmitter() private readonly emitter: EmailEvents,
    private authService: AuthService,
    private userService: UsersService,
    private bcryptService: BcryptService,
    private error: ErrorsService,
  ) {}

  @Post('/register')
  async register(@Response() res: any, @Body() body: RegisterDto) {
    console.log('AAAAAAAAA: ', body);
    const params = {
      email: body.email,
      password: body.password,
      firstName: body.firstName,
      lastName: body.lastName,
      activationToken: generateToken(),
    };
    const createUserResponse = await this.authService.create(params);
    const token = await this.authService.signIn(createUserResponse);
    this.emitter.emit('confirmationEmail', createUserResponse);
    return res.status(HttpStatus.OK).json({ auth: true, token });
  }

  @Post('/login')
  async authenticateDashboard(@Response() res: any, @Body() body: AuthDto) {
    console.log('AAAAAAAAAAAAAAAAAAAAAAAA');
    const user = await this.authService.validateUser({ email: body.email });
    console.log('user: ', user);
    if (!user) {
      throw new HttpException(this.error.get('USER_NOT_FOUND'), 400);
    }
    if (!user.isActive) {
      throw new HttpException(this.error.get('USER_NOT_ACTIVE'), 400);
    }
    if ((await this.bcryptService.comparePassword(body.password, user.password)) && user.isActive) {
      const token = await this.authService.signIn(user);
      return res.status(HttpStatus.OK).json({ auth: true, token });
    }
    throw new HttpException(this.error.get('PASSWORD_INCORRECT'), 400);
  }
}
