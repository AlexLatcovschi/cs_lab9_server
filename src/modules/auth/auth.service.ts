import { JwtService } from '@nestjs/jwt';
import { BadRequestException, HttpException, Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import * as crypto from 'crypto';
import { ErrorsService } from '../../config/errors.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private error: ErrorsService,
  ) {}

  public async create(user: any): Promise<any> {
    try {
      return await this.usersService.create(user);
    } catch (err) {
      if (err.code === 11000) {
        // error for dupes
        throw new HttpException(this.error.get('EMAIL_HAS_ALREADY_USED'), 400);
      }
      throw err;
    }
  }

  async validateUser(payload: JwtPayload): Promise<any> {
    try {
      return this.usersService.findOneByEmail(payload.email);
    } catch (err) {
      throw err;
    }
  }

  async validateUserById(userId: string): Promise<any> {
    return this.usersService.findById(userId);
  }

  async signIn(user): Promise<string> {
    return this.generateJWTToken(user);
  }

  public generateJWTToken(user: any): string {
    const payload: any = {};
    payload._id = user._id;
    payload.email = user.email;
    payload.role = user.role;
    return this.jwtService.sign(payload);
  }

  public decode(token: string): any {
    return this.jwtService.decode(token);
  }
}
export const generateToken = (): string => {
  return crypto.randomBytes(16).toString('hex');
};
