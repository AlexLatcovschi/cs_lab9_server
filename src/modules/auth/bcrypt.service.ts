import { Injectable } from '@nestjs/common';
import { compareSync } from 'bcrypt';

@Injectable()
export class BcryptService {
  async comparePassword(password: string | undefined, hash: string | undefined): Promise<boolean> {
    try {
      return compareSync(password, hash);
    } catch (err) {
      throw err;
    }
  }
}
