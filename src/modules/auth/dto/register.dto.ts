import { IsEmail } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class RegisterDto {
  @ApiModelProperty()
  @IsEmail()
  email: string;
  @ApiModelProperty()
  password: string;
  @ApiModelProperty()
  firstName: string;
  @ApiModelProperty()
  lastName: string;
  @ApiModelProperty()
  activationToken: string;
}
