import { Body, Controller, Delete, Get, Param, Post, Query, UseGuards } from '@nestjs/common';
import { BlogService } from './blog.service';
import { BlogDto } from './dto/blog.dto';
import { Product } from '../../schemas/product.schema';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { AbstractLangQuery } from '../../utils/abstract/AbstractLang.query';
import { Roles } from '../../decorators/roles.decorator';
import { PublicInfo } from '../../schemas/PublicInfo.schema';
import { ProductFindQuery } from './dto/ProductFind.query';
import { Partners } from '../../schemas/partners.schema';
import { Blog } from '../../schemas/blog.schema';

@ApiUseTags('Blog')
@Controller('blog')
export class BlogController {
  constructor(private readonly service: BlogService) {}

  @Get()
  async findAll(@Query() query: ProductFindQuery): Promise<Blog[]> {
    return this.service.findAll(query);
  }

  @Get('/:id')
  async findById(@Param('id') id: string, @Query() query: AbstractLangQuery): Promise<Blog> {
    return this.service.findById(id, query);
  }

  @Get('/slug/:slug')
  async findBySlug(@Param('slug') slug: string, @Query() query: any): Promise<Blog> {
    return this.service.findBySlug(slug, query);
  }

  @ApiBearerAuth()
  @Post()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async create(@Body() createProductDto: Blog) {
    console.log('AAAAAAAAAAA: ', createProductDto);
    return this.service.create(createProductDto);
  }

  @ApiBearerAuth()
  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async remove(@Param('id') id: string) {
    return this.service.remove(id);
  }
}
