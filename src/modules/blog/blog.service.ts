import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { BlogDto } from './dto/blog.dto';
import { PaginateModel } from 'mongoose-paginate-v2';
import { Model } from 'mongoose';
import { generateSlugByName, generateVariantSlug } from '../../utils/generate-slug.utils';
import { getDataByLang } from '../../utils/lang.utils';
import { Language } from '../../schemas/language.schema';
import { AbstractLangQuery } from '../../utils/abstract/AbstractLang.query';
import { PublicInfo } from '../../schemas/PublicInfo.schema';
import { Partners } from '../../schemas/partners.schema';
import { Blog } from '../../schemas/blog.schema';

@Injectable()
export class BlogService {
  constructor(
    @InjectModel('Blog') private publicInfoModel: PaginateModel<Blog>,
    @InjectModel('Language') private langModel: Model<Language>,
  ) {}

  async findById(id: string, query?: AbstractLangQuery): Promise<Blog> {
    const res = await this.publicInfoModel.findById(id).lean();
    if (query && query.lang && res) {
      res.description = res.description[query.lang];
      res.title = res.title[query.lang];
    }
    if (query && query.lang) {
      getDataByLang(res, query);
    }
    return res;
  }

  async findBySlug(slug: string, query): Promise<Blog> {
    const res = await this.publicInfoModel.findOne({ slug }).lean();
    if (query.lang) {
      return getDataByLang(res, query);
    }
    return res;
  }

  async create(createProductDto: Blog): Promise<Blog> {
    let createdProduct = new this.publicInfoModel(createProductDto);
    createdProduct = await generateSlugByName(createdProduct, 'title');
    // await this.validateSlug(null, createdProduct);
    createdProduct.variants = await generateVariantSlug(createdProduct.variants);
    return this.publicInfoModel.create(createdProduct);
  }

  async findByIdAndUpdate(id: string, product): Promise<Blog> {
    product = await generateSlugByName(product, 'title');
    product.variants = await generateVariantSlug(product.variants);
    // await this.validateSlug(id, product);
    return this.publicInfoModel.findByIdAndUpdate(id, product);
  }

  async findAll(queryParams: any): Promise<Blog[]> {
    const query: any = {};
    const options: any = {
      lean: true,
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
      populate: 'images',
      sort: { createdAt: -1 },
    };

    if (queryParams._sort) {
      options.sort = {
        [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
      };
    }

    if (queryParams._sort) {
      options.sort = {
        [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
      };
    }

    if (queryParams.lang && !queryParams.page) {
      let array = await this.publicInfoModel.find(query).lean();
      array = await getDataByLang(array, queryParams);
      return array;
    }

    if (queryParams.title) {
      query['title.' + queryParams.lang] = { $regex: queryParams.title, $options: 'i' };
    }

    const paginateData = await this.publicInfoModel.paginate(query, options);
    return getDataByLang(paginateData, queryParams);
  }

  async remove(id: string): Promise<Blog> {
    return this.publicInfoModel.findByIdAndDelete(id);
  }
}
