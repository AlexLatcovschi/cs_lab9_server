import { ApiModelProperty } from '@nestjs/swagger';

export class BlogDto {
  @ApiModelProperty()
  title: object;
  @ApiModelProperty()
  description: object;
  @ApiModelProperty()
  slug: string;
  @ApiModelProperty()
  price: number;
  @ApiModelProperty()
  category: string;
  @ApiModelProperty()
  metaTitle: object;
  @ApiModelProperty()
  metaDescription: object;
  @ApiModelProperty()
  metaKeywords: object;
  @ApiModelProperty()
  widgetDescription: boolean;
  @ApiModelProperty()
  documentation: object[];
  @ApiModelProperty()
  widgets: object;
  @ApiModelProperty()
  facilities: string[];
  @ApiModelProperty()
  images: any[];
  @ApiModelProperty()
  tags: string[];
  @ApiModelProperty()
  variants: any[];
  @ApiModelProperty()
  relatedProducts: any[];
}
