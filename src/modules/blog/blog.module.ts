import { Module } from '@nestjs/common';
import { BlogService } from './blog.service';
import { BlogController } from './blog.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UploadController } from '../upload/upload.controller';
import { TagSchema } from '../../schemas/tag.schema';
import { LanguageSchema } from '../../schemas/language.schema';
import { ErrorsService } from '../../config/errors.service';
import { UploadService } from '../upload/upload.service';
import { PublicInfoSchema } from '../../schemas/PublicInfo.schema';
import { PartnersSchema } from '../../schemas/partners.schema';
import { BlogSchema } from '../../schemas/blog.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Blog', schema: BlogSchema }, { name: 'Language', schema: LanguageSchema }]),
  ],
  providers: [BlogService, ErrorsService, UploadService],
  controllers: [BlogController, UploadController],
  exports: [BlogService],
})
export class BlogModule {}
