import { Module } from '@nestjs/common';
import { OrderRequestService } from './order-request.service';
import { OrderRequestController } from './order-request.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UploadController } from '../upload/upload.controller';
import { TagSchema } from '../../schemas/tag.schema';
import { LanguageSchema } from '../../schemas/language.schema';
import { ErrorsService } from '../../config/errors.service';
import { UploadService } from '../upload/upload.service';
import { PublicInfoSchema } from '../../schemas/PublicInfo.schema';
import { PartnersSchema } from '../../schemas/partners.schema';
import { PartnersRequestSchema } from '../../schemas/partners-request.schema';
import { OrderRequestSchema } from '../../schemas/order-request.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'OrderRequest', schema: OrderRequestSchema },
      { name: 'Language', schema: LanguageSchema },
    ]),
  ],
  providers: [OrderRequestService, ErrorsService, UploadService],
  controllers: [OrderRequestController, UploadController],
  exports: [OrderRequestService],
})
export class OrderRequestModule {}
