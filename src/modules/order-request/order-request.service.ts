import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { OrderRequestDto } from './dto/order-request.dto';
import { PaginateModel } from 'mongoose-paginate-v2';
import { Model } from 'mongoose';
import { generateSlugByName, generateVariantSlug } from '../../utils/generate-slug.utils';
import { getDataByLang } from '../../utils/lang.utils';
import { Language } from '../../schemas/language.schema';
import { AbstractLangQuery } from '../../utils/abstract/AbstractLang.query';
import { PublicInfo } from '../../schemas/PublicInfo.schema';
import { Partners } from '../../schemas/partners.schema';
import { Blog } from '../../schemas/blog.schema';
import { PartnersRequest } from '../../schemas/partners-request.schema';
import { OrderRequest } from '../../schemas/order-request.schema';

@Injectable()
export class OrderRequestService {
  constructor(
    @InjectModel('OrderRequest') private orderRequestSchema: PaginateModel<OrderRequest>,
    @InjectModel('Language') private langModel: Model<Language>,
  ) {}

  async findById(id: string, query?: AbstractLangQuery): Promise<OrderRequest> {
    const res = await this.orderRequestSchema.findById(id).lean();
    if (query && query.lang && res) {
      res.description = res.description[query.lang];
      res.title = res.title[query.lang];
    }
    if (query && query.lang) {
      getDataByLang(res, query);
    }
    return res;
  }

  async findBySlug(slug: string, query): Promise<OrderRequest> {
    const res = await this.orderRequestSchema.findOne({ slug }).lean();
    if (query.lang) {
      return getDataByLang(res, query);
    }
    return res;
  }

  async create(createProductDto: OrderRequest): Promise<OrderRequest> {
    let createdProduct = new this.orderRequestSchema(createProductDto);
    createdProduct = await generateSlugByName(createdProduct, 'title');
    // await this.validateSlug(null, createdProduct);
    createdProduct.variants = await generateVariantSlug(createdProduct.variants);
    return this.orderRequestSchema.create(createdProduct);
  }

  async findByIdAndUpdate(id: string, product): Promise<OrderRequest> {
    product = await generateSlugByName(product, 'title');
    product.variants = await generateVariantSlug(product.variants);
    // await this.validateSlug(id, product);
    return this.orderRequestSchema.findByIdAndUpdate(id, product);
  }

  async findAll(queryParams: any): Promise<OrderRequest[]> {
    const query: any = {};
    const options: any = {
      lean: true,
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
      populate: 'images',
      sort: { createdAt: -1 },
    };

    if (queryParams._sort) {
      options.sort = {
        [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
      };
    }

    if (queryParams._sort) {
      options.sort = {
        [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
      };
    }

    if (queryParams.lang && !queryParams.page) {
      let array = await this.orderRequestSchema.find(query).lean();
      array = await getDataByLang(array, queryParams);
      return array;
    }

    if (queryParams.title) {
      query['title.' + queryParams.lang] = { $regex: queryParams.title, $options: 'i' };
    }

    const paginateData = await this.orderRequestSchema.paginate(query, options);
    return getDataByLang(paginateData, queryParams);
  }

  async remove(id: string): Promise<OrderRequest> {
    return this.orderRequestSchema.findByIdAndDelete(id);
  }
}
