import { Body, Controller, Delete, Get, Param, Post, Query, UseGuards } from '@nestjs/common';
import { OrderRequestService } from './order-request.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { AbstractLangQuery } from '../../utils/abstract/AbstractLang.query';
import { Roles } from '../../decorators/roles.decorator';
import { ProductFindQuery } from './dto/ProductFind.query';
import { Blog } from '../../schemas/blog.schema';
import { PartnersRequest } from '../../schemas/partners-request.schema';
import { OrderRequest } from '../../schemas/order-request.schema';

@ApiUseTags('OrderRequest')
@Controller('order-request')
export class OrderRequestController {
  constructor(private readonly service: OrderRequestService) {}

  @Get()
  async findAll(@Query() query: ProductFindQuery): Promise<OrderRequest[]> {
    return this.service.findAll(query);
  }

  @Get('/:id')
  async findById(@Param('id') id: string, @Query() query: AbstractLangQuery): Promise<OrderRequest> {
    return this.service.findById(id, query);
  }

  @Get('/slug/:slug')
  async findBySlug(@Param('slug') slug: string, @Query() query: any): Promise<OrderRequest> {
    return this.service.findBySlug(slug, query);
  }

  @Post()
  async create(@Body() createProductDto: OrderRequest) {
    console.log('AAAAAAAAAAA: ', createProductDto);
    return this.service.create(createProductDto);
  }

  @ApiBearerAuth()
  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async remove(@Param('id') id: string) {
    return this.service.remove(id);
  }
}
