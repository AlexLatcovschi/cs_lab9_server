import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { FilterGroupService } from './filter-group.service';
import { CreateFilterGroupDto } from './dto/create-filter-group.dto';
import { FilterGroup } from '../../schemas/filter-group.schema';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { Roles } from '../../decorators/roles.decorator';

@ApiUseTags('Filters')
@Controller('filters')
export class FilterGroupController {
  constructor(private readonly filterGroupService: FilterGroupService) {}

  @Get()
  async findAll(@Query() query): Promise<FilterGroup[]> {
    return this.filterGroupService.findAll(query);
  }

  @Get(':id')
  async findById(@Param('id') id: string): Promise<FilterGroup> {
    return this.filterGroupService.findById(id);
  }

  @ApiBearerAuth()
  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async findByIdAndUpdate(@Param('id') id: string, @Body() category): Promise<FilterGroup> {
    return this.filterGroupService.findByIdAndUpdate(id, category);
  }

  @ApiBearerAuth()
  @Post()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async create(@Body() createFilterGroupDto: CreateFilterGroupDto) {
    return this.filterGroupService.create(createFilterGroupDto);
  }

  @ApiBearerAuth()
  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async remove(@Param('id') id: string) {
    return this.filterGroupService.remove(id);
  }
}
