import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateFilterGroupDto {
  @ApiModelProperty()
  @IsNotEmpty()
  readonly name: object;
  @ApiModelPropertyOptional()
  readonly description?: object;
  @ApiModelPropertyOptional()
  readonly attributes?: object[];
}
