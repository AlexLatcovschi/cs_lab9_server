import { BadRequestException, Injectable } from '@nestjs/common';
import { PaginateModel } from 'mongoose-paginate-v2';
import { InjectModel } from '@nestjs/mongoose';
import { CreateFilterGroupDto } from './dto/create-filter-group.dto';
import { FilterGroup } from '../../schemas/filter-group.schema';
import { getDataByLang } from '../../utils/lang.utils';
import { ProductCategory } from '../../schemas/product-category.schema';

@Injectable()
export class FilterGroupService {
  constructor(
    @InjectModel('FilterGroup')
    private readonly filterGroupModel: PaginateModel<FilterGroup>,
    @InjectModel('ProductCategory')
    private readonly productCategoryModel: PaginateModel<ProductCategory>,
  ) {}

  async findAll(queryParams: any): Promise<FilterGroup[]> {
    const query = {};
    const options: any = {
      lean: true,
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 2,
      sort: { updatedAt: -1 },
    };
    if (Object.keys(queryParams).length === 0) {
      return this.filterGroupModel
        .find()
        .populate('attributes.attributeId')
        .sort({ createdAt: -1 });
    }

    if (queryParams.name) {
      query['name.' + queryParams.lang] = { $regex: queryParams.name, $options: 'i' };
    }

    if (queryParams.lang && !queryParams.page) {
      let array = await this.filterGroupModel
        .find(query)
        .sort({ createdAt: -1 })
        .lean();
      array = await getDataByLang(array, queryParams);
      return array;
    }

    const paginateData = await this.filterGroupModel.paginate(query, options);
    return getDataByLang(paginateData, queryParams);
  }

  async findById(id: string): Promise<FilterGroup> {
    return this.filterGroupModel.findById(id).populate('attributes.attributeId');
  }

  async findByIdAndUpdate(id: string, category: object): Promise<FilterGroup> {
    return this.filterGroupModel.findByIdAndUpdate(id, category);
  }

  async create(createProductDto: CreateFilterGroupDto): Promise<FilterGroup> {
    const createdProduct = new this.filterGroupModel(createProductDto);
    return this.filterGroupModel.create(createdProduct);
  }

  // TODO set null to all products when deleted
  async remove(id: string): Promise<FilterGroup> {
    const products = await this.productCategoryModel.find({ filters: { $in: id } });
    if (products.length > 0) {
      throw new BadRequestException('This filter is used in one product or more');
    } else {
      return this.filterGroupModel.findByIdAndDelete(id);
    }
  }
}
