import { Module } from '@nestjs/common';
import { FilterGroupController } from './filter-group.controller';
import { FilterGroupService } from './filter-group.service';
import { MongooseModule } from '@nestjs/mongoose';
import { FilterGroupSchema } from '../../schemas/filter-group.schema';
import { ProductCategorySchema } from '../../schemas/product-category.schema';
import { ErrorsService } from '../../config/errors.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'FilterGroup',
        schema: FilterGroupSchema,
      },
      {
        name: 'ProductCategory',
        schema: ProductCategorySchema,
      },
    ]),
  ],

  controllers: [FilterGroupController],
  providers: [FilterGroupService, ErrorsService],
  exports: [FilterGroupService],
})
export class FilterGroupModule {}
