import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UploadController } from '../upload/upload.controller';
import { AttributeUnitSchema } from '../../schemas/attribute-unit.schema';
import { AttributeUnitService } from './attribute-unit.service';
import { AttributeUnitsController } from './attribute-units.controller';
import { ErrorsService } from '../../config/errors.service';
import { UploadService } from '../upload/upload.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'AttributeUnit', schema: AttributeUnitSchema }])],
  controllers: [AttributeUnitsController, UploadController],
  providers: [AttributeUnitService, ErrorsService, UploadService],
  exports: [AttributeUnitService],
})
export class AttributeUnitModule {}
