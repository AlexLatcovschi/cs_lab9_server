import { BadRequestException, Injectable } from '@nestjs/common';
import { PaginateModel } from 'mongoose-paginate-v2';
import { InjectModel } from '@nestjs/mongoose';
import { CreateAttributeUnitDto } from './dto/create-attribute-unit.dto';
import { AttributeUnit } from '../../schemas/attribute-unit.schema';

@Injectable()
export class AttributeUnitService {
  constructor(
    @InjectModel('AttributeUnit')
    private readonly attributeUnitModel: PaginateModel<AttributeUnit>,
  ) {}

  async findAll(queryParams: any): Promise<AttributeUnit[]> {
    const query: any = {};
    const options: any = {
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
    };

    if (Object.keys(queryParams).length === 0) {
      return this.attributeUnitModel.find({});
    }

    if (queryParams.name) {
      query.name = { $regex: queryParams.name, $options: 'i' };
    }

    return this.attributeUnitModel.paginate(query, options);
  }

  async findById(id: string): Promise<AttributeUnit> {
    return this.attributeUnitModel.findById(id);
  }

  async findByIdAndUpdate(id: string, attribute: object): Promise<AttributeUnit> {
    return this.attributeUnitModel.findByIdAndUpdate(id, attribute);
  }

  async create(createAttributeUnitDto: CreateAttributeUnitDto): Promise<AttributeUnit> {
    const newAttributeUnit = new this.attributeUnitModel(createAttributeUnitDto);
    return this.attributeUnitModel.create(newAttributeUnit);
  }

  async remove(id: string): Promise<AttributeUnit> {
    return this.attributeUnitModel.findByIdAndDelete(id);
  }
}
