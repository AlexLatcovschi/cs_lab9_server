import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { AttributeUnitService } from './attribute-unit.service';
import { CreateAttributeUnitDto } from './dto/create-attribute-unit.dto';
import { AuthGuard } from '@nestjs/passport';
import { AttributeUnit } from '../../schemas/attribute-unit.schema';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { Roles } from '../../decorators/roles.decorator';

@ApiUseTags('Attribute-units')
@Controller('attribute-units')
export class AttributeUnitsController {
  constructor(private readonly attributesUnitsService: AttributeUnitService) {}

  @Get()
  async findAll(@Query() query): Promise<AttributeUnit[]> {
    return this.attributesUnitsService.findAll(query);
  }

  @Get(':id')
  async findById(@Param('id') id: string): Promise<AttributeUnit> {
    return this.attributesUnitsService.findById(id);
  }

  @ApiBearerAuth()
  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async findByIdAndUpdate(@Param('id') id: string, @Body() attribute): Promise<AttributeUnit> {
    return this.attributesUnitsService.findByIdAndUpdate(id, attribute);
  }

  @ApiBearerAuth()
  @Post()
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async create(@Body() createAttributeUnitDto: CreateAttributeUnitDto) {
    return this.attributesUnitsService.create(createAttributeUnitDto);
  }

  @ApiBearerAuth()
  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @Roles('admin')
  async remove(@Param('id') id: string) {
    return this.attributesUnitsService.remove(id);
  }
}
