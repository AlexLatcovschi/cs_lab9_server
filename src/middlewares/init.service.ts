import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from '../schemas/user.schema';
import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '../config/config.service';

@Injectable()
export class InitService implements OnModuleInit {
  private config: ConfigService;
  private readonly logger = new Logger(InitService.name);

  constructor(@InjectModel('User') private userModel: Model<User>, config: ConfigService) {
    this.config = config;
  }

  async onModuleInit() {
    try {
      await this.create();
    } catch (err) {
      this.logger.error(err);
    }
  }

  async create(): Promise<User> {
    try {
      const user = await this.userModel.findOne({ email: this.config.get('ADMIN_EMAIL') });
      if (!user) {
        const newUser = {
          email: this.config.get('ADMIN_EMAIL'),
          firstName: 'Admin',
          lastName: 'Admin',
          isActive: true,
          password: this.config.get('ADMIN_PASSWORD'),
          role: 'admin',
        };
        return await this.userModel.create(newUser);
      } else {
        this.logger.log('Admin exists');
      }
    } catch (err) {
      throw err;
    }
  }
}
