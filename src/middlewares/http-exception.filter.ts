import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common';
import { Request, Response } from 'express';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status = exception.getStatus();

    if (exception) {
      response.status(status).json({
        message: exception.message.message,
        code: exception.message.code,
        statusCode: status,
        path: request.url,
      });
    } else {
      response.status(status).json({
        message: 'No exception found',
        code: 0,
        statusCode: status,
        path: request.url,
      });
    }
  }
}
