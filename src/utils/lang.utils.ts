const defaultLang = 'ro';

export async function CheckWidgetInComponents(components, widgetId): Promise<boolean> {
  let exist = false;
  for (const element of components) {
    for (const lang in element.widgets) {
      if (element.widgets.hasOwnProperty(lang)) {
        if (element.widgets[lang]) {
          const position = element.widgets[lang]
            .map(widget => {
              if (widget._id === undefined) {
                return widget.toString();
              }
              return widget._id.toString();
            })
            .indexOf(widgetId.toString());
          if (position !== -1) {
            exist = true;
            break;
          }
        }
      }
    }
  }
  return exist;
}

export function getPageByLang(page, lang) {
  for (const prop in page) {
    if (page.hasOwnProperty(prop) && page[prop]) {
      if (page[prop].hasOwnProperty(lang)) {
        page[prop] = page[prop][lang];
      } else if (page[prop].hasOwnProperty(defaultLang)) {
        page[prop] = page[prop][defaultLang];
      }
    }
  }
  return page;
}

export function getProductByLang(product, lang) {
  for (const prop in product) {
    if (product.hasOwnProperty(prop) && product[prop]) {
      if (product[prop].hasOwnProperty(lang)) {
        product[prop] = product[prop][lang];
      } else if (product[prop].hasOwnProperty(defaultLang)) {
        product[prop] = product[prop][defaultLang];
      }
    }
  }
  return product;
}

export function getCategoryWithSubcategoriesByLang(data, lang) {
  for (const field in data) {
    if (data.hasOwnProperty(field) && data[field]) {
      if (data[field].hasOwnProperty(lang)) {
        data[field] = data[field][lang];
      } else if (data[field].hasOwnProperty(defaultLang)) {
        data[field] = data[field][defaultLang];
      }
      if (field === 'parent') {
        data[field] = getCategoryWithSubcategoriesByLang(data[field], lang);
      }
      if (field === 'childs' && data[field].length > 0) {
        data[field] = data[field].map(subcategory => {
          subcategory = getCategoryWithSubcategoriesByLang(subcategory, lang);
          return subcategory;
        });
      }
    }
  }
  return data;
}

export function getDataByLang(array, queryParams) {
  if (array && array.docs) {
    getDataByLangIntermediate(array.docs, queryParams.lang);
  } else {
    getDataByLangIntermediate(array, queryParams.lang);
  }
  return array;
}

function getDataByLangIntermediate(value: object[] | object, lang): any {
  traverse(value, lang);
  return value;
}

function traverse(x, lang: string) {
  if (x !== null) {
    if (x instanceof Array) {
      traverseArray(x, lang);
    } else if (typeof x === 'object') {
      traverseObject(x, lang);
    }
  }
}

function traverseArray(arr, lang: string) {
  arr.forEach(x => traverse(x, lang));
}

function traverseObject(obj, lang: string) {
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      if (
        obj[key] &&
        (obj[key][lang] || obj[key][defaultLang] || obj[key][lang] === '' || obj[key][defaultLang] === '')
      ) {
        obj[key] = obj[key][lang] || obj[key][defaultLang] || '';
      } else {
        traverse(obj[key], lang);
      }
    }
  }
}
