import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { EmailTypeEnum } from '../../modules/email/emailType.enum';

export class ContactEmailDto {
  @ApiModelPropertyOptional()
  readonly name: string;
  @ApiModelProperty()
  @IsNotEmpty()
  readonly email: string;
  @ApiModelPropertyOptional()
  readonly phone: string;
  @ApiModelPropertyOptional()
  readonly company: string;
  @ApiModelProperty()
  readonly message: string;
  @ApiModelProperty()
  readonly type: EmailTypeEnum;
}
