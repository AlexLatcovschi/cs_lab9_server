import { Body, Controller, Post, Res, HttpStatus } from '@nestjs/common';
import { InjectEventEmitter } from 'nest-emitter';
import { EmailEvents } from '../modules/email/email.events';
import { ContactEmailDto } from './dto/contact-email.dto';

@Controller('utils')
export class ContactsController {
  constructor(@InjectEventEmitter() private readonly emitter: EmailEvents) {}

  @Post('/contact-email')
  findBySlug(@Body() data: ContactEmailDto, @Res() res: any): any {
    this.emitter.emit('contactEmail', data);
    res.status(HttpStatus.CREATED).send();
  }
}
