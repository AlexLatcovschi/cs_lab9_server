import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { AbstractLangQuery } from './AbstractLang.query';

export class AbstractPaginationQuery extends AbstractLangQuery {
  @ApiModelPropertyOptional()
  public readonly page: number;
  public readonly limit: number;
}
