import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class AbstractLangQuery {
  @ApiModelPropertyOptional()
  public readonly lang: string;
}
