import { extname } from 'path';
import * as sharp from 'sharp';

export const getResizedImagePath = (file: any, resizeType: string) => {
  const fileExtension = extname(file.path).substr(1);
  const pathWithoutExtension = file.path
    .split('.')
    .slice(0, -1)
    .join('.');
  return `${pathWithoutExtension}-${resizeType}.${fileExtension}`;
};

export const sharpImageWidth = async (file: any, resizeTypeName: string, width: number): Promise<any> => {
  const pathToSaveFile = getResizedImagePath(file, resizeTypeName);
  await sharp(file.path)
    .resize(width)
    .toFile(pathToSaveFile);
  const resizedImagePath = getResizedImagePath(file, resizeTypeName);
  return { key: resizeTypeName, path: resizedImagePath };
};

export const sharpImageWidthAndHeight = async (
  file: any,
  resizeTypeName: string,
  width: number,
  height: number,
): Promise<any> => {
  const pathToSaveFile = getResizedImagePath(file, resizeTypeName);
  await sharp(file.path)
    .resize(width, height)
    .toFile(pathToSaveFile);
  const resizedImagePath = getResizedImagePath(file, resizeTypeName);
  return { key: resizeTypeName, path: resizedImagePath };
};
