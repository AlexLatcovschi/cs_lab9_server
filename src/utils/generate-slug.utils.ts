import slugify from 'slugify';

export function generateVariantSlug(variants) {
  variants.map(x => {
    let slug;
    x.attributes.map(y => {
      if (!slug && y.value.ro) {
        slug = slugify(y.value.ro)
          .replace(/\s+/g, '-') // Replace spaces with -
          .replace(/[^\w\-]+/g, '') // Remove all non-word chars
          .replace(/\-\-+/g, '-') // Replace multiple - with single -
          .replace(/^-+/, '') // Trim - from start of text
          .replace(/-+$/, ''); // Trim - from end of text;
      } else if (y.value.ro) {
        slug =
          slug +
          '-' +
          slugify(y.value.ro, { remove: /[*+~.()'"!:@]/g })
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(/[^\w\-]+/g, '') // Remove all non-word chars
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, '') // Trim - from end of text
            .toLowerCase();
      }
    });
    if (slug) {
      x.slug = slug;
    }
  });
  return variants;
}

export function generateSlugByName(object, field) {
  // TODO fix slug generator
  // object.slug = slugify(object[field].ro).toLowerCase()
  //   .replace(/\s+/g, '-')           // Replace spaces with -
  //   .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
  //   .replace(/\-\-+/g, '-')         // Replace multiple - with single -
  //   .replace(/^-+/, '')             // Trim - from start of text
  //   .replace(/-+$/, '');            // Trim - from end of text;
  return object;
}
