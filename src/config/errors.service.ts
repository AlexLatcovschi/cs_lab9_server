export class ErrorsService {
  private readonly errors: {};

  constructor() {
    this.errors = {
      CART_NOT_FOUND: {
        code: 1,
        message: 'Cart is not found or is not available',
      },
      USER_NOT_FOUND: {
        code: 2,
        message: 'User does not exist or not found',
      },
      EMAIL_HAS_ALREADY_USED: {
        code: 3,
        message: 'This email address has already been used',
      },
      CART_HAS_ALREADY_ASSIGNED: {
        code: 4,
        message: 'Shopping Cart has already been assigned to the specified user',
      },
      USER_HAS_ALREADY_ACTIVATED: {
        code: 5,
        message: 'User with email is already activated',
      },
      ACTIVATION_TOKEN_INVALID: {
        code: 6,
        message: 'User with such reset token does not exist',
      },
      TOKEN_TIME_EXPIRED: {
        code: 7,
        message: 'Time for reset password has expired',
      },
      QUANTITY_PRODUCTS_EXCEEDED: {
        code: 8,
        message: 'The quantity of the products was exceeded',
      },
      NOT_HAVE_ACCES_TO_CART: {
        code: 9,
        message: 'You do not have access to this cart',
      },
      CART_IS_ORDERED: {
        code: 10,
        message: 'The cart is already ordered',
      },
      PASSWORD_INCORRECT: {
        code: 11,
        message: 'Password is incorrect',
      },
      USER_NOT_ACTIVE: {
        code: 12,
        message: 'User it is not active',
      },
    };
  }

  public get(key: string): string {
    return this.errors[key];
  }
}
