import { Module } from '@nestjs/common';
import { ConfigService } from './config.service';
import { ErrorsService } from './errors.service';

@Module({
  providers: [ConfigService, ErrorsService],
  exports: [ConfigService, ErrorsService],
})
export class ConfigModule {}
