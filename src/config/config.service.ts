import * as dotenv from 'dotenv';
import * as fs from 'fs';

export class ConfigService {
  MONGODB_URI: string;
  private readonly envConfig: { [key: string]: string };

  constructor() {
    if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging') {
      this.envConfig = {
        APP_FULL_HOST: process.env.APP_FULL_HOST,
        DASHBOARD_URL: process.env.DASHBOARD_URL,
        MONGODB_URI: process.env.MONGODB_URI,
        PORT: process.env.PORT,
        CLIENT_URL: process.env.CLIENT_URL,
        CONTACT_EMAIL: process.env.CONTACT_EMAIL,
        ORDERS_EMAIL: process.env.ORDERS_EMAIL,
        ADMIN_EMAIL: process.env.ADMIN_EMAIL,
        FROM_EMAIL: process.env.FROM_EMAIL,
        QUOTES_EMAIL: process.env.QUOTES_EMAIL,
        ADMIN_PASSWORD: process.env.ADMIN_PASSWORD,
        JWT_SECRET: process.env.JWT_SECRET,
        MAILCHIMP_AUDIENCE_ID: process.env.MAILCHIMP_AUDIENCE_ID,
        MAILCHIMP_API_KEY: process.env.MAILCHIMP_API_KEY,
        SENDGRID_API_KEY: process.env.SENDGRID_API_KEY,
      };
    } else {
      this.envConfig = dotenv.parse(fs.readFileSync('.env'));
    }
  }

  get(key: string): string {
    return this.envConfig[key];
  }
}
