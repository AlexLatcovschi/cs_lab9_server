import { Document, Schema } from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface Menu extends Document {
  readonly name: object;
  readonly elements: object[];
}

export const MenuSchema = new Schema(
  {
    name: { type: Object },
    elements: [
      {
        title: { type: Object },
        link: { type: Object },
        icon: {
          path: { type: String, default: null },
          mimetype: { type: String, default: null },
        },
        activeIcon: {
          path: { type: String, default: null },
          mimetype: { type: String, default: null },
        },
        submenu: [{ type: Object }],
        order: { type: Number },
        parent: { type: String },
        elementId: { type: Schema.Types.ObjectId },
        type: {
          type: String,
          enum: ['Page', 'Article', 'Custom Link', 'Category'],
          default: 'Custom Link',
        },
      },
    ],
    active: { type: Boolean, default: false },
  },
  {
    timestamps: true,
    minimize: false,
  },
);

MenuSchema.plugin(mongoosePaginate);
MenuSchema.plugin(slug);
