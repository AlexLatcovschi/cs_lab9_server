import { Document, Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface Order extends Document, mongoosePaginate {
  readonly products: any[];
  readonly userId: string;
  readonly status: string;
  readonly shipping: any;
}

export const OrderSchema = new Schema(
  {
    products: [
      {
        product: { type: Schema.Types.ObjectId, ref: 'Product', require: true },
        variant: { type: String },
        title: Object,
        thumbnail: { type: Object },
        price: Number,
        attributes: [
          {
            attributeName: Object,
            value: Object,
          },
        ],
        quantity: { type: Number, default: 1 },
        _id: false,
      },
    ],
    shipping: {
      firstName: { type: String },
      lastName: { type: String },
      email: { type: String },
      address: { type: String },
      floor: { type: Number },
      block: { type: String },
      phone: { type: String },
      apartment: { type: Number },
      interphone: { type: String },
      zipCode: { type: String },
      city: { type: String },
      country: { type: String },
    },
    userId: { type: Schema.Types.ObjectId, ref: 'User' },
    status: { type: String, enum: ['Pending', 'Shipped', 'Canceled', 'Received', 'Completed'], default: 'Pending' },
  },
  {
    timestamps: true,
    minimize: false,
  },
);

OrderSchema.plugin(mongoosePaginate);
