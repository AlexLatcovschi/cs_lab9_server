import { Document, Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface AttributeUnit extends Document {
  name: string;
}

export const AttributeUnitSchema = new Schema(
  {
    name: { type: String, required: true, unique: true },
  },
  {
    timestamps: true,
  },
);

AttributeUnitSchema.plugin(mongoosePaginate);
