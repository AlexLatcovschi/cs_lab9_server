import { Document, Schema } from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface Page extends Document {
  readonly title: object;
  readonly description: object;
  slug: string;
  image: {
    mimetype: string;
    path: string;
  };
  keywords: [];
  widgets: object;
  isMain: boolean;
}

export const PageSchema = new Schema(
  {
    title: { type: Object },
    description: { type: Object },
    slug: { type: String },
    keywords: { type: String },
    image: {
      mimetype: String,
      path: String,
    },
    published: { type: Boolean, default: false },
    widgets: { type: Object },
    isMain: { type: Boolean, default: false },
  },
  {
    timestamps: true,
    minimize: false,
  },
);

PageSchema.plugin(mongoosePaginate);
PageSchema.plugin(slug);
