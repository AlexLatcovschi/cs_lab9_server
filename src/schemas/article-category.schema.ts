import { Document, Schema } from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface ArticleCategory extends Document {
  readonly name: object;
  readonly description: object;
  readonly slug: string;
}

export const ArticleCategorySchema = new Schema(
  {
    name: { type: Object },
    description: { type: Object },
    slug: { type: String },
  },
  {
    timestamps: true,
  },
);

ArticleCategorySchema.plugin(mongoosePaginate);
ArticleCategorySchema.plugin(slug);
