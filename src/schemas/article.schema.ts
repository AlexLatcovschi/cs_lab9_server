import { Document, Schema } from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface Article extends Document {
  readonly title: object;
  readonly content: object;
  readonly preamble: object;
  readonly image: any;
  slug: string;
}

export const ArticleSchema = new Schema(
  {
    title: { type: Object },
    image: {
      _id: { type: Schema.Types.ObjectId, auto: true },
      description: String,
      path: { type: String, default: null },
      mimetype: { type: String, default: null },
      updatedAt: { type: Date, default: Date.now },
    },
    preamble: { type: Object },
    content: { type: Object },
    slug: { type: String },
    productCategories: [
      {
        type: Schema.Types.ObjectId,
        ref: 'ProductCategory',
      },
    ],
    category: { type: Schema.Types.ObjectId, ref: 'ArticleCategory' },
    categories: [{ type: Schema.Types.ObjectId, ref: 'ArticleCategory' }], // Delete after change
    tags: [{ type: Schema.Types.ObjectId, ref: 'Tag' }],
    author: { type: Schema.Types.ObjectId, ref: 'User' },
    type: { type: String },
    date: { type: Date },
    status: { type: String, enum: ['draft', 'published'] },
    language: { type: Schema.Types.ObjectId, ref: 'Language' }, //
    translation: [
      {
        lang: String,
        article: { type: Schema.Types.ObjectId, ref: 'Article' },
      },
    ],
  },
  {
    timestamps: true,
    minimize: false,
  },
);

ArticleSchema.plugin(mongoosePaginate);
ArticleSchema.plugin(slug);
