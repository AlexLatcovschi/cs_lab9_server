import { Document, Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface Quote extends Document {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  comment: string;
  status: string;
  productData: {
    product: string;
    variant: string;
  };
}

export const QuoteSchema = new Schema(
  {
    firstName: { type: String },
    lastName: { type: String },
    email: { type: String },
    phone: { type: String },
    comment: { type: String },
    status: { type: String, enum: ['pending', 'completed'], default: 'pending' },
    productData: {
      product: { type: Schema.Types.ObjectId, ref: 'Product' },
      variant: { type: Schema.Types.ObjectId },
    },
  },
  {
    timestamps: true,
  },
);

QuoteSchema.plugin(mongoosePaginate);
