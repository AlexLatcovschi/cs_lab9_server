import { Document, Schema } from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface PublicInfo extends Document {
  description: string;
  images: object[];
  sliderImages: object[];
  tags: string[];
  metaTitle: object;
  metaDescription: object;
  metaKeywords: object;
  mainPosts: object;
}

export const PublicInfoSchema = new Schema(
  {
    description: { type: Object },
    category: { type: Schema.Types.ObjectId, ref: 'ProductCategory' },
    tags: [{ type: Schema.Types.ObjectId, ref: 'Tag' }],
    images: [
      {
        description: String,
        path: { type: String, default: null },
        mimetype: { type: String, default: null },
        updatedAt: { type: Date, default: Date.now },
      },
    ],
    sliderImages: [
      {
        description: String,
        path: { type: String, default: null },
        mimetype: { type: String, default: null },
        updatedAt: { type: Date, default: Date.now },
      },
    ],
    metaTitle: { type: Object },
    metaDescription: { type: Object },
    metaKeywords: { type: Object },
  },
  {
    minimize: false,
    timestamps: true,
  },
);

PublicInfoSchema.plugin(slug);
PublicInfoSchema.plugin(mongoosePaginate);
