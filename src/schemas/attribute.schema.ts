import { Document, Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export enum ATTRIBUTE_TYPE {
  checkbox = 'checkbox',
  radio = 'radio',
  color = 'color',
  slider = 'slider',
}

// export const ATTRIBUTE_TYPES = [ATTRIBUTE_TYPE.checkbox, ATTRIBUTE_TYPE.radio, ATTRIBUTE_TYPE.color, ATTRIBUTE_TYPE.slider];

export interface Attribute extends Document {
  readonly name: object;
  readonly unit?: string;
  readonly order: number;
  readonly type: ATTRIBUTE_TYPE;
}

export const AttributeSchema = new Schema(
  {
    name: { type: Object, required: true },
    unit: { type: Schema.Types.ObjectId, ref: 'AttributeUnit' },
    order: { type: Number, unique: true, min: 1 },
    attributeGroup: { type: Schema.Types.ObjectId, ref: 'AttributeGroup' },
    type: { type: String, enum: Object.values(ATTRIBUTE_TYPE), default: ATTRIBUTE_TYPE.checkbox },
  },
  {
    timestamps: true,
  },
);

AttributeSchema.plugin(mongoosePaginate);
