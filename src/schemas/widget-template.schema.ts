import { Document, Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface WidgetTemplate extends Document {
  readonly title: string;
  readonly type: string;
  icon: string;
  structure: any;
}

export const WidgetTemplateSchema = new Schema(
  {
    name: { type: String, required: true, trim: true, unique: true },
    type: { type: String },
    icon: { type: String },
    structure: { type: Object },
  },
  {
    timestamps: true,
  },
);

WidgetTemplateSchema.plugin(mongoosePaginate);
