import { Document, Schema } from 'mongoose';
import { genSaltSync, hashSync } from 'bcrypt';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface User extends Document {
  readonly email: string;
  password: string;
  readonly firstName: string;
  readonly lastName: string;
  readonly description: string;
  isActive: boolean;
  activationToken: string;
  passwordResetExpire: Date;
  passwordResetToken: string;
  readonly role: string;
  hashPassword(password: string): boolean;
}

export const UserSchema = new Schema(
  {
    firstName: { type: String },
    lastName: { type: String },
    description: { type: String },
    email: { type: String, required: true, unique: true },
    userPhoto: {
      description: String,
      path: { type: String, default: null },
      mimetype: { type: String, default: null },
      updatedAt: { type: Date, default: Date.now },
    },
    shipping: [
      {
        firstName: { type: String },
        lastName: { type: String },
        email: { type: String },
        address: { type: String },
        floor: { type: Number },
        block: { type: String },
        phone: { type: String },
        apartment: { type: Number },
        interphone: { type: String },
        zipCode: { type: String },
        city: { type: String },
        country: { type: String },
        active: { type: Boolean, default: false },
      },
    ],
    isActive: { type: Boolean, default: false },
    activationToken: { type: String },
    passwordResetToken: { type: String },
    passwordResetExpire: { type: Date },
    password: { type: String, required: true },
    role: { type: String, enum: ['admin', 'user'], default: 'user' },
  },
  {
    timestamps: true,
  },
);
/**
 * Pre-save hook
 */

UserSchema.pre<User>('save', function(next: any): any {
  const saltRounds = 10;
  // tslint:disable-next-line:no-this-assignment
  const user = this;
  if (this.isModified('password') || this.isNew) {
    const salt = genSaltSync(saltRounds);
    user.password = hashSync(user.password, salt);
    next();
  } else {
    return next();
  }
});

UserSchema.plugin(mongoosePaginate);
