import { Document, Schema } from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface Tag extends Document {
  readonly name: string;
  readonly description: string;
  slug: string;
}

export const TagSchema = new Schema(
  {
    name: { type: String, required: true, trim: true, unique: true },
    description: { type: String },
    slug: { type: String, slug: 'name', unique: true, slugPaddingSize: 1 },
  },
  {
    timestamps: true,
  },
);

TagSchema.plugin(slug);
TagSchema.plugin(mongoosePaginate);
