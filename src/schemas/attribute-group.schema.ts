import { Document, Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface IAttributeGroup extends Document {
  readonly _id: string;
  readonly name?: object;
  readonly order?: number;
}

export const AttributeGroupSchema = new Schema(
  {
    name: { type: Object, required: true },
    order: { type: Number, min: 0 },
  },
  {
    timestamps: true,
  },
);

AttributeGroupSchema.plugin(mongoosePaginate);
