import { Document, Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface Widget extends Document {
  payload: any;
  readonly name: string;
  template: any;
  data: any;
  pages: string[];
}

export const WidgetSchema = new Schema(
  {
    name: { type: String },
    template: { type: Schema.Types.ObjectId, ref: 'WidgetTemplate' },
    payload: { type: Object }, // <-- Json
    published: { type: Boolean, default: false },
    pages: [{ type: Schema.Types.ObjectId, ref: 'PageWidget' }],
  },
  {
    timestamps: true,
  },
);

WidgetSchema.plugin(mongoosePaginate);
