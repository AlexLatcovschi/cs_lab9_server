import { Document, Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface FilterGroup extends Document {
  readonly name: object;
  readonly description: object;
}

export const FilterGroupSchema = new Schema(
  {
    name: { type: Object },
    description: { type: Object },
    attributes: [
      {
        attributeId: { type: Schema.Types.ObjectId, ref: 'Attribute' },
      },
    ],
  },
  {
    timestamps: true,
  },
);

FilterGroupSchema.plugin(mongoosePaginate);
