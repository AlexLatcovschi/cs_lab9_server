import { Document, Schema } from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface ProductCategory extends Document {
  readonly name: object;
  readonly description: object;
  readonly parent: string;
  readonly slug: string;
}

export const ProductCategorySchema = new Schema(
  {
    name: { type: Object },
    description: { type: Object },
    slug: { type: String },
    parent: { type: Schema.Types.ObjectId, ref: 'ProductCategory', default: null },
    coverPhoto: {
      description: String,
      path: { type: String, default: null },
      mimetype: { type: String, default: null },
      updatedAt: { type: Date, default: Date.now },
    },
    thumbnail: {
      description: String,
      path: { type: String, default: null },
      mimetype: { type: String, default: null },
      updatedAt: { type: Date, default: Date.now },
    },
    order: { type: Number, unique: true, min: 1 },
    icon: {
      path: { type: String, default: null },
      mimetype: { type: String, default: null },
    },
    activeIcon: {
      path: { type: String, default: null },
      mimetype: { type: String, default: null },
    },
    filters: [{ type: Schema.Types.ObjectId, ref: 'FilterGroup' }],
  },
  {
    timestamps: true,
    minimize: false,
  },
);

ProductCategorySchema.plugin(mongoosePaginate);
ProductCategorySchema.plugin(slug);
