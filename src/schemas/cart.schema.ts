import { Document, Schema } from 'mongoose';
import * as mongooseLeanVirtuals from 'mongoose-lean-virtuals';

export interface Cart extends Document {
  readonly products: any[];
  readonly status: string;
  readonly userId: string;
}

const ProductSchema = new Schema(
  {
    product: { type: Schema.Types.ObjectId, ref: 'Product', require: true },
    variant: { type: String },
    quantity: { type: Number, default: 1, min: [1, 'Quantity must be greater than 0'] },
  },
  {
    toObject: {
      virtuals: true,
    },
    toJSON: {
      virtuals: true,
    },
  },
);

export const CartSchema = new Schema(
  {
    products: [ProductSchema],
    userId: { type: Schema.Types.ObjectId, ref: 'User' },
    status: { type: String, enum: ['active', 'complete'], default: 'active' },
  },
  {
    timestamps: true,
    toObject: {
      virtuals: true,
    },
    toJSON: {
      virtuals: true,
    },
  },
);

ProductSchema.virtual('totalPrice', { type: Number }).get(function(this: any): number {
  const variants = this.product.variants.filter(variant => {
    return variant._id.toString() === this.variant;
  });
  return parseFloat(variants[0].price) * parseFloat(this.quantity);
});

CartSchema.virtual('subTotal', { type: Number }).get(function(this: any): number {
  const reducer = (accumulator: number, currentValue) => {
    const currentVariant = currentValue.product.variants.filter(variant => {
      return variant._id.toString() === currentValue.variant;
    });
    return accumulator + parseFloat(currentVariant[0].price) * parseFloat(currentValue.quantity);
  };

  return this.products.reduce(reducer, 0);
});

CartSchema.plugin(mongooseLeanVirtuals);
