import { Document, Schema } from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface PartnersRequest extends Document {
  name: Buffer;
  company: Buffer;
  phone: Buffer;
  email: Buffer;
}

export const PartnersRequestSchema = new Schema(
  {
    name: { type: Buffer },
    company: { type: Buffer },
    phone: { type: Buffer },
    email: { type: Buffer },
  },
  {
    minimize: false,
    timestamps: true,
  },
);

PartnersRequestSchema.plugin(slug);
PartnersRequestSchema.plugin(mongoosePaginate);
