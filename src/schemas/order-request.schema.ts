import { Document, Schema } from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface OrderRequest extends Document {
  address: string;
  phone: string;
  fullName: string;
  email: string;
  description: string;
  products: object[];
}

export const OrderRequestSchema = new Schema(
  {
    address: { type: String },
    phone: { type: String },
    fullName: { type: String },
    email: { type: String },
    description: { type: String },
    products: { type: Object },
  },
  {
    minimize: false,
    timestamps: true,
  },
);

OrderRequestSchema.plugin(slug);
OrderRequestSchema.plugin(mongoosePaginate);
