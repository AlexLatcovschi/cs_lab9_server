import { Document, Schema } from 'mongoose';

export interface Language extends Document {
  readonly name: string;
  readonly shortName: string;
}

export const LanguageSchema = new Schema(
  {
    name: { type: String, required: true },
    shortName: { type: String, required: true, unique: true },
  },
  {
    timestamps: true,
  },
);
