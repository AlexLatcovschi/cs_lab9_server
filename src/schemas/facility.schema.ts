import { Document, Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface Facility extends Document, mongoosePaginate {
  readonly name: object;
  readonly description: object;
  readonly icon: string;
}

export const FacilitySchema = new Schema(
  {
    name: { type: Object },
    description: { type: Object },
    icon: { type: String, default: null },
  },
  {
    timestamps: true,
  },
);

FacilitySchema.plugin(mongoosePaginate);
