import { Document, Schema } from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface Blog extends Document {
  categoryImage: object;
  title: string;
  photo: object;
  content: string;
  metaTitle: object;
  metaDescription: object;
  metaKeywords: object;
}

export const BlogSchema = new Schema(
  {
    categoryImage: {
      description: String,
      path: { type: String, default: null },
      mimetype: { type: String, default: null },
      updatedAt: { type: Date, default: Date.now },
    },
    title: { type: Object },
    photo: {
      description: String,
      path: { type: String, default: null },
      mimetype: { type: String, default: null },
      updatedAt: { type: Date, default: Date.now },
    },
    content: { type: String },
    metaTitle: { type: Object },
    metaDescription: { type: Object },
    metaKeywords: { type: Object },
    relatedProducts: [{ type: Schema.Types.ObjectId, ref: 'Product' }],
  },
  {
    minimize: false,
    timestamps: true,
  },
);

BlogSchema.plugin(slug);
BlogSchema.plugin(mongoosePaginate);
