import { Document, Schema } from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface Partners extends Document {
  description: string;
  images: object[];
  sliderImages: object[];
  metaTitle: object;
  metaDescription: object;
  metaKeywords: object;
}

export const PartnersSchema = new Schema(
  {
    description: { type: Object },
    images: [
      {
        description: String,
        path: { type: String, default: null },
        mimetype: { type: String, default: null },
        updatedAt: { type: Date, default: Date.now },
      },
    ],
    sliderImages: [
      {
        description: String,
        path: { type: String, default: null },
        mimetype: { type: String, default: null },
        updatedAt: { type: Date, default: Date.now },
      },
    ],
    metaTitle: { type: Object },
    metaDescription: { type: Object },
    metaKeywords: { type: Object },
  },
  {
    minimize: false,
    timestamps: true,
  },
);

PartnersSchema.plugin(slug);
PartnersSchema.plugin(mongoosePaginate);
