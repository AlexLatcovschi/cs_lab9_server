import { Document, Schema } from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface Product extends Document {
  readonly title: object;
  readonly description: object;
  slug: string;
  images: object[];
  category: object;
  price: number;
  tags: string[];
  metaTitle: object;
  metaDescription: object;
  metaKeywords: object;
}

export const ProductSchema = new Schema(
  {
    title: { type: Object },
    description: { type: Object },
    slug: { type: String },
    category: { type: Schema.Types.ObjectId, ref: 'ProductCategory' },
    tags: [{ type: Schema.Types.ObjectId, ref: 'Tag' }],
    images: [
      {
        description: String,
        path: { type: String, default: null },
        mimetype: { type: String, default: null },
        updatedAt: { type: Date, default: Date.now },
      },
    ],
    documentation: [{ type: Object }],
    facilities: [{ type: Schema.Types.ObjectId, ref: 'Facility' }],
    widgetDescription: { type: Boolean, default: false },
    widgets: { type: Object, id: false, default: null },
    attributes: [
      {
        attributeId: { type: Schema.Types.ObjectId, ref: 'Attribute' },
        value: Object,
      },
    ],
    variants: [
      {
        sku: { type: String },
        slug: { type: String },
        attributes: [
          {
            attributeId: { type: Schema.Types.ObjectId, ref: 'Attribute' },
            value: { type: Object },
          },
        ],
        askQuote: { type: Boolean, default: false },
        quantity: { type: Number, default: 0, min: [0, 'Quantity must be greater than 0'] },
        price: { type: Number, min: [0, 'Price must be greater than 0'], default: 0 },
        image: {
          path: { type: String, default: null },
          mimetype: { type: String, default: null },
        },
      },
    ],
    metaTitle: { type: Object },
    metaDescription: { type: Object },
    metaKeywords: { type: Object },
    relatedProducts: [{ type: Schema.Types.ObjectId, ref: 'Product' }],
  },
  {
    minimize: false,
    timestamps: true,
  },
);

ProductSchema.plugin(slug);
ProductSchema.plugin(mongoosePaginate);
